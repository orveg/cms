<?php

function wordlimit($string, $length = 40, $ellipsis = "...") {
    $string = strip_tags($string, '<div>');
    $string = strip_tags($string, '<p>');
    $words = explode(' ', $string);
    if(count($words) > $length) {
        return implode(' ', array_slice($words, 0, $length)) . $ellipsis;
    } else {
        return $string . $ellipsis;
    }
}


function tree_builder($items, $html) {
    $output = '';

    if(is_array($items)) {
        foreach($items as $item) {
            if(isset($item['children']) and !empty($item['children'])) {
                // if there are children we build their html and set it up to be parsed as {{ children }}
                $item['children'] = '<ul>' . tree_builder($item['children'], $html) . '</ul>';
            } else {
                $item['children'] = NULL;
            }

            // now that the children html is sorted we parse the html that they passed
            $output .= ci()->parser->parse_string($html, $item, TRUE);
        }

        return $output;
    }

}

function dateFormat($date) {
    $date = strtotime($date);

    return date("d-m-Y H:i:s", $date);
}

function convertPageName($temp) {
    if($temp == "referenslarimiz") {
      return "Referenaslarimiz";
    }
    if($temp == "page-ik") {
        return "IK Sayfası";
    }
    if($temp == "contact") {
        return "İletişim Sayfası";
    }
    if($temp == "page_hizmetler") {
        return  "Hizmetler Sayfası";
    }
    if($temp == "ik_formu") {
        return "IK Formu Sayfası";
    }
    if($temp == "page-ik") {
        return "IK Sayfası";
    }
    if($temp == "page") {
        return "Genel Sayfa";
    }
    return $temp;
}
function typeContact($a) {
    if($a == 1) {
        return "<label class='label label-danger'>İletişim Formundan Gelen</label>";
    }
    if($a == 2) {
        return "<label class='label label-danger'>Ücretsiz Keşif Talebi</label>";
    }
    if($a == 3) {
        return "<label class='label label-danger'>İnsan Kaynakları Başvurusu</label>";
    }
}
?>