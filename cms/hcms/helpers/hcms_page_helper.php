<?php

function wordlimit($string, $length = 40, $ellipsis = "...") {
    $string = strip_tags($string, '<div>');
    $string = strip_tags($string, '<p>');
    $words = explode(' ', $string);
    if(count($words) > $length) {
        return implode(' ', array_slice($words, 0, $length)) . $ellipsis;
    } else {
        return $string . $ellipsis;
    }
}

//Get the navigation bar
function hooskNav($slug) {
    $CI =& get_instance();
    $CI->db->where('navSlug', $slug);
    $query = $CI->db->get('hcms_navigation');
    $n = $query->row_array();

    $totSegments = $CI->uri->total_segments();
    if(!is_numeric($CI->uri->segment($totSegments))) {
        $current = "/" . $CI->uri->segment($totSegments);
    } else {
        if(is_numeric($CI->uri->segment($totSegments))) {
            $current = "/" . $CI->uri->segment($totSegments - 1);
        }
    }

    if($current == "/") {
        $current = BASE_URL;
    };
    $nav = str_replace('<li><a href="' . $current . '">', '<li class="active"><a href="' . $current . '">',
        $n['navHTML']);
    $nav = str_replace('//', '/', $n['navHTML']);


    echo $nav;


}

function getFeedPosts() {
    $CI =& get_instance();
    $CI->db->order_by("unixStamp", "desc");
    $CI->db->where('published', 1);
    $query = $CI->db->get('hcms_post');

    return $query->result_array();
}

//Get the Latest 5 news posts
function getLatestNewsSidebar() {
    $CI =& get_instance();
    $CI->db->order_by("unixStamp", "desc");
    $CI->db->where('published', 1);
    $CI->db->limit(5, 0);
    $query = $CI->db->get('hcms_post');
    $posts = '<ul class="list-group">';
    foreach($query->result_array() as $c):
        $posts .= '<li class="list-group-item"><a href="' . BASE_URL . '/article/' . $c['postURL'] . '">' . $c['postTitle'] . '</a></li>';
    endforeach;
    $posts .= "</ul>";
    echo $posts;
}

//Get the Latest news for the main column
function getLatestNews($limit = 10, $offset = 0) {
    $CI =& get_instance();
    $CI->db->order_by("unixStamp", "desc");
    $CI->db->where('published', 1);
    $CI->db->limit($limit, $offset);
    $query = $CI->db->get('hcms_post');
    $posts = '';
    foreach($query->result_array() as $c):
        $date = new DateTime($c['datePosted']);
        $posts .= '<div class="row">';
        if($c['postImage'] != "") {
            $posts .= '<div class="col-md-3"><a href="' . BASE_URL . '/article/' . $c['postURL'] . '"><img class="img-responsive" src="' . BASE_URL . '/images/' . $c['postImage'] . '" alt="' . $c['postTitle'] . '"/></a></div>';
            $posts .= '<div class="col-md-9"><h3><a href="' . BASE_URL . '/article/' . $c['postURL'] . '">' . $c['postTitle'] . '</a></h3>';
            $posts .= '<p class="meta">' . date_format($date, 'd/m/Y') . '</p>';
            $posts .= '<p>' . $c['postExcerpt'] . '</p>';
            $posts .= '<p><a class="btn btn-primary" href="' . BASE_URL . '/article/' . $c['postURL'] . '">Read More</a></p>';
        } else {
            $posts .= '<div class="col-md-12"><h3><a href="' . BASE_URL . '/article/' . $c['postURL'] . '">' . $c['postTitle'] . '</a></h3>';
            $posts .= '<p class="meta">' . date_format($date, 'd/m/Y') . '</p>';
            $posts .= '<p>' . $c['postExcerpt'] . '</p>';
            $posts .= '<p><a class="btn btn-primary" href="' . BASE_URL . '/article/' . $c['postURL'] . '">Read More</a></p>';
        }
        $posts .= '</div>';
        $posts .= "</div><hr />";
    endforeach;
    echo $posts;
}

//Get the categories
function getCategories() {
    $CI =& get_instance();
    $CI->db->order_by("categoryOrder", "asc");
    $query = $CI->db->get('hcms_post_category');
    $categories = '<ul class="list-group">';
    foreach($query->result_array() as $c):
        $CI->db->where('categoryID', $c['categoryID']);
        $CI->db->from('hcms_post');
        $query = $CI->db->get();
        $totPosts = $query->num_rows();
        if($totPosts > 0) {
            $selected = $CI->uri->segments[2] == $c['categorySlug'] ? "selected" : "";
            $categories .= '<li class="' . $selected . '"><a href="' . BASE_URL . '/kategori/' . $c['categorySlug'] . '">' . tr_strtoupper($c['categoryTitle']) . '</a></li>';
        }
    endforeach;
    $categories .= "</ul>";
    echo $categories;
}


//Get the total posts
function countPosts($categoryID = 0, $limit = 10, $offset = 0, $yaz = TRUE) {
    $CI =& get_instance();
    $CI->db->from('hcms_post');
    $CI->db->where('published', 1);
    if($categoryID != 0) {
        $CI->db->where('categoryID', $categoryID);
    }
    $query = $CI->db->get();
    $totPosts = $query->num_rows();
    $showing = $offset + $limit;
    if($showing > $totPosts) {
        $showing = $totPosts;
    }
    $offset++;
    if($yaz) {
        echo "Gösterilen ürün " . $offset . " - " . $showing . " ile " . $totPosts;
    } else {
        return $totPosts;
    }
}


function countCategoryPosts($categoryID, $limit = 10, $offset = 0) {
    $CI =& get_instance();
    $CI->db->from('hcms_post');
    $CI->db->where('categoryID', $categoryID);
    $CI->db->where('published', 1);
    $query = $CI->db->get();
    $totPosts = $query->num_rows();
    $showing = $offset + $limit;
    if($showing > $totPosts) {
        $showing = $totPosts;
    }
    $offset++;
    echo "Showing posts " . $offset . " - " . $showing . " of " . $totPosts;
}

function getPrevBtnCategory($categoryID, $limit = 10, $offset = 0) {
    $CI =& get_instance();
    $totSegments = $CI->uri->total_segments();
    $i = 1;
    $pagURL = "";
    while($i <= $totSegments) {
        if(!is_numeric($CI->uri->segment($i))) {
            $pagURL .= "/" . $CI->uri->segment($i);
        }
        $i++;
    }
    $CI->db->from('hcms_post');
    $CI->db->where('categoryID', $categoryID);
    $CI->db->where('published', 1);
    $query = $CI->db->get();
    $totPosts = $query->num_rows();
    $showing = $offset + $limit;
    if($showing > $totPosts) {
        $showing = $totPosts;
    }

    $prevNum = $offset - $limit;
    if($prevNum < 0) {
        $prevNum = 0;
    }
    if($prevNum < $offset) {
        echo '<a href="' . BASE_URL . $pagURL . '/' . $prevNum . '" class="btn btn-success float-left">Previous</a>';
    }
}

function getNextBtnCategory($categoryID, $limit = 10, $offset = 0) {
    $CI =& get_instance();
    $totSegments = $CI->uri->total_segments();
    $i = 1;
    $pagURL = "";
    while($i <= $totSegments) {
        if(!is_numeric($CI->uri->segment($i))) {
            $pagURL .= "/" . $CI->uri->segment($i);
        }
        $i++;
    }
    $CI->db->from('hcms_post');
    $CI->db->where('published', 1);
    $CI->db->where('categoryID', $categoryID);
    $query = $CI->db->get();
    $totPosts = $query->num_rows();
    $showing = $offset + $limit;
    if($showing > $totPosts) {
        $showing = $totPosts;
    }
    $offset++;
    $nextNum = $offset + $limit;
    if($nextNum > $totPosts) {
    } elseif($nextNum <= $totPosts) {
        $nextNum--;
        echo '<a href="' . BASE_URL . $pagURL . '/' . $nextNum . '" class="btn btn-success float-right">Next</a>';
    }
}

function getPrevBtn($limit = 10, $offset = 0) {
    $CI =& get_instance();
    $totSegments = $CI->uri->total_segments();
    $i = 1;
    $pagURL = "";
    while($i <= $totSegments) {
        if(!is_numeric($CI->uri->segment($i))) {
            if($CI->uri->segment($i) != "sayfa") {
                $pagURL .= "/" . $CI->uri->segment($i);
            }
        }
        $i++;
    }
    $CI->db->where('published', 1);
    $CI->db->from('hcms_post');
    $query = $CI->db->get();
    $totPosts = $query->num_rows();
    $showing = $offset + $limit;
    if($showing > $totPosts) {
        $showing = $totPosts;
    }

    $prevNum = $offset - $limit;
    if($prevNum < 0) {
        $prevNum = 0;
    }
    if($prevNum < $offset) {

        echo '   <li class="page-item"><a class="page-link" href="' . BASE_URL . $pagURL . '/sayfa/' . $prevNum . '"> &lt;</a></li>';

    }
}

function getNextBtn($limit = 10, $offset = 0) {
    $CI =& get_instance();
    $totSegments = $CI->uri->total_segments();
    $i = 1;
    $pagURL = "";
    while($i <= $totSegments) {
        if(!is_numeric($CI->uri->segment($i))) {
            if($CI->uri->segment($i) != "sayfa") {
                $pagURL .= "/" . $CI->uri->segment($i);
            }
        }
        $i++;
    }
    $CI->db->where('published', 1);
    $CI->db->from('hcms_post');
    $query = $CI->db->get();
    $totPosts = $query->num_rows();
    $showing = $offset + $limit;
    if($showing > $totPosts) {
        $showing = $totPosts;
    }
    $offset++;
    $nextNum = $offset + $limit;
    if($nextNum > $totPosts) {
    } elseif($nextNum <= $totPosts) {
        $nextNum--;
        echo '<li class="page-item"><a class="page-link" href="' . BASE_URL . $pagURL . '/sayfa/' . $nextNum . '">&gt; </a></li>';
    }
}

//Get the Latest news for the main column
function getCategoryProducts($categoryID, $limit = 10, $offset = 0) {
    $CI =& get_instance();
    $CI->db->order_by("postOrder", "asc");
    $CI->db->limit($limit, $offset);
    if($categoryID != 0) {
        $CI->db->where('hcms_post.categoryID', $categoryID);
    }
    $CI->db->where('published', 1);
    $CI->db->join('hcms_post_category', 'hcms_post_category.categoryID = hcms_post.categoryID');
    $query = $CI->db->get('hcms_post');
    $posts = '';
    foreach($query->result_array() as $c):

        $date = new DateTime($c['datePosted']);
        $posts .= ' <div class="col-lg-4 col-md-6 col-12 m-b3"> <a href="' . BASE_URL . '/kategori/' . $c['categorySlug'] . '/urun/' . $c['postURL'] . '" class="mx-mini-box">';

        $posts .= '<figure><img class="img-fluid" src="' . $c['postImage'] . '" alt="' . $c['postTitle'] . '"/></figure>';
        $posts .= '<div class="mini-box-txt"><h5>' . $c['postTitle'] . '</h5>';
        $posts .= '<p>' . $c['postExcerpt'] . '</p>';
        $posts .= '  <p class="more">Detaylı Bilgi</p></div>';

        $posts .= '</a></div>';

    endforeach;
    echo $posts;
}


function getCategoryProductCount($categoryID) {
    $CI =& get_instance();
    $CI->db->order_by("unixStamp", "desc");
    if($categoryID != 0) {
        $CI->db->where('categoryID', $categoryID);
    }
    $CI->db->where('published', 1);
    $query = $CI->db->get('hcms_post');
    $posts = '';
    echo $query->num_rows();

}


function getCarousel($id)
{
    $CI =& get_instance();
    $CI->db->order_by("slideOrder", "asc");
    $CI->db->where("pageID", $id);
    $query = $CI->db->get('hcms_banner');
    $m = "";
    if ($CI->session->flashdata('msg')) {
        $m = ' <p class="frmalert">' . $CI->session->flashdata('msg') .

            ' ';
    }
    $s = 0;
    $carousel = '<div  class="carousel-inner" role="listbox">';
    foreach ($query->result_array() as $c):
        if ($s == 0) {
            $carousel .= '<div class="carousel-item active">' . "\r\n";
            if ($c['slideLink'] != "") {
                $carousel .= '<a target="_blank" href="' . $c['slideLink'] . '">' . "\r\n";
            }
            $carousel .= '<img class="w-100" src="' . $c['slideImage'] . '" alt="' . $c['slideAlt'] . '">' . "\r\n";
            if ($c['slideLink'] != "") {
                $carousel .= '</a>' . "\r\n";
            }
            $carousel .= '              <div class="mmx-form wow fadeInRightBig d-none d-lg-block d-md-none d-sm-none">

                               ' . $m . '

                                <h1 class="text-center ">Ücretsiz Keşif Talebi</h1>
                                <p class="font12 ">Ücretsiz keşif için sizi ziyaret edelim, size en uygun sistemi
                                    sunalım.</p>
                                <form action="/iletisim-gonder/2" method="post" id="free-1">
                                    <div class="form-group">
                                        <input type="text" name="name" placeholder="Adınız & Soyadınız" required
                                               class="form-control">
                                    </div>
                                             <div class="form-group">
                                        <input type="text" name="phone" placeholder="İrtibat Numaranız" required
                                               class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <textarea name="message" rows="3" cols="50" placeholder="Mesajınız"  class="form-control"></textarea>
                                        </textarea>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" name="defaultCheck1"
                                               id="defaultCheck1" required>
                                        <label class="form-check-label" for="defaultCheck1" data-toggle="modal"
                                               data-target="#privateModal">
                                            Kişisel verilerin
                                            korunması kanununu okudum, kabul ediyorum.
                                        </label>
                                    </div>

                                    <div class="form-group text-center">
                                        <input type="submit" value="Ücretsiz Teklif" class="btn-submit btn-danger">
                                    </div>
                                    <p class="safe"><i class="fas fa-user-shield"></i>Bilgileriniz bizimle güvende</p>

                                </form>
                            </div>
                            <div class="carousel-caption ">

                                <h3 class="wow fadeInRight d-none d-md-block">Yangın ve Güvenlik Sistemleri </h3>
                                <p class="wow fadeInLeft d-none d-md-block">konularında 16 Yıllık sektör tecrübemiz ile
                                    hizmetinizdeyiz</p>

                                <p class="wow fadeInRightBig mt-5 d-none">
                                    <input value="Ücretsiz Keşif Talep Formu" type="button" class="btn-danger w-50 p-2">
                                </p>
                                <a href="#" class="free-cap d-lg-none" data-toggle="modal" data-target="#formModal">
                                    Ücretsiz Keşif</a>
                            </div></div>' . "\r\n";
        } else {
            $carousel .= '<div class="carousel-item">' . "\r\n";
            if ($c['slideLink'] != "") {
                $carousel .= '<a target="_blank" href="' . $c['slideLink'] . '">' . "\r\n";
            }
            $carousel .= '<img class="w-100" src="' . $c['slideImage'] . '" alt="' . $c['slideAlt'] . '">' . "\r\n";
            if ($c['slideLink'] != "") {
                $carousel .= '</a>' . "\r\n";
            }
            $carousel .= '     <div class="carousel-caption d-none d-md-block">
                                <h3 class="wow fadeInRight">SERVİS & BAKIM & PERİYODİK BAKIM HİZMETİ</h3>
                                <p class="wow fadeInLeft">Periyodik bakım, küçük sorunların büyüyerek, yüksek maliyetli
                                    sorunlara dönüşmesini önler.</p>
                            </div></div>' . "\r\n";
        }
        $s++;
    endforeach;
    $carousel .= "</div>";
    echo $carousel;
}

function getBanner($id) {
    $CI =& get_instance();
    $CI->db->order_by("slideOrder", "asc");
    $CI->db->where("pageID", $id);
    $query = $CI->db->get('hcms_banner')->result();

    return $query[0];


}


function convertWidget($widget) {

    $widget = ltrim($widget, "{{");
    $widget = rtrim($widget, "}}");

    $CI =& get_instance();

    $CI->db->select("content");
    $CI->db->where("title", $widget);
    $query = $CI->db->get('hcms_widgets')->row();
    if(!empty($query->content)) {
        return $query->content;
    } else {
        return "";
    }

}

//Get social
function getSocial() {
    $CI =& get_instance();
    $CI->db->where("socialEnabled", 1);
    $query = $CI->db->get('hcms_social');
    $social = '';
    foreach($query->result_array() as $c):
        $social .= '<a href="' . $c['socialLink'] . '" target="_blank"><span class="socicon socicon-' . $c['socialName'] . '"></span></a>';
    endforeach;
    echo $social;
}


function breadcrumb($url) {

    $output = '<ul class="mmx-bread-crumb"> <li>
                        <a href="/">Anasayfa</a>
                    </li>';

    foreach($url->segments as $segment) {
        $output .= '<li><a href=" ">' . breadcrumb1($segment) . '</a></li>';
    }

    $output .= '</ul>';

    return $output;

}

function breadcrumb1($title) {
    $title = str_replace("-", " ", $title);

    return ucwords($title);
}

function referenslarimiz() {
    $dir = BASEPATH . "../../uploads/referenslarimiz";
    $files = scandir($dir);
    echo '<div class="carousel-inner">';
    if($handle = opendir(BASEPATH . "../../uploads/referenslarimiz")) {

        $i = 0;
        foreach($files as $file) {
            if($file != "." && $file != "..") {
                if($i % 6 == 0) {
                    $active = $i == 0 ? "active" : "";
                    echo '<div class="carousel-item ' . $active . '"/><div class="row">';
                }
                echo '<div class="col-md-2 col-4"> 
                <img src="/uploads/referenslarimiz/' . $file . '" class="img-fluid border-hover"/>
                </div>';
                if($i % 6 == 5) {
                    echo "</div></div>";
                }
                $i++;
            }

        }
        closedir($handle);
    }
    echo '</div>';
}

function referenslarimiz_sayfa() {
    $dir = BASEPATH . "../../uploads/referenslarimiz";
    $files = scandir($dir);

    foreach($files as $file) {

        $i = 1;
        if($file != "." && $file != "..") {
            echo ' <div class="col-lg-3 wow fadeInUp">
                            <div class="mmx-ref-box mmx-line-5">
                                <span> <img src="/uploads/referenslarimiz/' . $file . '" class="img-fluid"/></span>
                            </div>
                       </div>';
        }
    }


}

function tr_strtoupper($text) {
    $search = array("ç",
        "i",
        "ı",
        "ğ",
        "ö",
        "ş",
        "ü");
    $replace = array("Ç",
        "İ",
        "I",
        "Ğ",
        "Ö",
        "Ş",
        "Ü");
    $text = str_replace($search, $replace, $text);
    $text = strtoupper($text);

    return $text;
}

?>
