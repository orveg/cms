<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Hcms_default extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Hcms_page_model');
        $this->load->helper('hcms_page_helper');
        $this->load->library('session');
        $this->data['settings'] = $this->Hcms_page_model->getSettings();
        $this->data['socials'] = $this->Hcms_page_model->getSocials();
        define('SITE_NAME', $this->data['settings']['siteTitle']);
        define('THEME', $this->data['settings']['siteTheme']);
        define('THEME_FOLDER', BASE_URL . '/theme/' . THEME);
        $this->maintenanceMode = $this->data['settings']['siteMaintenance'];
        if (($this->maintenanceMode) && ($this->session->userdata('logged_in'))) {
            $this->maintenanceMode = FALSE;
        }
    }


    public function index()
    {

        if (!$this->maintenanceMode) {
            $totSegments = $this->uri->total_segments();
            if (!is_numeric($this->uri->segment($totSegments))) {
                $pageURL = $this->uri->segment($totSegments);
            } else {
                if (is_numeric($this->uri->segment($totSegments))) {
                    $pageURL = $this->uri->segment($totSegments - 1);
                }
            }
            if ($pageURL == "") {
                $pageURL = "home";
                $this->data['home_products'] = $this->Hcms_page_model->homeProducts($pageURL);
            }
            $this->data['page'] = $this->Hcms_page_model->getPage($pageURL);
            if ($this->data['page']['pageTemplate'] != "") {
                $this->data['header'] = $this->load->view('templates/header', $this->data, TRUE);
                $this->data['footer'] = $this->load->view('templates/footer', '', TRUE);
                $this->load->view('templates/' . $this->data['page']['pageTemplate'], $this->data);
            } else {
                $this->error();
            }
        } else {
            $this->maintenance();
        }
    }

    public function category()
    {
        if (!$this->maintenanceMode) {
            $this->load->library('pagination');
            $result_per_page = 6;

            $config['full_tag_open'] = '<ul class="pagination mmx-navigation">';
            $config['full_tag_close'] = '</ul>';

            $catSlug = $this->uri->segment(2);
            if ($catSlug != "tum-urunler") {

                $config['total_rows'] = $this->Hcms_page_model->countProduct($catSlug);
                $this->data['page'] = $this->Hcms_page_model->getCategory($catSlug);
                $this->data['categoryID'] = $this->data['page']["categoryID"];
                $config['base_url'] = BASE_URL . '/kategori/tum-urunler';
            } else {
                $config['total_rows'] = $this->Hcms_page_model->countProduct();
                $config['base_url'] = BASE_URL . '/kategori/tum-urunler';
                $this->data['page'] = array("pageTitle" => "Tüm Ürünler");
                $this->data['categoryID'] = 0;
            }
            $config['uri_segment'] = 3;
            $config['per_page'] = $result_per_page;

            $this->pagination->initialize($config);
            $this->data['header'] = $this->load->view('templates/header', $this->data, TRUE);
            $this->data['footer'] = $this->load->view('templates/footer', '', TRUE);
            $this->load->view('templates/category', $this->data);

        } else {
            $this->maintenance();
        }
    }

    public function search()
    {
        $this->data['page'] = $this->Hcms_page_model->getPage("arama");
        $this->data['search'] = $this->Hcms_page_model->getSearch($this->input->post("q"));
        $this->data['count'] = $this->Hcms_page_model->getSearchCount($this->input->post("q"));
        $this->data['q'] = $this->input->post("q");

        $this->data['header'] = $this->load->view('templates/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('templates/footer', '', TRUE);
        $this->load->view('templates/search', $this->data);
    }

    public function article()
    {
        if (!$this->maintenanceMode) {

            $articleURL = $this->uri->segment(4);

            $this->data['page'] = $this->Hcms_page_model->getArticle($articleURL);
            if ($this->data['page']['postID'] != "") {
                $this->data['header'] = $this->load->view('templates/header', $this->data, TRUE);
                $this->data['footer'] = $this->load->view('templates/footer', '', TRUE);
                $this->load->view('templates/article', $this->data);
            } else {
                $this->error();
            }
        } else {
            $this->maintenance();
        }
    }

    public function error()
    {
        $this->data['page']['pageTitle'] = "Oops, Error";
        $this->data['page']['pageDescription'] = "Oops, Error";
        $this->data['page']['pageKeywords'] = "Oops, Error";
        $this->data['page']['pageID'] = "0";
        $this->data['page']['pageTemplate'] = "error";
        $this->data['header'] = $this->load->view('templates/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('templates/footer', '', TRUE);
        $this->load->view('templates/' . $this->data['page']['pageTemplate'], $this->data);
    }

    public function maintenance()
    {
        $this->data['page']['pageTitle'] = "Maintenance Mode";
        $this->data['page']['pageDescription'] = "Maintenance Mode";
        $this->data['page']['pageKeywords'] = "Maintenance Mode";
        $this->data['page']['pageID'] = "0";
        $this->data['page']['pageTemplate'] = "maintenance";
        $this->data['header'] = $this->load->view('templates/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('templates/footer', '', TRUE);
        $this->load->view('templates/' . $this->data['page']['pageTemplate'], $this->data);
    }

    public function feed()
    {

        if (($this->uri->segment(2) == "atom") || ($this->uri->segment(2) == "rss")) {
            $posts = getFeedPosts();
            $this->load->library('feed');
            $feed = new Feed();
            $feed->title = SITE_NAME;
            $feed->description = SITE_NAME;
            $feed->link = BASE_URL;
            $feed->pubdate = date("m/d/y H:i:s", $posts[0]['unixStamp']);
            foreach ($posts as $post) {
                $feed->add($post['postTitle'], BASE_URL . '/article/' . $post['postURL'],
                    date("m/d/y H:i:s", $post['unixStamp']), $post['postExcerpt']);
            }
            $feed->render($this->uri->segment(2));
        } else {
            if ($this->uri->segment(2) == "json") {
                $posts = getFeedPosts();
                $json_posts = array();
                foreach ($posts as $post) {
                    $single_post = array('postTitle' => $post['postTitle'],
                        'postExcerpt' => $post['postExcerpt'],
                        'postDate' => date("m/d/y H:i:s", $post['unixStamp']),
                        'postURL' => BASE_URL . '/article/' . $post['postURL'],
                        'postContentHTML' => $post['postContentHTML'],
                        'postContentJSON' => json_decode($post['postContent']),);
                    array_push($json_posts, $single_post);
                }
                $response = array('status' => 'OK');

                $this->output->set_status_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($json_posts,
                        JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                    ->_display();
                exit;
            } else {
                $this->error();
            }
        }
    }

    public function contact_send()
    {

        $slug = $this->uri->segment(2);

        $this->load->library('form_validation');
        $this->load->library('email');

        $config = array();
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.yandex.com';
        $config['smtp_user'] = 'info@ersistem.com.tr';
        $config['smtp_pass'] = '191976Bd';
        $config['smtp_port'] = 587;
        $config['smtp_crypto'] = 'tls';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);


        //Set validation rules
        $this->form_validation->set_rules('name', 'İsim', 'trim|required');
        $this->form_validation->set_rules('phone', 'Telefon', 'trim|required');
        if ($slug == 1) {
            $this->form_validation->set_rules('message', 'Message', 'trim|required');
        }
        if ($slug == 3) {
            $config['upload_path'] = './uploads/cv';
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['max_size'] = 2222;
            $config['max_width'] = 1024;
            $config['max_height'] = 768;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('fileupload')) {

                $this->form_validation->set_error_delimiters('<p class="error">asdad', '</p>');
                redirect(BASE_URL . '/insan-kaynaklari/is-basvuru-formu');

            }

        }
        if ($this->form_validation->run() == FALSE) {

            //Validation failed
            if ($slug == 1) {
                redirect(BASE_URL . '/iletisim');
            } elseif ($slug == 3) {
                redirect(BASE_URL . '/insan-kaynaklari/is-basvuru-formu');
            } else {
                redirect(BASE_URL . '/');
            }

        } else {
            //Validation passed

            //Add the post
            $this->load->library('Sioen');
            $this->Hcms_page_model->concact_send($slug);


            $this->email->from($config['smtp_user'], 'info@ersistem.com.tr');
            $this->email->to($config['smtp_user']);
            if ($slug == 1) {
                $this->email->subject('İletişim Formundan Gelen');
            } elseif ($slug == 3) {
                $this->email->subject('İnsan Kaynakları Başvurusu');
            } else {
                $this->email->subject('Ücretsiz Keşif Talebi');
            }
            $message = "İsim: " . $this->input->post('name') . "<br>İletişim Bilgisi: " . $this->input->post('phone') . " <br> Mesaj:" . $this->input->post('message');
            $this->email->message($message);
            $this->email->send();

            //Return to post list
            $this->session->set_flashdata('msg', 'Mesajınız Başarıyla Bize Ulaştı');
            if ($slug == 1) {
                redirect(BASE_URL . '/iletisim');
            } elseif ($slug == 3) {
                redirect(BASE_URL . '/insan-kaynaklari/is-basvuru-formu');
            } else {
                $this->session->set_flashdata('msg',
                    'Talebiniz bize başarıyla ulaştı. Size en kısa sürede dönüş yapacağız.');
                redirect(BASE_URL . '/');
            }
        }

    }
}
