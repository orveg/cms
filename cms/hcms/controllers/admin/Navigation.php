<?php if(!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Navigation extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        define("HCMS_ADMIN", 1);
        $this->load->model('Hcms_model');
        $this->load->helper(array('admincontrol',
            'url',
            'hcms_admin',
            'form'));
        $this->load->library('session');
        define('LANG', $this->Hcms_model->getLang());
        $this->lang->load('admin', LANG);
        //Define what page we are on for nav
        $this->data['current'] = $this->uri->segment(2);
        define('SITE_NAME', $this->Hcms_model->getSiteName());
        define('THEME', $this->Hcms_model->getTheme());
        define('THEME_FOLDER', BASE_URL . '/theme/' . THEME);
        //check session exists
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
    }

    public function index()
    {
        $this->load->library('pagination');
        $result_per_page = 15;  // the number of result per page
        $config['base_url'] = BASE_URL . '/admin/navigation/';
        $config['total_rows'] = $this->Hcms_model->countNavigation();
        $config['per_page'] = $result_per_page;

        $this->pagination->initialize($config);

        //Get pages from database
        $this->data['nav'] = $this->Hcms_model->getAllNav($result_per_page, $this->uri->segment(3));
        $this->load->helper('form');
        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/navigation/navigation', $this->data);
    }

    public function newNav()
    {
        //Get pages from database
        $this->data['pages'] = $this->Hcms_model->getPagesAll();
        $this->data['posts'] = $this->Hcms_model->getPostsAll();
        $this->data['categories'] = $this->Hcms_model->getCategoryAll();
        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/navigation/nav_new', $this->data);
    }



    public function editNav()
    {
        //Get pages from database
        $this->data['pages'] = $this->Hcms_model->getPagesAll();
        $this->data['posts'] = $this->Hcms_model->getPostsAll();
        $this->data['categories'] = $this->Hcms_model->getCategoryAll();
        //Get navigation from database
        $this->data['nav'] = $this->Hcms_model->getNav($this->uri->segment(4));
        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/navigation/nav_edit', $this->data);
    }

    public function navAdd()
    {
        //Get navigation from database
        if($this->uri->segment(4) == "page") {
            $this->data['page'] = $this->Hcms_model->getPageNav($this->uri->segment(3));
        }
        if($this->uri->segment(4) == "post") {
            $this->data['page'] = $this->Hcms_model->getPostUrl($this->uri->segment(3));
        }
        if($this->uri->segment(4) == "category") {
            $this->data['page'] = $this->Hcms_model->getCat($this->uri->segment(3));
        }


        //Load the view
        $this->load->view('admin/navigation/nav_add', $this->data);
    }

    public function insert()
    {
        //Load the form validation library
        $this->load->library('form_validation');

        $this->form_validation->set_rules('navSlug', 'nav slug', 'trim|alpha_dash|required|max_length[10]|is_unique[hmcs_navigation.navSlug]');
        $this->form_validation->set_rules('navTitle', 'navigation title', 'trim|required');

        if($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->newNav();
        } else {
            //Validation passed
            $this->Hcms_model->insertNav();
            //Return to navigation list
            redirect(BASE_URL . '/admin/navigation/navigation', 'refresh');
        }

    }


    public function update()
    {
        //Load the form validation library
        $this->load->library('form_validation');

        $this->form_validation->set_rules('navTitle', 'navigation title', 'trim|required');

        if($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->editNav();
        } else {
            //Validation passed

            $this->Hcms_model->updateNav($this->uri->segment(4));
            //Return to navigation list
            redirect(BASE_URL . '/admin/navigation/navigation', 'refresh');
        }
    }


    function deleteNav()
    {
        if($this->input->post('deleteid')):
            $this->Hcms_model->removeNav($this->input->post('deleteid'));
            redirect(BASE_URL . '/admin/navigation/navigation');
        else:
            $this->data['form'] = $this->Hcms_model->getNav($this->uri->segment(4));
            $this->load->view('admin/navigation/nav_delete.php', $this->data);
        endif;
    }
}
