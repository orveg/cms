<?php if(!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Contacts extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        define("HCMS_ADMIN", 1);
        $this->load->model('Hcms_model');
        $this->load->helper(array('admincontrol',
                                'url',
                                'hcms_admin',
                                'form'));
        $this->load->library('session');
        define('LANG', $this->Hcms_model->getLang());
        $this->lang->load('admin', LANG);
        //Define what page we are on for nav
        $this->data['current'] = $this->uri->segment(2);
        define('SITE_NAME', $this->Hcms_model->getSiteName());
        define('THEME', $this->Hcms_model->getTheme());
        define('THEME_FOLDER', BASE_URL . '/theme/' . THEME);
        //check session exists
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
    }

    public function index()
    {
        $this->load->library('pagination');
        $result_per_page = 20;  // the number of result per page
        $config['base_url'] = BASE_URL . '/admin/contacts/';
        $config['total_rows'] = $this->Hcms_model->countContact();
        $config['per_page'] = $result_per_page;
        $this->pagination->initialize($config);
        //Get pages from database
        $this->data['contacts'] = $this->Hcms_model->getContacts($result_per_page, $this->uri->segment(3));

        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/contacts/contacts', $this->data);
    }

    public function contact()
    {


        //Load the view
        $this->data['contact'] = $this->Hcms_model->getContact($this->uri->segment(3));

        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/contacts/contact', $this->data);
    }



    function delete()
    {
        if($this->input->post('deleteid')):
            $this->Hcms_model->removeContact($this->input->post('deleteid'));
            redirect('/admin/contacts/contacts');
        else:
            $this->data['form'] = $this->Hcms_model->getContact($this->uri->segment(4));
            $this->load->view('admin/contacts/contact_delete.php', $this->data);
        endif;
    }

    function contactSearch()
    {
        $this->Hcms_model->pageSearch($this->input->post('term'));
    }


}
