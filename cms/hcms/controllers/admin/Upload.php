<?php if(!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Upload extends CI_Controller {

    function __construct() {
        parent::__construct();
        define("HCMS_ADMIN", 1);
        $this->load->model('Hcms_model');
        $this->load->helper(array('admincontrol',
                                'url',
                                'hcms_admin',
                                'file',
                                'form'));
        $this->load->library('session');
        define('LANG', $this->Hcms_model->getLang());
        $this->lang->load('admin', LANG);
        //Define what page we are on for nav
        $this->data['current'] = $this->uri->segment(2);
        define('SITE_NAME', $this->Hcms_model->getSiteName());
        define('THEME', $this->Hcms_model->getTheme());
        define('THEME_FOLDER', BASE_URL . '/theme/' . THEME);
        //check session exists
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
    }



    public function upload() {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 100;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            echo  json_encode($error);
        } else {
            $data = array('upload_data' => $this->upload->data());

            echo  json_encode($data);
        }
    }

    public function uploa1d()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $attachment = $this->input->post('attachment');
        $uploadedFile = $_FILES['attachment']['tmp_name']['file'];

        $path = $_SERVER["DOCUMENT_ROOT"] . '/images';
        $url = BASE_URL . '/images';

        // create an image name
        $fileName = $attachment['name'];

        // upload the image
        move_uploaded_file($uploadedFile, $path . '/' . $fileName);

        $this->output->set_output(json_encode(array('file' => array('url' => $url . '/' . $fileName,
                                                                    'filename' => $fileName))), 200, array('Content-Type' => 'application/json'));
    }


}
