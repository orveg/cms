<?php if(!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Admin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        define("HCMS_ADMIN", 1);
        $this->load->helper(array('admincontrol',
                                'url',
                                'hcms_admin',
                                'form'));
        $this->load->library('session');
        $this->load->model('Hcms_model');
        define('LANG', $this->Hcms_model->getLang());
        $this->lang->load('admin', LANG);
        define('SITE_NAME', $this->Hcms_model->getSiteName());
        define('THEME', $this->Hcms_model->getTheme());
        define('THEME_FOLDER', BASE_URL . '/theme/' . THEME);

    }

    public function index()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $this->data['current'] = $this->uri->segment(2);
        $this->data['contacts'] = $this->Hcms_model->getIndexContacts();
        $this->data['new_message'] = $this->Hcms_model->getCountContacts(0);
        $this->data['old_message'] = $this->Hcms_model->getCountContacts(1);
        $this->data['maintenaceActive'] = $this->Hcms_model->checkMaintenance();

        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/home', $this->data);
    }

    public function upload()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $attachment = $this->input->post('attachment');
        $uploadedFile = $_FILES['attachment']['tmp_name']['file'];

        $path = $_SERVER["DOCUMENT_ROOT"] . '/uploads';
        $url = BASE_URL . '/uploads';

        // create an image name
        $fileName = $attachment['name'];

        // upload the image
        move_uploaded_file($uploadedFile, $path . '/' . $fileName);

        $this->output->set_output(json_encode(array('file' => array('url' => $url . '/' . $fileName,
                                                                    'filename' => $fileName))), 200, array('Content-Type' => 'application/json'));
    }

    public function login()
    {
        $this->data['header'] = $this->load->view('admin/login/headerlog', '', TRUE);
        $this->data['footer'] = $this->load->view('admin/login/footerlog', '', TRUE);
        $this->load->view('admin/login/login', $this->data);
    }

    public function loginCheck()
    {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password') . SALT);
        $result = $this->Hcms_model->login($username, $password);

        if($result) {
            redirect(BASE_URL . '/admin', 'refresh');
        } else {
            $this->data['error'] = "1";
            $this->login();
        }
    }

    function ajaxLogin()
    {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password') . SALT);
        $result = $this->Hcms_model->login($username, $password);
        if($result) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function logout()
    {
        $data = array('userID' => '',
                      'userName' => '',
                      'logged_in' => FALSE,);
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        $this->login();
    }


    public function settings()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $this->load->helper('directory');
        $this->data['themesdir'] = directory_map($_SERVER["DOCUMENT_ROOT"] . '/theme/', 1);
        $this->data['langdir'] = directory_map(APPPATH . '/language/', 1);

        $this->data['settings'] = $this->Hcms_model->getSettings();
        $this->data['current'] = $this->uri->segment(2);
        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/settings', $this->data);
    }

    public function updateSettings()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $path_upload = $_SERVER["DOCUMENT_ROOT"] . '/uploads/';
        $path_images = $_SERVER["DOCUMENT_ROOT"] . '/images/';
        if($this->input->post('siteLogo') != "") {
            rename($path_upload . $this->input->post('siteLogo'), $path_images . $this->input->post('siteLogo'));
        }
        if($this->input->post('siteFavicon') != "") {
            rename($path_upload . $this->input->post('siteFavicon'), $path_images . $this->input->post('siteFavicon'));
        }
        $this->Hcms_model->updateSettings();
        redirect(BASE_URL . '/admin', 'refresh');
    }

    public function uploadLogo()
    {

        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png|svg|ico';
        $this->load->library('upload', $config);
        foreach($_FILES as $key => $value) {

            if(!$this->upload->do_upload($key)) {
                $error = array('error' => $this->upload->display_errors());
                echo 0;
            } else {
                echo '"' . $this->upload->data('file_name') . '"';
            }
        }
    }

    public function filemanager()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));

        $this->data['current'] = $this->uri->segment(2);
        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/filemanager', $this->data);
    }

    public function social()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));


        $this->data['social'] = $this->Hcms_model->getSocial();
        $this->data['current'] = $this->uri->segment(2);
        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/social', $this->data);
    }

    public function updateSocial()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $this->Hcms_model->updateSocial();
        redirect(BASE_URL . '/admin/social', 'refresh');
    }

    public function checkSession()
    {
        if(!$this->session->userdata('logged_in')) {
            echo 0;
        } else {
            echo 1;
        }
    }

    public function complete()
    {
        unlink(FCPATH . "install/hoosk.sql");
        unlink(FCPATH . "install/index.php");
        redirect(BASE_URL . '/admin', 'refresh');
    }
}
