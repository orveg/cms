<?php if(!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pages extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        define("HCMS_ADMIN", 1);
        $this->load->model('Hcms_model');
        $this->load->helper(array('admincontrol',
                                'url',
                                'hcms_admin',
                                'file',
                                'form'));
        $this->load->library('session');
        define('LANG', $this->Hcms_model->getLang());
        $this->lang->load('admin', LANG);
        //Define what page we are on for nav
        $this->data['current'] = $this->uri->segment(2);
        define('SITE_NAME', $this->Hcms_model->getSiteName());
        define('THEME', $this->Hcms_model->getTheme());
        define('THEME_FOLDER', BASE_URL . '/theme/' . THEME);
        //check session exists
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
    }

    public function index()
    {
        $this->load->library('pagination');
        $result_per_page = 15;  // the number of result per page
        $config['base_url'] = BASE_URL . '/admin/pages/';
        $config['total_rows'] = $this->Hcms_model->countPages();
        $config['per_page'] = $result_per_page;
        $this->pagination->initialize($config);
        //Get pages from database
        $this->data['pages'] = $this->Hcms_model->getPages($result_per_page, $this->uri->segment(3));

        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/pages/pages', $this->data);
    }

    public function addPage()
    {
        //Load the view
        $this->data['templates'] = get_filenames('theme/' . THEME . '/templates');
        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/pages/page_new', $this->data);
    }

    public function confirm()
    {
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        $this->form_validation->set_rules('pageURL', 'page URL', 'trim|alpha_dash|required|is_unique[hcms_page_attributes.pageURL]');
        $this->form_validation->set_rules('pageTitle', 'page title', 'trim|required');
        $this->form_validation->set_rules('navTitle', 'navigation title', 'trim|required');

        if($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->addPage();
        } else {
            //Validation passed
            //Add the page
            $this->load->library('Sioen');
            $this->Hcms_model->createPage();
            //Return to page list
            redirect(BASE_URL . '/admin/pages/pages', 'refresh');
        }
    }

    public function editPage()
    {
        //Get page details from database
        $this->data['pages'] = $this->Hcms_model->getPage($this->uri->segment(4));
        //Load the view
        $this->data['templates'] = get_filenames('theme/' . THEME . '/templates');
        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/pages/page_edit', $this->data);
    }

    public function edited()
    {
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        if($this->uri->segment(4) != 1) {
            $this->form_validation->set_rules('pageURL', 'page URL', 'trim|alpha_dash|required|is_unique[hcms_page_attributes.pageURL.pageID.' . $this->uri->segment(4) . ']');
        }
        $this->form_validation->set_rules('pageTitle', 'page title', 'trim|required');
        $this->form_validation->set_rules('navTitle', 'navigation title', 'trim|required');

        if($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->editPage();
        } else {
            //Validation passed
            //Update the page
            $this->load->library('Sioen');
            $this->Hcms_model->updatePage($this->uri->segment(4));
            //Return to page list
            redirect(BASE_URL . '/admin/pages/pages', 'refresh');
        }
    }

    public function jumbo()
    {
        //Get page details from database
        $this->data['pages'] = $this->Hcms_model->getPage($this->uri->segment(4));
        $this->data['slides'] = $this->Hcms_model->getPageBanners($this->uri->segment(4));

        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/pages/jumbotron_edit', $this->data);
    }

    public function jumboAdd()
    {

        $this->load->library('Sioen');

        $this->Hcms_model->updateJumbotron($this->uri->segment(4));
        redirect(BASE_URL . '/admin/pages/pages', 'refresh');
    }


    function delete()
    {
        if($this->input->post('deleteid')):
            $this->Hcms_model->removePage($this->input->post('deleteid'));
            redirect('/admin/pages/pages');
        else:
            $this->data['form'] = $this->Hcms_model->getPage($this->uri->segment(4));
            $this->load->view('admin/pages/page_delete.php', $this->data);
        endif;
    }

    function pageSearch()
    {
        $this->Hcms_model->pageSearch($this->input->post('term'));
    }


}
