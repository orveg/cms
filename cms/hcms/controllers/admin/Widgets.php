<?php if(!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Widgets extends CI_Controller {

    function __construct() {
        parent::__construct();
        define("HCMS_ADMIN", 1);
        $this->load->model('Hcms_model');
        $this->load->helper(array('admincontrol',
                                'url',
                                'hcms_admin',
                                'file',
                                'form'));
        $this->load->library('session');
        define('LANG', $this->Hcms_model->getLang());
        $this->lang->load('admin', LANG);
        //Define what page we are on for nav
        $this->data['current'] = $this->uri->segment(2);
        define('SITE_NAME', $this->Hcms_model->getSiteName());
        define('THEME', $this->Hcms_model->getTheme());
        define('THEME_FOLDER', BASE_URL . '/theme/' . THEME);
        //check session exists
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
    }

    public function index()
    {
        $this->load->library('pagination');
        $result_per_page = 15;  // the number of result per page
        $config['base_url'] = BASE_URL . '/admin/widgets/';

        $this->data['widgets'] = $this->Hcms_model->getWidgets();

        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/widgets/widgets', $this->data);
    }

    public function addWidget()
    {
        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/widgets/widget_new', $this->data);
    }
    public function confirm()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        $this->form_validation->set_rules('title', 'title', 'trim|alpha_dash|required|is_unique[hcms_widgets.title]');
        $this->form_validation->set_rules('content', 'content', 'trim|required');



        if($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->addWidget();
        } else {
            //Validation passed
            //Add the user
            $this->Hcms_model->createWidget();
            //Return to user list
            redirect(BASE_URL . '/admin/widgets', 'refresh');
        }
    }

    public function editWidget()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        //Get user details from database
        $this->data['widget'] = $this->Hcms_model->getWidget($this->uri->segment(4));
        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, TRUE);
        $this->data['footer'] = $this->load->view('admin/footer', '', TRUE);
        $this->load->view('admin/widgets/widget_edit', $this->data);
    }
    public function edited()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        $this->form_validation->set_rules('title', 'title', 'trim|required|is_unique[hcms_widgets.title.id.' . $this->uri->segment(4) . ']');
        $this->form_validation->set_rules('content', 'content', 'trim|required');


        if($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->editWidget();
        } else {
            //Validation passed
            //Update the user
            $this->Hcms_model->updateWidget($this->uri->segment(4));
            //Return to user list
            redirect(BASE_URL . '/admin/widgets', 'refresh');
        }
    }

    function delete()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        if($this->input->post('deleteid')):
            $this->Hcms_model->removeWidget($this->input->post('deleteid'));
            redirect(BASE_URL . '/admin/widgets');
        else:
            $this->data['form'] = $this->Hcms_model->getWidget($this->uri->segment(4));
            $this->load->view('admin/widgets/widget_delete', $this->data);
        endif;
    }
}