<?php
//Navigation Bar
$lang['nav_dash'] = "Dashboard";
$lang['nav_content'] = "Ürünler";
$lang['nav_pages'] = "Sayfalar";
$lang['nav_pages_new'] = "Yeni Sayfa";
$lang['nav_pages_all'] = "Sayfalar";
$lang['nav_posts'] = "Ürünler";
$lang['nav_posts_new'] = "Yeni Ürün";
$lang['nav_posts_all'] = "Ürünler";
$lang['nav_categories_new'] = "Yeni Kategori";
$lang['nav_categories_all'] = "Kategoriler";
$lang['nav_users'] = "Kullanıcılar";
$lang['nav_filemanager'] = "Dosya Yöneticisi";
$lang['nav_users_new'] = "Yeni Kullanıcı";
$lang['nav_users_all'] = "Kullanıcılar";
$lang['nav_navigation'] = "Navigasyon";
$lang['nav_navigation_new'] = "Yeni Navigasyon Menüsü";
$lang['nav_navigation_all'] = "Menüler";
$lang['nav_settings'] = "Ayarlar";
$lang['nav_social'] = "Sosyal Ağlar";
$lang['nav_view_site'] = "Siteyi Gör";
$lang['nav_logout'] = "Çıkış";
$lang['nav_profile'] = "Profil";


//Login Page
$lang['login_message'] = "Tekrar hoşgeldin!";
$lang['login_username'] = "Kullanıcı Adı";
$lang['login_password'] = "Şifre";
$lang['login_signin'] = "Giriş";
$lang['login_reset'] = "Şifreyi Sıfırla";
$lang['login_incorrect'] = "Kullanıcı adı ve ya şifreniz yanlış";
$lang['login_expired'] = "Oturumunuzun süresi doldu, lütfen tekrar oturum açmak için aşağıdaki formu kullanın!";

//Forgot Password
$lang['forgot_reset'] = "Parola sıfırlama";
$lang['forgot_email'] = "Email Adres";
$lang['forgot_password'] = "Yeni Şifre";
$lang['forgot_confirm'] = "Şifreyi Onayla";
$lang['forgot_complete'] = "Şifreniz sıfırlandı";
$lang['forgot_btn'] = "Reset";
$lang['forgot_check_email'] = "Daha fazla talimat için lütfen e-postanızı kontrol edin";


//Dashboard
$lang['dash_welcome'] = "Hoşgeldiniz <small>Minerva</small>";
$lang['dash_unreachable'] = "Haber akışına ulaşılamıyor.";
$lang['dash_recent'] = "Son Güncellenen Sayfalar";
$lang['dash_maintenance_message'] = "Siteniz şu anda bakım modunda, bunu devre dışı bırakmak ve ziyaretçilerin sitenizi görüntülemesine izin vermek için ayarlar sayfasını ziyaret edin.";
$lang['dash_message'] = "Bu, Minerva gösterge tablonuzdur, Gezinme çubuğunu kullanarak web sitenize kullanıcılar, sayfalar veya gezinme menüleri ekleyebilir veya düzenleyebilirsiniz. Minerva, önyükleme çerçevesi etrafında inşa edilmiştir, bu web sitenizi kolaylıkla tam olarak yanıtlamalıdır.";
$lang['feed_heading'] = "Minerva News";


//Settings Page
$lang['settings_header'] = "Ayarlar";
$lang['settings_message'] = "Sitenin basit ayarlarını buradan yapabilirsiniz. ";
$lang['settings_info'] = "Bilgiler";
$lang['settings_name'] = "Site Adı*";
$lang['settings_footer'] = "Site alt mesajı";
$lang['settings_theme'] = "Seçili Tema:";
$lang['settings_lang'] = "Seçili Dil:";
$lang['settings_logo'] = "Site Logo";
$lang['settings_favicon'] = "Site Favicon";
$lang['settings_maintenance'] = "Bakım Modu";
$lang['settings_maintenance_heading'] = "Bakım Modu Başlık:";
$lang['settings_maintenance_meta'] = "Bakım Modu Açıklama";
$lang['settings_maintenance_content'] = "Bakım Sayfası İçeriği:";
$lang['settings_additional_js'] = "Additional Javascript (such as Google Analytics) this will be loaded into all pages, remember to include script tags where necessary:";
$lang['settings_image_error'] = "Hata! Dosya bir resim değil.";


//Page - All Pages
$lang['pages_header'] = "Sayfalar";
$lang['pages_table_page'] = "Sayfa";
$lang['pages_table_updated'] = "Güncelleme";
$lang['pages_table_created'] = "Oluşturma";
$lang['pages_table_published'] = "Paylaşılmış";
$lang['pages_delete'] = "Sayfayı sil ";
$lang['pages_delete_message'] = "Bu işlem kalıcıdır ve geri alınamaz…";
$lang['pages_delete_home'] = "Anasayfayı silemezsiniz";


//Page - New Page/Edit Page
$lang['pages_new_header'] = "Yeni Sayfa";
$lang['pages_edit_header'] = "Sayfa Düzenle";
$lang['pages_new_attributes'] = "Sayfa Özellikleri";
$lang['pages_new_required'] = "* İle işaretlenmiş alanların doldurulması zorunludur!";
$lang['pages_new_title'] = "Sayfa/Meta Başlık*";
$lang['pages_new_nav'] = "Navigasyon Başlık* (navigasyon menülerinde görüntülenir.)";
$lang['pages_new_keywords'] = "Meta Keywords";
$lang['pages_new_description'] = "Meta Açıklama";
$lang['pages_new_url'] = "Sayfa URL* (boşluk veya özel karakter girilemez)";
$lang['pages_new_publish'] = "Sayfayı Yayınla";
$lang['pages_new_template'] = "Sayfa Teması";


//Page - Jumbotron
$lang['pages_jumbo_intro'] = "\"Jumbotron\", mesajı, slider veya her ikisi için kullanılan sayfanın en üstünde büyük bir başlık alanıdır!";
$lang['pages_slider_header'] = "Resim Slider";
$lang['pages_slider_btn'] = " Resim Slider Ayarı";
$lang['pages_slider_enable'] = " Slider Aç?";
$lang['pages_slider_add'] = "Slider Ekle";
$lang['pages_slider_title'] = "Slide";
$lang['pages_slider_alt'] = "Resim Alt Tag";
$lang['pages_slider_link'] = "Resim Link";
$lang['pages_jumbo_header'] = "Jumbotron İçeriği";
$lang['pages_jumbo_header_att'] = "Jumbotron";
$lang['pages_jumbo_enable'] = "Jumbotron'u Etkinleştir?";
$lang['pages_jumbo_edit'] = "Jumbotron'u Düzenle";


//Posts - All Posts
$lang['posts_header'] = "Ürünler";
$lang['posts_table_post'] = "Ürün Başlık";
$lang['posts_table_category'] = "Kategori";
$lang['posts_table_posted'] = "Tarih";
$lang['posts_table_published'] = "Yayınlanıyor";
$lang['posts_delete'] = "İçeriği Sil";
$lang['posts_delete_message'] = "Bu işlem kalıcıdır ve geri alınamaz…";


//Posts - New Post/Edit Post
$lang['posts_new_header'] = "Yeni İçerik";
$lang['posts_edit_header'] = "İçerik Düzenle";
$lang['posts_new_attributes'] = "İçerik Özellikleri";
$lang['posts_new_required'] = "* İle işaretlenmiş alanların doldurulması zorunludur!";
$lang['posts_new_title'] = "Post/Meta Title*";
$lang['posts_new_feature'] = "Özellik Resmi";
$lang['posts_new_excerpt'] = "Ürün Kısa Açıklama*";
$lang['posts_new_category'] = "Kategori";
$lang['posts_new_url'] = "Post URL* (boşluk veya özel karakter girilemez)";
$lang['posts_new_date'] = "Tarih";
$lang['posts_new_published'] = "Sayfayı Yayınla?";

//Posts - All Categories
$lang['cat_header'] = "Kategoriler";
$lang['cat_table_category'] = "Kategori Başlık";
$lang['cat_delete'] = "Kategori sil ";
$lang['cat_delete_message'] = "Bu işlem kalıcıdır ve geri alınamaz…";


//Posts - New Category/Edit Category
$lang['cat_new_header'] = "Yeni Kategori";
$lang['cat_edit_header'] = "Kategori Düzenle";
$lang['cat_new_required'] = "* İle işaretlenmiş alanların doldurulması zorunludur!";
$lang['cat_new_title'] = "Kategori Başlık*";
$lang['cat_new_url'] = "Kategori URL Slug* (bu özel karakter veya boşluk içermemelidir)";
$lang['cat_new_description'] = "Kategori Açıklama";


//Users - All Users
$lang['user_header'] = "Kullanıcılar";
$lang['user_username'] = "Kullanıcı Adı";
$lang['user_email'] = "E-Posta Adresi";
$lang['user_delete'] = "Kullanıcı sil ";
$lang['user_delete_message'] = "Bunun tek kullanıcı hesabınız olmadığından emin olun";


//Users - New User/Edit User
$lang['user_new_header'] = "Yeni Kullanıcı";
$lang['user_edit_header'] = "Kullanıcı Düzenle";
$lang['user_new_username'] = "Kullanıcı Adı";
$lang['user_new_message'] = "Kullanıcı adınız giriş yapmak içindir ve değiştirilemez.";
$lang['user_new_email'] = "E-Posta Adresi";
$lang['user_new_pass'] = "Şifre";
$lang['user_new_confirm'] = "Şifre Tekrar";


//Navigation - All Menus
$lang['menu_header'] = "Menüler";
$lang['menu_table_title'] = "Menu Başlık";
$lang['menu_delete'] = "Menü Sil";
$lang['menu_delete_message'] = "Bu kalıcıdır ve geri alınamaz…";
$lang['menu_delete_message_header'] = "Başlık menüsünü silemezsiniz…";


//Navigation - New Menu/Edit Menu
$lang['menu_new_nav'] = "Navigasyon";
$lang['menu_new_pages'] = "Sayfalar";
$lang['menu_new_nav_slug'] = "Nav Slug* (maksimum 10 karakter, boşluk bırakılmamış)";
$lang['menu_new_nav_title'] = "Nav Başlık*";
$lang['menu_new_add_page'] = "Sayfa Ekle";
$lang['menu_new_select_page'] = "Sayfa Seç";
$lang['menu_new_custom_title'] = "Özel bağlantı başlığı";
$lang['menu_new_custom_link'] = "Özel bağlantı URL(include http://)";
$lang['menu_new_drop_down'] = " Açılır Menü Ekle";
$lang['menu_new_drop_title'] = "Başlık";
$lang['menu_new_drop_link'] = "URL Slug (bu URL’ye eklenir, boşluk bırakılmaz)";
$lang['menu_new_drop_error'] = "Açılan drop down  boşluk veya özel karakterler içeriyor";


//Social Page
$lang['social_header'] = "Sosyal Medya Bağlantıları";
$lang['social_message'] = "Burada, çeşitli sosyal hesaplarınızın bağlantılarını ayarlayabilirsiniz.</br>
Bu bağlantılar temanızın belirtilen alanında görünecektir.";
$lang['social_enable'] = "Bir simgeyi etkinleştirmek için yanındaki kutuyu işaretleyin.";
$lang['social_twitter'] = "Twitter";
$lang['social_facebook'] = "Facebook";
$lang['social_google'] = "Google+";
$lang['social_pinterest'] = "Pinterest";
$lang['social_foursquare'] = "Foursquare";
$lang['social_linkedin'] = "LinkedIn";
$lang['social_myspace'] = "Myspace";
$lang['social_soundcloud'] = "Soundcloud";
$lang['social_spotify'] = "Spotify";
$lang['social_lastfm'] = "LastFM";
$lang['social_youtube'] = "YouTube";
$lang['social_vimeo'] = "Vimeo";
$lang['social_dailymotion'] = "DailyMotion";
$lang['social_vine'] = "Vine";
$lang['social_flickr'] = "Flickr";
$lang['social_instagram'] = "Instagram";
$lang['social_tumblr'] = "Tumblr";
$lang['social_reddit'] = "Reddit";
$lang['social_envato'] = "Envato";
$lang['social_github'] = "Github";
$lang['social_tripadvisor'] = "TripAdvisor";
$lang['social_stackoverflow'] = "Stack Overflow";
$lang['social_persona'] = "Mozilla Persona";
$lang['social_table_title'] = "Social Site";
$lang['social_table_link'] = "Profile Link";
$lang['social_table_enabled'] = "Enabled?";


// COMMON BUTTONS
$lang['btn_save'] = "Kaydet";
$lang['btn_jumbotron'] = "Jumbotron";
$lang['btn_cancel'] = "İptal";
$lang['btn_delete'] = "Sil";
$lang['btn_next'] = "İlerle";
$lang['btn_back'] = "Geri";
$lang['btn_add'] = "Ekle";


$lang['option_yes'] = "Evet";
$lang['option_no'] = "Hayır";


// EMAILS
$lang['email_reset_subject'] = "Şifrenizi sıfırlayın";
$lang['email_reset_message'] = "Lütfen şifrenizi sıfırlamak için aşağıdaki bağlantıya tıklayın.";


// ERROR MESSAGES
$lang['no_results'] = "Sonuç bulunamadı.";
$lang['email_check'] = "Bu e-posta adresi mevcut değil.";
$lang['required'] = "The %s field is required.";
$lang['isset'] = "The %s field must have a value.";
$lang['valid_email'] = "The %s field must contain a valid email address.";
$lang['valid_emails'] = "The %s field must contain all valid email addresses.";
$lang['valid_url'] = "The %s field must contain a valid URL.";
$lang['valid_ip'] = "The %s field must contain a valid IP.";
$lang['min_length'] = "The %s field must be at least %s characters in length.";
$lang['max_length'] = "The %s field can not exceed %s characters in length.";
$lang['exact_length'] = "The %s field must be exactly %s characters in length.";
$lang['alpha'] = "The %s field may only contain alphabetical characters.";
$lang['alpha_numeric'] = "The %s field may only contain alpha-numeric characters.";
$lang['alpha_dash'] = "The %s field may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric'] = "The %s field must contain only numbers.";
$lang['is_numeric'] = "The %s field must contain only numeric characters.";
$lang['integer'] = "The %s field must contain an integer.";
$lang['regex_match'] = "The %s field is not in the correct format.";
$lang['matches'] = "The %s field does not match the %s field.";
$lang['is_unique'] = "This %s already exists.";
$lang['is_natural'] = "The %s field must contain only positive numbers.";
$lang['is_natural_no_zero'] = "The %s field must contain a number greater than zero.";
$lang['decimal'] = "The %s field must contain a decimal number.";
$lang['less_than'] = "The %s field must contain a number less than %s.";
$lang['greater_than'] = "The %s field must contain a number greater than %s.";
