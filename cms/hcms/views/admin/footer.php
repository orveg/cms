<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            &copy; <?php echo date("Y"); ?> <a target="_blank" href="http://mediaminerva.com/">Minerva Dashboard </a>. v1.0.0
        </div>
    </div>
</footer>
<!-- End of Footer -->
</div>
<!-- End of Content Wrapper -->
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Emin misiniz?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Oturumu sonlandırmak istediğinize emin misiniz?</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal"><?php echo $this->lang->line('btn_cancel'); ?></button>
                <a class="btn btn-primary" href="<?php echo BASE_URL; ?>/admin/logout"><?php echo $this->lang->line('nav_logout'); ?></a>
            </div>
        </div>
    </div>
</div>
<!-- remote modals -->
<div id="remote-modals">
    <div class="modal fade" id="ajaxModal" aria-labelledby="ajaxModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div> <!-- /.modal -->
</div>
<!-- Modal -->
<div id="loginModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo $this->lang->line('login_signin'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning"><p><?php echo $this->lang->line('login_expired'); ?></p></div>
                <div id="loginError" class='alert alert-danger'><?php echo $this->lang->line('login_incorrect'); ?></div>
                <div class="form-group">
                    <label for="username"><?php echo $this->lang->line('login_username'); ?></label>
                    <?php $data = array('name' => 'username',
                        'id' => 'username',
                        'class' => 'form-control',
                        'value' => set_value('username'),
                        'placeholder' => $this->lang->line('login_username'));

                    echo form_input($data); ?>
                </div>
                <div class="form-group">
                    <label for="password"><?php echo $this->lang->line('login_password'); ?>:</label>
                    <?php $data = array('name' => 'password',
                        'id' => 'password',
                        'class' => 'form-control',
                        'value' => set_value('password'),
                        'placeholder' => $this->lang->line('login_password'));

                    echo form_password($data); ?>
                </div>
            </div>
            <div class="modal-footer">
                <a id="ajaxLoginbtn" class="btn btn-primary"><?php echo $this->lang->line('login_signin'); ?></a>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo ADMIN_THEME; ?>/js/jquery.min.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/bootstrap/bootstrap.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/bootstrap/bootstrap.bundle.min.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/jquery-easing/jquery.easing.min.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/sb-admin-2.min.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/datatables.min.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/jquery.nestable.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/ckeditor/ckeditor.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/repeater.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/base.js"></script>
</body>

</html>
