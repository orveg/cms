<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">    <?php echo $this->lang->line('user_header'); ?></h1>
            </div>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li class="active">
                    <a href="<?php echo BASE_URL; ?>/admin/users"><?php echo $this->lang->line('user_header'); ?></a>
                </li>
            </ol>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">
                <?php echo $this->lang->line('user_header'); ?>
            </h6>

            <div class="dropdown no-arrow">
                <a class="dropdown-toggle" href="/admin/users/new">
                    <i class="fas fa-user-plus fa-sm fa-fw text-gray-900"></i>
                </a>

            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th> <?php echo $this->lang->line('user_username'); ?> </th>
                        <th> <?php echo $this->lang->line('user_email'); ?> </th>
                        <th class="td-actions"></th>
                    </tr>
                    </thead>
                    <tbody id="pageContainer">
                    <?php foreach($users as $u) { ?>

                        <tr>
                            <td><?php echo $u['userName'] ?></td>
                            <td><?php echo $u['email'] ?></td>
                            <td>
                                <a href="<?= BASE_URL . '/admin/users/edit/' . $u['userID'] ?>" class="btn btn-small btn-success">
                                    <i class="fas fa-fw fa-pencil-alt"></i>
                                </a>
                                <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small delete" href="<?= BASE_URL . '/admin/users/delete/' . $u['userID'] ?>">
                                    <i class="fas fa-fw fa-eraser"></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <div class="text-center" id="loadingSpinner">
                    <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
                </div>
                <div id="paginationContainer">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
