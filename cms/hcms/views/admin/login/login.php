<?php echo $header ?>
    <div class="row justify-content-center">
        <div class="col-xl-6 col-lg-6 col-md-6">
            <div class="col-lg-12 p-5">
                <a class="text-white-50 text-center d-flex align-items-center justify-content-center" href="<?php echo BASE_URL; ?>/admin">
                    <div class="sidebar-brand-text mx-3"><img style="width: 75%;" src="http://cms.orveg.com/theme/ersistem/images/ersistem-white-logo.svg" class="fo-logo"></div>
                </a>
            </div>
            <div class="card o-hidden border-0 shadow-lg my-2">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="p-5">
                                <?php if(isset($error)) {
                                    if($error == "1") {
                                        echo "<div class='alert alert-danger'>" . $this->lang->line('login_incorrect') . "</div>";
                                    }
                                } ?>
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4"><?php echo $this->lang->line('login_message'); ?></h1>
                                </div>
                                <?php echo form_open(BASE_URL . '/admin/login/check', 'class="user"'); ?>
                                <div class="form-group">

                                    <?php $data = array('name' => 'username',
                                        'id' => 'username',
                                        'class' => 'form-control form-control-user',
                                        'value' => set_value('username'),
                                        'placeholder' => $this->lang->line('login_username'));

                                    echo form_input($data); ?>

                                </div>
                                <div class="form-group">
                                    <?php $data = array('name' => 'password',
                                        'id' => 'password',
                                        'class' => 'form-control form-control-user',
                                        'value' => set_value('password'),
                                        'placeholder' => $this->lang->line('login_password'));

                                    echo form_password($data); ?>
                                </div>
                                <!--        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Remember Me</label>
                                            </div>
                                        </div>-->
                                <input type="submit" class="btn btn-success btn-block" value="<?php echo $this->lang->line('login_signin'); ?>">
                                <?php echo form_close() ?>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="<?php echo BASE_URL; ?>/admin/user/forgot"><?php echo $this->lang->line('login_reset'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo $footer ?>
