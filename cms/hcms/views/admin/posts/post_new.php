<?php echo $header; ?>
<link href="<?php echo ADMIN_THEME; ?>/js/trevor/sir-trevor.css" rel="stylesheet">
<link href="<?php echo ADMIN_THEME; ?>/js/trevor/sir-trevor-bootstrap.css" rel="stylesheet">
<link href="<?php echo ADMIN_THEME; ?>/js/trevor/sir-trevor-icons.css" rel="stylesheet">
<link href="<?php echo ADMIN_THEME; ?>/js/datepicker/jquery.datetimepicker.css" rel="stylesheet">
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">    <?php echo $this->lang->line('posts_new_header'); ?></h1>
            </div>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>/admin/posts"><?php echo $this->lang->line('nav_posts_all'); ?></a>
                </li>
                <li class="active">
                    <?php echo $this->lang->line('posts_new_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="card shadow mb-4">
        <?php echo form_error('postTitle', '<div class="alert alert-danger">', '</div>'); ?>
        <?php echo form_error('postURL', '<div class="alert alert-danger">', '</div>'); ?>
        <?php echo form_error('postExcerpt', '<div class="alert alert-danger">', '</div>'); ?>

        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">
                <?php echo $this->lang->line('posts_new_header'); ?>
            </h6>
        </div>


        <div class="card-body">

            <label class="mt-4">
                Ürün Sağ Bölüm Açıklama
            </label>
            <?php $data = array('id' => 'ckeditor1',
                'name' => 'postExcerptHTML',
                'class' => 'js-st-instance',);

            echo form_textarea($data, set_value('postExcerptHTML', $this->input->post('postExcerptHTML'), FALSE)); ?>
<br><br>
            <label class="mb-1">
                Ürün İçeriği Açıklama
            </label>
            <?php
            $attr = array('id' => 'contentForm');
            echo form_open(BASE_URL . '/admin/posts/new/add', $attr); ?>
            <?php $data = array('name' => 'content',
                'id' => 'ckeditor',
                'class' => 'js-st-instance',);

            echo form_textarea($data, set_value('content', $this->input->post('content'), FALSE)); ?>

        </div>
        <div class="card-footer">
            <a class="btn btn-primary" data-toggle="modal" href="#attributes"><?php echo $this->lang->line('btn_next'); ?></a>
            <a class="btn" href="<?php echo BASE_URL; ?>/admin/posts"><?php echo $this->lang->line('btn_cancel'); ?></a>
        </div>
    </div>


    <div id="attributes" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo $this->lang->line('posts_new_attributes'); ?></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="alert alert-info"><?php echo $this->lang->line('posts_new_required'); ?></div>
                    <div class="form-group">
                        <?php echo form_error('postTitle', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label" for="postTitle"><?php echo $this->lang->line('posts_new_title'); ?></label>
                        <div class="controls">
                            <?php $data = array('name' => 'postTitle',
                                'id' => 'postTitle',
                                'class' => 'form-control',
                                'value' => set_value('postTitle', '', FALSE));

                            echo form_input($data); ?>
                        </div> <!-- /controls -->
                    </div> <!-- /form-group -->
                    <div class="form-group">
                        <?php echo form_error('postImage', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label" for="file_upload"><?php echo $this->lang->line('posts_new_feature'); ?></label>
                        <div class="input-group">
                            <input name="postImage" class="form-control" id="postImage" type="text" value="">
                            <a href="javascript:open_popup('/filemanager/dialog.php?&amp;popup=1&amp;field_id=postImage&akey=<?= $this->config->config["encryption_key"] ?>')" class="btn" type="button">
                                Select
                            </a>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_error('postImage2', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label" for="postImage2">2. Resim</label>
                        <div class="input-group">
                            <input name="postImage2" class="form-control" id="postImage2" type="text" value="">
                            <a href="javascript:open_popup('/filemanager/dialog.php?&amp;popup=1&amp;field_id=postImage2&akey=<?= $this->config->config["encryption_key"] ?>')" class="btn" type="button">
                                Select
                            </a>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_error('postImage3', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label" for="postImage3">3. Resim</label>
                        <div class="input-group">
                            <input name="postImage3" class="form-control" id="postImage3" type="text" value="">
                            <a href="javascript:open_popup('/filemanager/dialog.php?&amp;popup=1&amp;field_id=postImage3&akey=<?= $this->config->config["encryption_key"] ?>')" class="btn" type="button">
                                Select
                            </a>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_error('postImage4', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label" for="postImage4">4. Resim</label>
                        <div class="input-group">
                            <input name="postImage4" class="form-control" id="postImage4" type="text" value="">
                            <a href="javascript:open_popup('/filemanager/dialog.php?&amp;popup=1&amp;field_id=postImage4&akey=<?= $this->config->config["encryption_key"] ?>')" class="btn" type="button">
                                Select
                            </a>
                        </div>
                    </div>


                    <div class="form-group">
                        <?php echo form_error('postExcerpt', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label" for="postExcerpt"><?php echo $this->lang->line('posts_new_excerpt'); ?></label>
                        <div class="controls">
                            <?php $data = array('name' => 'postExcerpt',
                                'id' => 'postExcerpt',
                                'class' => 'form-control',
                                'rows' => '4',);

                            echo form_textarea($data, set_value('postExcerpt', '', FALSE)); ?>

                        </div> <!-- /controls -->
                    </div> <!-- /form-group -->

                    <div class="form-group">
                        <?php echo form_error('postURL', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label" for="postURL"><?php echo $this->lang->line('posts_new_url'); ?></label>
                        <div class="controls">
                            <?php $data = array('name' => 'postURL',
                                'id' => 'postURL',
                                'class' => 'form-control URLField',
                                'value' => set_value('postURL', '', FALSE));

                            echo form_input($data); ?>

                        </div> <!-- /controls -->
                    </div> <!-- /form-group -->
                    <div class="form-group">
                        <?php echo form_error('categoryID', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label Color1" for="categoryID"><?php echo $this->lang->line('posts_new_category'); ?></label>
                        <div class="controls">
                            <?php
                            $att = 'id="categoryID" class="form-control"';
                            $data = array();
                            foreach($categories as $c) {
                                $data[$c['categoryID']] = $c['categoryTitle'];
                            }
                            echo form_dropdown('categoryID', $data, '0', $att); ?>
                        </div> <!-- /controls -->
                    </div> <!-- /form-group -->
                    <div class="form-group">
                        <?php echo form_error('published', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label" for="published"><?php echo $this->lang->line('posts_new_published'); ?></label>
                        <div class="controls">
                            <?php
                            $att = 'id="published" class="form-control"';
                            $data = array(1 => 'Yes',
                                0 => 'No');

                            echo form_dropdown('published', $data, '1', $att); ?>
                        </div> <!-- /controls -->
                    </div> <!-- /form-group -->
                    <div class="form-group">
                        <?php echo form_error('postOrder', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label" for="postOrder">Sıralama</label>
                        <div class="controls">
                            <?php $data = array('name' => 'postOrder',
                                                'id' => 'postOrder',
                                                'class' => 'form-control',
                                                'value' => set_value('postOrder', '0', FALSE));

                            echo form_input($data); ?>
                        </div> <!-- /controls -->
                    </div> <!-- /form-group -->
                    <div class="form-group">
                        <?php echo form_error('postHome', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label" for="postHome">Anasayfada Göster</label>
                        <div class="controls">
                            <?php $data = array('name'  => 'postHome',
                                                'id'    => 'postHome',
                                                'class' => 'form-control',
                                                'value' => set_value('postHome', "0", FALSE));

                            echo form_dropdown($data, array("0" => "Hayır",
                                                            "1" => "Evet"),0); ?>
                        </div> <!-- /controls -->
                    </div> <!-- /form-group -->
                    <div class="form-group">
                        <div id="datetimepicker1" class="input-append date">
                            <label class="control-label" for="categoryID"><?php echo $this->lang->line('posts_new_date'); ?></label>
                            <div class="controls">
                                <?php $data = array('name' => 'datePosted',
                                    'id' => 'datetimepicker',
                                    'class' => 'form-control',
                                    'value' => set_value('datePosted', '', FALSE));

                                echo form_input($data); ?>
                                <?php $data = array('name' => 'unixStamp',
                                    'id' => 'unixStamp',
                                    'style' => 'display:none;',
                                    'value' => set_value('unixStamp', '', FALSE));

                                echo form_input($data); ?>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo $this->lang->line('btn_back'); ?></button>
                    <a class="btn btn-primary" onClick="formSubmit()"><?php echo $this->lang->line('btn_save'); ?></a>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <!-- /span12 -->

    </div>
    <!-- /row -->
</div>


<?php echo $footer; ?>
<script src="<?php echo ADMIN_THEME; ?>/js/datepicker/jquery.datetimepicker.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/date.js"></script>
<script type="text/javascript">
    window.onload = function () {
        var now = new Date();
        var strDateTime = [[AddZero(now.getMonth() + 1), AddZero(now.getDate()), now.getFullYear()].join("/"), [AddZero(now.getHours()), AddZero(now.getMinutes()), AddZero(now.getSeconds())].join(":")].join(" ");
        document.getElementById("datetimepicker").value = strDateTime;
    };

    function AddZero(num) {
        return (num >= 0 && num < 10) ? "0" + num : num + "";
    }

    $(document).ready(function () {
        $("#postTitle").keyup(function () {
            $("#postURL").val(slugify($(this).val()));
        });
    });
    jQuery('#datetimepicker').datetimepicker({
        format: 'm/d/Y H:m:s'
    });

    CKEDITOR.replace('ckeditor', {
        filebrowserBrowseUrl: '/filemanager/<?=$this->config->config["encryption_key"]?>',
        filebrowserUploadUrl: '/filemanager/<?=$this->config->config["encryption_key"]?>',
        filebrowserImageBrowseUrl: '/filemanager/<?=$this->config->config["encryption_key"]?>',
        filebrowserWindowWidth: '1024',
        filebrowserWindowHeight: '800'
    });
    CKEDITOR.replace('ckeditor1', {
        filebrowserBrowseUrl: '/filemanager/<?=$this->config->config["encryption_key"]?>',
        filebrowserUploadUrl: '/filemanager/<?=$this->config->config["encryption_key"]?>',
        filebrowserImageBrowseUrl: '/filemanager/<?=$this->config->config["encryption_key"]?>',
        filebrowserWindowWidth: '1024',
        filebrowserWindowHeight: '800'
    });


    function formSubmit() {
        $.ajax({
            url: "/admin/check/session",
        }).done(function (data) {
            sessionExist = data;
            if (sessionExist == 0) {
                $('.modal').modal('hide');
                $('#loginModal').modal({
                    backdrop: 'static',
                    keyboard: false
                }).modal('show');
            } else {
                var unixtime = Date.parse(document.getElementById('datetimepicker').value).getTime() / 1000;
                document.getElementById("unixStamp").value = unixtime;
                document.getElementById("contentForm").submit();
            }
        });
    }

    $(function () {

        if (document.getElementById('file_upload')) {
            function prepareUpload(event) {
                files = event.target.files;
                uploadFiles(event);
            }

            function uploadFiles(event) {
                event.stopPropagation();
                event.preventDefault();

                $('#loading_pic').show();

                var data = new FormData();
                $.each(files, function (key, value) {
                    data.append(key, value);
                });

                $.ajax({
                    url: '<?php echo BASE_URL; ?>/admin/upload',
                    type: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function (data, textStatus, jqXHR) {
                        if (data != '0') {
                            $('#logo_preloaded').show();
                            document.getElementById('logo_preloaded').src = '<?php echo BASE_URL; ?>/uploads/' + data;
                            document.getElementById('postImage').value = data;
                            $('#loading_pic').hide();
                        } else
                            alert('Error! The file is not an image.');
                    }
                });
            }

            function submitForm(event, data) {
                $form = $(event.target);
                var formData = $form.serialize();
                $.each(data.files, function (key, value) {
                    formData = formData + '&filenames[]=' + value;
                });

                $.ajax({
                    url: '<?php echo BASE_URL; ?>/admin/settings/submit',
                    type: 'POST',
                    data: formData,
                    cache: false,
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        if (typeof data.error === 'undefined')
                            console.log('SUCCESS: ' + data.success);
                        else
                            console.log('ERRORS: ' + data.error);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                    },
                    complete: function () {
                        $('#loading_pic').hide();
                    }
                });
            }

            var files;
            $('input[type=file]').on('change', prepareUpload);
        }
    });
</script>

