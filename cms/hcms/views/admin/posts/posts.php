<?php echo $header; ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">
                        <?php echo $this->lang->line('posts_header'); ?>
                    </h1>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="input-group searchContainer">
                    <input type="text" class="form-control" id="searchString">
                    <span class="input-group-btn">
				<button class="btn btn-default" type="button" id="searchBtn" onClick="doPostSearch();"><span class="fa fa-search"></span></button>
			  </span>
                </div>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                    </li>
                    <li class="active">
                        <a href="<?php echo BASE_URL; ?>/admin/posts"><?php echo $this->lang->line('nav_posts_all'); ?></a>
                    </li>
                </ol>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                    <?php echo $this->lang->line('posts_header'); ?>
                </h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th> <?php echo $this->lang->line('posts_table_post'); ?> </th>
                            <th> <?php echo $this->lang->line('posts_table_category'); ?> </th>
                            <th> <?php echo $this->lang->line('posts_table_posted'); ?> </th>
                            <th> Sıralama </th>
                            <th> <?php echo $this->lang->line('posts_table_published'); ?> </th>
                            <th class="td-actions"></th>
                        </tr>
                        </thead>
                        <tbody id="pageContainer">
                        <?php foreach($posts as $p) { ?>

                            <tr>
                                <td style="width: 238px"><?php echo $p["postTitle"] ?></td>
                                <td><?php echo $p["categoryTitle"] ?></td>
                                <td class="text-center"><?php echo $p["datePosted"] ?></td>
                                <td class="text-center"><?php echo $p["postOrder"] ?></td>
                                <td><?php echo $p["published"] ? '<span class="fa fa-2x fa-check-circle"></span>' : '<span class="fa fa-2x fa-times-circle"></span>' ?></td>
                                <td>
                                    <a href="<?= BASE_URL . '/admin/posts/edit/' . $p['postID'] ?>" class="btn btn-small btn-success">
                                        <i class="fas fa-fw fa-pencil-alt"></i>
                                    </a>
                                    <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small delete" href="<?= BASE_URL . '/admin/posts/delete/' . $p['postID'] ?>">
                                        <i class="fas fa-fw fa-eraser"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <div class="text-center" id="loadingSpinner">
                        <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
                    </div>
                    <div id="paginationContainer">
                        <?php echo $this->pagination->create_links(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo $footer; ?>
