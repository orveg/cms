<?php echo $header; ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">    Mesajlar</h1>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="input-group searchContainer">
                    <input type="text" class="form-control" id="searchString" placeholder="Sayfalarda ara">
                    <span class="input-group-btn">
				<button class="btn btn-default" id="searchBtn" type="button" onClick="doPageSearch();"><span class="fa fa-search"></span></button>
			  </span>
                </div><!-- /input-group -->
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                    </li>
                    <li class="active">
                        <a href="<?php echo BASE_URL; ?>/admin/contacts">Mesajlar</a>
                    </li>
                </ol>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                    Mesajlar
                </h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th> <?php echo $this->lang->line('pages_table_created'); ?> </th>
                            <th> İsim </th>
                            <th> Tipi </th>
                            <th> Telefon </th>
                            <th class="td-actions"></th>
                        </tr>
                        </thead>
                        <tbody id="pageContainer">
                        <?php foreach($contacts as $p) { ?>

                            <tr style="<?=$p["status"] ? "" : "font-weight:bold" ?>">
                                <td><?php echo dateFormat($p["date"]) ?></td>
                                <td><?php echo $p["name"] ?></td>
                                <td><?php if($p["type"] == 1) {
                                       echo "İletişim Formu";
                                    } elseif($p["type"] == 2) {
                                        echo "Ücretsiz Keşif";
                                    } elseif($p["type"] == 3) {
                                        echo "Başvuru Formu";
                                    } ?></td>
                                <td><?php echo $p["phone"] ?></td>
                                <td>
                                    <a href="<?= BASE_URL . '/admin/contact/' . $p['id'] ?>" class="btn btn-small btn-success">
                                        <i class="fas fa-fw fa-pencil-alt"></i>
                                    </a>
                                    <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small delete" href="<?= BASE_URL . '/admin/contact/delete/' . $p['id'] ?>">
                                        <i class="fas fa-fw fa-eraser"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <div class="text-center" id="loadingSpinner">
                        <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
                    </div>
                    <div id="paginationContainer">
                        <?php echo $this->pagination->create_links(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /container -->
<?php echo $footer; ?>