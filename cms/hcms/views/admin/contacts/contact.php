<?php echo $header; ?>
<?php

if($contact["type"] == 1) {
    $a = "İletişim Formu";
} elseif($contact["type"] == 2) {
    $a = "Ücretsiz Keşif";
} elseif($contact["type"] == 3) {
    $a = "Başvuru Formu";
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Mesaj</h1>
            </div>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>/admin/contacts">Mesajlar</a>
                </li>
                <li class="active">
                    <?php echo $contact["name"] ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">
                Mesaj | <?php echo $a ?> |     <?php echo dateFormat($contact["date"]) ?>
            </h6>
            <div class="dropdown no-arrow">
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                </a>
            </div>
        </div>
        <div class="card-body mmx-message">
            <p>  <?php echo $contact["name"] ?></p>

           <p>
               <?php echo $contact["phone"] ?>
           </p>
            <p>
                <?php echo $contact["email"] ?>
            </p>
           <p>
               <?= $contact["message"] ?>
           </p>
            <p>
                <? if($contact["file"]) {?>
                   <a href="/uploads/cv/<?php echo $contact["file"] ?>"><?php echo $contact["file"] ?></a>
                <? } ?>

            </p>
        </div>
    </div>
</div>

<?php echo $footer;


?>

