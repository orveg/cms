<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('menu_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                	<a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li class="active">
                	<a href="<?php echo BASE_URL; ?>/admin/navigation"><?php echo $this->lang->line('menu_header'); ?></a>
                </li>
            </ol>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">
                <?php echo $this->lang->line('nav_header'); ?>
            </h6>
            <div class="dropdown no-arrow">
                <a class="dropdown-toggle" href="/admin/navigation/new">
                 Sayfa Ekle   <i class="fas fa-plus fa-sm fa-fw text-gray-800"></i>
                </a>

            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th> <?php echo $this->lang->line('menu_table_title'); ?> </th>
                        <th class="td-actions"> </th>
                    </tr>
                    </thead>
                    <tbody id="pageContainer">
                    <?php foreach($nav as $n) { ?>
                        <tr>
                            <td><?php echo $n['navTitle'] ?></td>
                            <td>
                                <a href="<?= BASE_URL . '/admin/navigation/edit/' . $n['navSlug'] ?>" class="btn btn-small btn-success">
                                    <i class="fas fa-fw fa-pencil-alt"></i>
                                </a>
                                <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small delete" href="<?= BASE_URL . '/admin/navigation/delete/' . $n['navSlug'] ?>">
                                    <i class="fas fa fa-fw fa-eraser"></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <div class="text-center" id="loadingSpinner">
                    <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
                </div>
                <div id="paginationContainer">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
