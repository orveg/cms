<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                <?php echo $this->lang->line('menu_new_nav'); ?>
            </h3>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li class="active">
                    <i class="fa fa-fw fa-list-alt"></i>
                    <a href="<?php echo BASE_URL; ?>/admin/navigation"><?php echo $this->lang->line('menu_header'); ?></a>
                </li>
                <li class="active">
                    <i class="fa fa-fw fa-pencil"></i>
                    <?php echo $this->lang->line('menu_new_pages'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <?php foreach($nav as $n) { ?>
            <div class="col-md-5">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">
                            <?php echo $this->lang->line('menu_new_pages'); ?>
                        </h6>
                    </div>
                    <div class="card-body">

                        <div class="form-group">

                            <?php $attr = array('id' => 'navForm');
                            echo form_open(BASE_URL . '/admin/navigation/update/' . $n['navSlug'], $attr); ?>

                            <label class="control-label" for="pageTitle"><?php echo $this->lang->line('menu_new_nav_slug'); ?></label>
                            <div class="controls add-spec-input">
                                <?php $data = array('name'     => 'navSlug',
                                                    'id'       => 'navSlug',
                                                    'class'    => 'form-control URLField disabled',
                                                    'disabled' => '',
                                                    'value'    => set_value('navSlug', $n['navSlug']));

                                echo form_input($data); ?>
                            </div> <!-- /controls -->
                        </div> <!-- /form-group -->

                        <div class="form-group">
                            <?php echo form_error('navTitle', '<div class="alert">', '</div>'); ?>
                            <label class="control-label" for="navTitle"><?php echo $this->lang->line('menu_new_nav_title'); ?></label>
                            <div class="controls">
                                <?php $data = array('name'  => 'navTitle',
                                                    'id'    => 'navTitle',
                                                    'class' => 'form-control',
                                                    'value' => set_value('navTitle', $n['navTitle']));

                                echo form_input($data); ?>
                            </div> <!-- /controls -->
                        </div> <!-- /form-group -->

                        <div>
                            <hr/>
                            <h5 class="Color1">Sayfa Ekle</h5>
                            <hr/>
                            <div class="form-group">
                                <label class="control-label" for="pagesList"><?php echo $this->lang->line('menu_new_select_page'); ?></label>
                                <div class="controls">

                                    <?php $att = 'id="pagesList" class="form-control"';
                                    $data = array();
                                    foreach($pages as $p) {
                                        $data[$p['pageURL']] = $p['navTitle'];
                                    }
                                    echo form_dropdown('pagesList', $data, '1', $att); ?>

                                </div> <!-- /controls -->
                            </div> <!-- /form-group -->
                            <div class="form-group">
                                <div class="controls">
                                    <button type="button" class="btn btn-primary" onClick="addNav()"><?php echo $this->lang->line('btn_add'); ?></button>
                                </div> <!-- /controls -->
                            </div> <!-- /form-group -->

                        </div>
                        <div>
                            <hr/>
                            <h5 class="Color1">Kategori Ekle</h5>
                        
                            <div class="form-group">
                                <label class="control-label" for="categoriesList"><?php echo $this->lang->line('menu_new_select_page'); ?></label>
                                <div class="controls">

                                    <?php $att = 'id="categoriesList" class="form-control"';
                                    $data = array();
                                    foreach($categories as $p) {
                                        $data[$p['categorySlug']] = $p['categoryTitle'];
                                    }
                                    echo form_dropdown('categoriesList', $data, '1', $att); ?>

                                </div> <!-- /controls -->
                            </div> <!-- /form-group -->
                            <div class="form-group">
                                <div class="controls">
                                    <button type="button" class="btn btn-primary" onClick="addNavCat()"><?php echo $this->lang->line('btn_add'); ?></button>
                                </div> <!-- /controls -->
                            </div> <!-- /form-group -->
                            <hr/>
                        </div>
                        <div>
                            <h5 class="Color1">Ürün Sayfası Ekle</h5>

                            <div class="form-group">
                                <label class="control-label" for="postsList"><?php echo $this->lang->line('menu_new_select_page'); ?></label>
                                <div class="controls">

                                    <?php $att = 'id="postsList" class="form-control"';
                                    $data = array();
                                    foreach($posts as $p) {
                                        $data[$p['postURL']] = $p['postTitle'];
                                    }
                                    echo form_dropdown('postsList', $data, '1', $att); ?>

                                </div> <!-- /controls -->
                            </div> <!-- /form-group -->
                            <div class="form-group">
                                <div class="controls">
                                    <button type="button" class="btn btn-primary" onClick="addNavPost()"><?php echo $this->lang->line('btn_add'); ?></button>
                                </div> <!-- /controls -->
                            </div> <!-- /form-group -->
                        </div>



                        <div class="form-group d-none">
                            <label class="control-label" for="customlinkTitle"><?php echo $this->lang->line('menu_new_custom_title'); ?></label>
                            <div class="controls">

                                <input type="text" id="customlinkTitle" value="" class="form-control"/>

                            </div> <!-- /controls -->
                        </div> <!-- /form-group -->
                        <div class="form-group d-none">
                            <label class="control-label" for="customURLHREF"><?php echo $this->lang->line('menu_new_custom_link'); ?></label>
                            <div class="controls">

                                <input type="text" id="customURLHREF" value="http://" class="form-control"/>

                            </div> <!-- /controls -->
                        </div> <!-- /form-group -->
                        <div class="form-group d-none">
                            <div class="controls">
                                <button type="button" class="btn btn-primary" onClick="addCustomURL()"><?php echo $this->lang->line('btn_add'); ?></button>
                            </div> <!-- /controls -->
                        </div> <!-- /form-group -->
                        <hr/>
                        <h5 class="Color1"><?php echo $this->lang->line('menu_new_drop_down'); ?></h5>

                        <div class="form-group">
                            <label class="control-label" for="parent"><?php echo $this->lang->line('menu_new_drop_title'); ?></label>
                            <div class="controls">
                                <input type="text" id="parentTitle" value="" class="form-control"/>
                            </div> <!-- /controls -->
                        </div> <!-- /form-group -->
                        <div class="form-group d-none">
                            <label class="control-label" for="parent"><?php echo $this->lang->line('menu_new_drop_link'); ?></label>
                            <div class="controls">
                                <input type="text" id="parentSlug" value="" class="form-control URLField"/>
                            </div> <!-- /controls -->
                        </div> <!-- /form-group -->
                        <div class="form-group  ">
                            <div class="controls">
                                <button type="button" class="btn btn-primary" onClick="addDropDown()"><?php echo $this->lang->line('btn_add'); ?></button>
                            </div> <!-- /controls -->
                        </div> <!-- /form-group -->
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">
                            <?php echo $this->lang->line('menu_new_nav'); ?>
                        </h6>
                    </div>
                    <div class="card-body">
                        <div class="dd" id="navHolder">
                            <ul class="dd-list" id="mainNav">
                                <?php echo $n['navEdit'] ?>
                            </ul>
                        </div>
                    </div>
                    <div class="card-footer">
                        <input type="hidden" id="seriaNav" name="seriaNav"/>
                        <input type="hidden" name="convertedNav" id="convertedNav"/>
                        <input type="hidden" name="convertedJson" id="convertedJson"/>
                        <div class="controls">
                            <button type="button" class="btn btn-primary" onClick="serializeNav()"><?php echo $this->lang->line('btn_save'); ?></button>
                        </div> <!-- /controls -->
                        <?php echo form_close() ?>
                    </div> <!-- /form-group -->

                </div>
                <!-- /widget -->
            </div>
        <?php } ?>
    </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#parentTitle").keyup(function () {
            $("#parentSlug").val(slugify($(this).val()));
        });
    });

    function addNav() {
        var navHolder = document.getElementById("mainNav").innerHTML;
        var navSelected = $('#pagesList').val();
        $.ajax({
            url: "<?php echo BASE_URL; ?>/admin/navadd/" + navSelected + "/page",
            type: "POST",
            success: function (html) {
                var navContainer = $('#navContainer'); //jquery selector (get element by id)
                if (html) {
                    document.getElementById("mainNav").innerHTML += html;
                }
            },
            error: function (html) {
                alert('error');
            }
        });

    }

    function addNavCat() {
        var navHolder = document.getElementById("mainNav").innerHTML;
        var navSelected = $('#categoriesList').val();
        $.ajax({
            url: "<?php echo BASE_URL; ?>/admin/navadd/" + navSelected + "/category",
            type: "POST",
            success: function (html) {
                var navContainer = $('#navContainer'); //jquery selector (get element by id)
                if (html) {
                    document.getElementById("mainNav").innerHTML += html;
                }
            },
            error: function (html) {
                alert('error');
            }
        });

    }

    function addNavPost() {
        var navHolder = document.getElementById("mainNav").innerHTML;
        var navSelected = $('#postsList').val();
        $.ajax({
            url: "<?php echo BASE_URL; ?>/admin/navadd/" + navSelected + "/post",
            type: "POST",
            success: function (html) {
                var navContainer = $('#navContainer'); //jquery selector (get element by id)
                if (html) {
                    document.getElementById("mainNav").innerHTML += html;
                }
            },
            error: function (html) {
                alert('error');
            }
        });

    }

    function addCustomURL() {
        var navHolder = document.getElementById("mainNav").innerHTML;
        var customlinkTitle = document.getElementById("customlinkTitle").value;
        var customURLHREF = document.getElementById("customURLHREF").value;
        if (customlinkTitle != "") {
            newLink = "<li class='dd-item' data-href='" + customURLHREF + "' data-title='" + customlinkTitle + "' data-type='1'><a class='right' onclick='var li = this.parentNode; var ul = li.parentNode; ul.removeChild(li);'><i class='fa fa-times'></i></a><div class='dd-handle'>" + customlinkTitle + "</div></li>";
            document.getElementById("mainNav").innerHTML += newLink;
        }
    }

    function addDropDown() {
        var navHolder = document.getElementById("mainNav").innerHTML;
        var parentTitle = document.getElementById("parentTitle").value;
        var parentSlug = document.getElementById("parentSlug").value;
        var regexp = /^[a-zA-Z0-9-_]+$/;
        if (parentSlug.search(regexp) == -1) {
            alert('<?php echo $this->lang->line('menu_new_drop_error'); ?>');
        } else {
            if (parentTitle != "" && parentSlug != "") {
                newLink = "<li class='dd-item parent' data-href='" + parentSlug + "' data-title='" + parentTitle + "'><a class='right' onclick='var li = this.parentNode; var ul = li.parentNode; ul.removeChild(li);'><i class='fa fa-times'></i></a><div class='dd-handle'>" + parentTitle + " <b class='caret dd-caret'></b></div></li>";
                document.getElementById("mainNav").innerHTML += newLink;
            }
        }
    }

    function updateOutput(e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    $(document).ready(function () {


        // activate Nestable for list 1
        $('.dd').nestable({
            group: 5,
            listNodeName: 'ul',
            maxDepth: 5,
        })
            .on('change', updateOutput);
        // output initial serialised data
        updateOutput($('.dd').data('output', $('#seriaNav')));
    });

    function serializeNav() {

        $.ajax({
            url: "/admin/check/session",
        }).done(function (data) {
            sessionExist = data;
            if (sessionExist == 0) {
                $('.modal').modal('hide');
                $('#loginModal').modal({
                    backdrop: 'static',
                    keyboard: false
                }).modal('show');
            } else {
                updateOutput($('.dd').data('output', $('#seriaNav')));
                var jsn = JSON.parse(document.getElementById('seriaNav').value);
                var parentHREF = '';
                var parseJsonAsHTMLTree = function (jsn) {
                    var result = '';

                    jsn.forEach(function (item) {
                        if (item.title && item.children) {
                            if (item.href != "") {
                                result += '<li><a href="' + item.href + '">' + item.title + '<span class="mmx-angle-down"></span></a><ul>';
                            }else {
                                result += '<li><a href="#">' + item.title + '<span class="mmx-angle-down"></span></a><ul>';
                            }

                            parentHREF = item.href;
                            result += parseJsonAsHTMLTree(item.children);
                            parentHREF = "";
                            result += '</ul></li>';
                        } else {
                            if (parentHREF == "") {
                                if (item.href != "home") {
                                    if (item.type != "1") {
                                        result += '<li><a href="/' + item.href + '">' + item.title + '</a></li>';
                                    } else {
                                        result += '<li><a href="/' + item.href + '">' + item.title + '</a></li>';
                                    }
                                } else {
                                    result += '<li><a href="/">' + item.title + '</a></li>';
                                }
                            } else {
                                if (item.type != "1") {
                                    result += '<li><a href="/' + parentHREF + "/" + item.href + '">' + item.title + '</a></li>';
                                } else {
                                    result += '<li><a href="/' + item.href + '">' + item.title + '</a></li>';
                                }
                            }
                        }
                    });

                    return result + '';
                };

                var result = '<ul class="slimmenu">' + parseJsonAsHTMLTree(jsn) + '</ul>';
                document.getElementById('convertedNav').value = result;
                document.getElementById('seriaNav').value = document.getElementById("mainNav").innerHTML;

                document.getElementById("navForm").submit();
            }
        });
    }
</script>
