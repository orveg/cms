<?php echo $header; ?>
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <a href="/" target="_blank" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-sitemap fa-sm text-white-50"></i>
            Siteye Git</a>
    </div>
    <div class="row">
        <div class="col-xl-3 col-md-4 col-sm-12  mb-6">
            <div class="col-12 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Okunmamış Mesaj</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $new_message?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Okunmuş Mesaj</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $old_message?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-9 col-md-8 col-sm-12 mb-6">
            <div class="card shadow mb-4">

                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">
                        Mesajlar            </h6>

                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="/admin/contacts">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </a>

                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Tarih</th>
                                <th>İsim</th>
                                <th>Telefon</th>
                                <th>Tipi</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($contacts as $contact) { ?>
                                <tr>
                                    <td><?php echo dateFormat($contact["date"]) ?></td>
                                    <td><?php echo $contact["name"] ?></td>
                                    <td><?php echo $contact["phone"] ?></td>
                                    <td><?php echo typeContact($contact["type"]) ?></td>
                                    <td>  <a href="<?= BASE_URL . '/admin/contact/' . $contact['id'] ?>" class="btn btn-small btn-success">
                                            <i class="fas fa-fw fa-eye"></i>
                                        </a></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php echo $footer; ?>
