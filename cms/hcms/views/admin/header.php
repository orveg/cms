<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Minerva Dashboard - <?php echo SITE_NAME; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="<?php echo ADMIN_THEME; ?>/css/font-awesome/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="<?php echo ADMIN_THEME; ?>/css/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo ADMIN_THEME; ?>/css/datatables.min.css" rel="stylesheet">
</head>
<body id="page-top">
<div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo BASE_URL; ?>/admin">
            <div class="sidebar-brand-text mx-3"><?php echo wordlimit(SITE_NAME, 20, "") ?> <sup>v.1</sup></div>
        </a>
        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="<?php echo BASE_URL; ?>/admin">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span><?php echo $this->lang->line('nav_dash'); ?></span></a>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider">
        <!-- Heading -->
        <div class="sidebar-heading">
            <?php echo $this->lang->line("nav_content") ?>
        </div>
        <li class="nav-item  <?php echo $current == "pages" ? "active" : "" ?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pages" aria-expanded="true" aria-controls="pages">
                <i class="fas fa-fw fa-folder"></i>
                <span><?php echo $this->lang->line('nav_pages'); ?></span>
            </a>
            <div id="pages" class="collapse <?php echo $current == "pages" ? "show" : "" ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header"><?php echo $this->lang->line('nav_pages'); ?></h6>
                    <a class="collapse-item <?php echo ($current == "pages") && ($this->uri->segment(3) == "") ? "active" : "" ?>" href="<?php echo BASE_URL; ?>/admin/pages">
                        <?php echo $this->lang->line('nav_pages_all'); ?>
                    </a>
                    <a class="collapse-item <?php echo ($current == "pages") && ($this->uri->segment(3) == "new") ? "active" : "" ?>" href="<?php echo BASE_URL; ?>/admin/pages/new">
                        <?php echo $this->lang->line('nav_pages_new'); ?>
                    </a>
                </div>
            </div>
        </li>

        <li class="nav-item  <?php echo $current == "posts" ? "active" : "" ?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#posts" aria-expanded="true" aria-controls="pages">
                <i class="fas fa-fw fa-newspaper"></i>
                <span><?php echo $this->lang->line('nav_posts'); ?></span>
            </a>
            <div id="posts" class="collapse <?php echo $current == "posts" ? "show" : "" ?>" aria-labelledby="headingPosts" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header"><?php echo $this->lang->line('nav_posts'); ?></h6>
                    <a class="collapse-item <?php echo ($current == "posts") && ($this->uri->segment(3) == "") ? "active" : "" ?>" href="<?php echo BASE_URL; ?>/admin/posts">
                        <?php echo $this->lang->line('nav_posts_all'); ?>
                    </a>
                    <a class="collapse-item <?php echo ($current == "posts") && ($this->uri->segment(3) == "new") ? "active" : "" ?>" href="<?php echo BASE_URL; ?>/admin/posts/new">
                        <?php echo $this->lang->line('nav_posts_new'); ?>
                    </a>
                    <a class="collapse-item <?php echo ($current == "categories") && ($this->uri->segment(3) == "") ? "active" : "" ?>" href="<?php echo BASE_URL; ?>/admin/posts/categories">
                        <?php echo $this->lang->line('nav_categories_all'); ?>
                    </a>
                    <a class="collapse-item <?php echo ($current == "categories") && ($this->uri->segment(3) == "new") ? "active" : "" ?>" href="<?php echo BASE_URL; ?>/admin/posts/categories/new">
                        <?php echo $this->lang->line('nav_categories_new'); ?>
                    </a>
                </div>
            </div>
        </li>

        <li class="nav-item  <?php echo $current == "users" ? "active" : "" ?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#users" aria-expanded="true" aria-controls="pages">
                <i class="fas fa-fw fa-users"></i>
                <span><?php echo $this->lang->line('nav_users'); ?></span>
            </a>
            <div id="users" class="collapse <?php echo $current == "users" ? "show" : "" ?>" aria-labelledby="headingUsers" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header"><?php echo $this->lang->line('nav_users'); ?></h6>
                    <a class="collapse-item <?php echo ($current == "users") && ($this->uri->segment(3) == "") ? "active" : "" ?>" href="<?php echo BASE_URL; ?>/admin/users">
                        <?php echo $this->lang->line('nav_users_all'); ?>
                    </a>
                    <a class="collapse-item <?php echo ($current == "users") && ($this->uri->segment(3) == "new") ? "active" : "" ?>" href="<?php echo BASE_URL; ?>/admin/users/new">
                        <?php echo $this->lang->line('nav_users_new'); ?>
                    </a>
                </div>
            </div>
        </li>

        <li class="nav-item  <?php echo $current == "navigation" ? "active" : "" ?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#navigation" aria-expanded="true" aria-controls="pages">
                <i class="fas fa-fw fa-list"></i>
                <span><?php echo $this->lang->line('nav_navigation'); ?></span>
            </a>
            <div id="navigation" class="collapse <?php echo $current == "navigation" ? "show" : "" ?>" aria-labelledby="headingNavigation" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header"><?php echo $this->lang->line('nav_navigation'); ?></h6>
                    <a class="collapse-item <?php echo ($current == "navigation") && ($this->uri->segment(3) == "") ? "active" : "" ?>" href="<?php echo BASE_URL; ?>/admin/navigation">
                        <?php echo $this->lang->line('nav_navigation_all'); ?>
                    </a>
                    <a class="collapse-item <?php echo ($current == "navigation") && ($this->uri->segment(3) == "new") ? "active" : "" ?>" href="<?php echo BASE_URL; ?>/admin/navigation/new">
                        <?php echo $this->lang->line('nav_navigation_new'); ?>
                    </a>
                </div>
            </div>
        </li>
        <li class="nav-item  <?php echo $current == "widgets" ? "active" : "" ?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#widgets" aria-expanded="true" aria-controls="pages">
                <i class="fas fa-fw fa-wine-glass"></i>
                <span>Widget</span>
            </a>
            <div id="widgets" class="collapse <?php echo $current == "widgets" ? "show" : "" ?>" aria-labelledby="headingNavigation" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Widget</h6>
                    <a class="collapse-item <?php echo ($current == "widgets") && ($this->uri->segment(3) == "") ? "active" : "" ?>" href="<?php echo BASE_URL; ?>/admin/widgets">
                        Widgetlar
                    </a>
                    <a class="collapse-item <?php echo ($current == "widgets") && ($this->uri->segment(3) == "new") ? "active" : "" ?>" href="<?php echo BASE_URL; ?>/admin/widgets/new">
                        Wigdet Ekle
                    </a>
                </div>
            </div>
        </li>
        <li class="nav-item <?php if($current == "contacts") {
            echo "active";
        } ?>">
            <a class="nav-link" href="<?php echo BASE_URL; ?>/admin/contacts">
                <i class="fas fa-fw fa-envelope"></i>
                <span>Mesajlar</span>
            </a>
        </li>
        <li class="nav-item <?php if($current == "filemanager") {
            echo "active";
        } ?>">
            <a class="nav-link" href="<?php echo BASE_URL; ?>/admin/filemanager">
                <i class="fas fa-fw fa-file-archive"></i>
                <span><?php echo $this->lang->line('nav_filemanager'); ?></span>
            </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            <?php echo $this->lang->line('nav_settings'); ?>
        </div>

        <li class="nav-item <?php if($current == "social") {
            echo "active";
        } ?>">
            <a class="nav-link" href="<?php echo BASE_URL; ?>/admin/social">
                <i class="fas fa-fw fa-share-alt"></i>
                <span><?php echo $this->lang->line('nav_social'); ?></span>
            </a>
        </li>

        <li class="nav-item <?php if($current == "settings") {
            echo "active";
        } ?>">
            <a class="nav-link" href="<?php echo BASE_URL; ?>/admin/settings">
                <i class="fas fa-fw fa-cogs"></i>
                <span><?php echo $this->lang->line('nav_settings'); ?></span>
            </a>
        </li>


        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>
    </ul>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Search -->
                <form class="d-none  navbar-search">
                    <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Arama..." aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>
                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline login-user small"> <?php echo $this->session->userdata('userName'); ?></span>
                            <i class="fas fa-chevron-down"></i>
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                Profile
                            </a>
                            <a class="dropdown-item" href="<?php echo BASE_URL; ?>/admin/settings">
                                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                <span><?php echo $this->lang->line('nav_settings'); ?></span>
                            </a>

                            <a class="dropdown-item" href="#">
                                <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                Activity Log
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                <?php echo $this->lang->line('nav_logout'); ?>
                            </a>
                        </div>
                    </li>
                </ul>
            </nav>
