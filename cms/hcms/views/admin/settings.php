<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('settings_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li class="active">
                    <a href="<?php echo BASE_URL; ?>/admin/settings"><?php echo $this->lang->line('settings_header'); ?></a>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">
                        <?php echo $this->lang->line('settings_header'); ?>
                    </h6>
                </div>
                <div class="card-body">
                    <?php echo $this->lang->line('settings_message'); ?>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">
                        <?php echo $this->lang->line('settings_info'); ?>
                    </h6>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <?php foreach($settings
                        as $s) { ?>
                        <?php echo form_open_multipart(BASE_URL . '/admin/settings/update'); ?>
                        <?php echo form_error('siteTitle', '<div class="alert">', '</div>'); ?>
                        <label class="control-label" for="siteTitle"><?php echo $this->lang->line('settings_name'); ?></label>
                        <div class="controls">
                            <?php $data = array('name' => 'siteTitle',
                                'id' => 'siteTitle',
                                'class' => 'form-control',
                                'value' => set_value('siteTitle', $s['siteTitle'], FALSE));
                            echo form_input($data); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="siteFooter"><?php echo $this->lang->line('settings_footer'); ?></label>
                        <div class="controls">
                            <?php $data = array('name' => 'siteFooter',
                                'id' => 'siteFooter',
                                'class' => 'form-control',
                                'value' => set_value('siteFooter', $s['siteFooter'], FALSE));
                            echo form_input($data); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="themes"><?php echo $this->lang->line('settings_theme'); ?></label>
                        <div class="controls">
                            <?php
                            $att = 'id="siteTheme" class="form-control"';
                            $data = array();
                            foreach($themesdir as $t) {
                                if(!is_dir($t)) {
                                    if(($t != "index.html") && ($t != "admin/") && ($t != "admin")) {
                                        $t = str_replace("/", "", $t);
                                        $data[$t] = $t;
                                    }
                                }
                            }
                            echo form_dropdown('siteTheme', $data, $s['siteTheme'], $att); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="siteLang"><?php echo $this->lang->line('settings_lang'); ?></label>
                        <div class="controls">
                            <?php
                            $att = 'id="siteLang" class="form-control"';
                            $data = array();
                            foreach($langdir as $l) {
                                if(!is_dir($l)) {
                                    if($l != "index.html") {
                                        $l = str_replace("/", "", $l);
                                        $data[$l] = $l;
                                    }
                                }
                            }
                            echo form_dropdown('siteLang', $data, $s['siteLang'], $att); ?>
                        </div>
                    </div>
                    <hr/>

                    <div class="form-group">
                        <?php echo form_error('file_upload', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label" for="siteLogo">Site Logo</label>
                        <div class="input-group">
                            <input name="siteLogo" class="form-control" id="siteLogo" type="text" value="<?php echo $s['siteLogo'] ?>">
                            <a href="javascript:open_popup('/filemanager/dialog.php?field_id=imgField&lang=tr_TR&akey=<?= $this->config->config["encryption_key"] ?>')" class="btn" type="button">
                                Select
                            </a>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php echo form_error('file_upload', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label" for="siteFavicon">Site Favicon</label>
                        <div class="input-group">
                            <input name="siteFavicon" class="form-control" id="siteFavicon" type="text" value="<?php echo $s['siteFavicon'] ?>">
                            <a href="javascript:open_popup('/filemanager/dialog.php?field_id=imgField&lang=tr_TR&akey=<?= $this->config->config["encryption_key"] ?>')" class="btn" type="button">
                                Select
                            </a>
                        </div>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="control-label" for="siteMaintenance"><?php echo $this->lang->line('settings_maintenance'); ?></label>
                        <div class="controls">
                            <?php
                            $att = 'id="siteMaintenance" class="form-control"';
                            $data = array("0" => "Disabled",
                                "1" => "Enabled");


                            echo form_dropdown('siteMaintenance', $data, $s['siteMaintenance'], $att); ?>
                        </div>
                    </div>
                   <!-- <div class="form-group">
                        <label class="control-label" for="siteMaintenanceHeading"><?php echo $this->lang->line('settings_maintenance_heading'); ?></label>
                        <div class="controls">
                            <?php $data = array('name' => 'siteMaintenanceHeading',
                                'id' => 'siteMaintenanceHeading',
                                'class' => 'form-control',
                                'value' => set_value('siteMaintenanceHeading', $s['siteMaintenanceHeading'], FALSE));

                            echo form_input($data); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="siteMaintenanceMeta"><?php echo $this->lang->line('settings_maintenance_meta'); ?></label>
                        <div class="controls">
                            <?php $data = array('name' => 'siteMaintenanceMeta',
                                'id' => 'siteMaintenanceMeta',
                                'class' => 'form-control',
                                'value' => set_value('siteMaintenanceMeta', $s['siteMaintenanceMeta'], FALSE));

                            echo form_input($data); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="siteMaintenanceContent"><?php echo $this->lang->line('settings_maintenance_content'); ?></label>
                        <div class="controls">
                            <?php $data = array('name' => 'siteMaintenanceContent',
                                'id' => 'siteMaintenanceContent',
                                'class' => 'form-control',
                                'value' => set_value('siteMaintenanceContent', $s['siteMaintenanceContent'], FALSE));

                            echo form_textarea($data); ?>
                        </div>
                    </div>-->
                    <hr/>
                    <div class="form-group">
                        <label class="control-label" for="siteAdditionalJS"><?php echo $this->lang->line('settings_additional_js'); ?></label>
                        <div class="controls">
                            <?php $data = array('name' => 'siteAdditionalJS',
                                'id' => 'siteAdditionalJS',
                                'class' => 'form-control',
                                'value' => set_value('siteAdditionalJS', $s['siteAdditionalJS'], FALSE));

                            echo form_textarea($data); ?>
                        </div>
                    </div>
                    <div class="panel-footer">


                        <?php $data = array('name' => 'submit',
                            'id' => 'submit',
                            'class' => 'btn btn-primary',
                            'value' => $this->lang->line('btn_save'),);
                        echo form_submit($data); ?>
                        <?php echo form_close();
                        } ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php echo $footer; ?>

    <script type="text/javascript">
        $(function () {

            if (document.getElementById('file_upload')) {
                function prepareUpload(event) {
                    files = event.target.files;
                    uploadFiles(event);
                }

                function uploadFiles(event) {
                    event.stopPropagation();
                    event.preventDefault();

                    $('#loading_pic').show();

                    var data = new FormData();
                    $.each(files, function (key, value) {
                        data.append(key, value);
                    });

                    $.ajax({
                        url: '<?php echo BASE_URL; ?>/admin/settings/submit',
                        type: 'POST',
                        data: data,
                        cache: false,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (data, textStatus, jqXHR) {
                            if (data != '0') {
                                $('#logo_preloaded').show();
                                document.getElementById('logo_preloaded').src = '<?php echo BASE_URL; ?>/uploads/' + data;
                                document.getElementById('siteLogo').value = data;
                                $('#loading_pic').hide();
                            } else
                                alert('<?php echo $this->lang->line('settings_image_error'); ?>');
                        }
                    });
                }

                function submitForm(event, data) {
                    $form = $(event.target);
                    var formData = $form.serialize();
                    $.each(data.files, function (key, value) {
                        formData = formData + '&filenames[]=' + value;
                    });

                    $.ajax({
                        url: '<?php echo BASE_URL; ?>/admin/settings/submit',
                        type: 'POST',
                        data: formData,
                        cache: false,
                        dataType: 'json',
                        success: function (data, textStatus, jqXHR) {
                            if (typeof data.error === 'undefined')
                                console.log('SUCCESS: ' + data.success);
                            else
                                console.log('ERRORS: ' + data.error);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log('ERRORS: ' + textStatus);
                        },
                        complete: function () {
                            $('#loading_pic').hide();
                        }
                    });
                }

                var files;
                $('input[name=file_upload]').on('change', prepareUpload);
            }
            if (document.getElementById('favicon_upload')) {
                function prepareUploadFavi(event) {
                    files = event.target.files;
                    uploadFilesFavi(event);
                }

                function uploadFilesFavi(event) {
                    event.stopPropagation();
                    event.preventDefault();

                    $('#loading_pic_favicon').show();

                    var data = new FormData();
                    $.each(files, function (key, value) {
                        data.append(key, value);
                    });

                    $.ajax({
                        url: '<?php echo BASE_URL; ?>/admin/settings/submit',
                        type: 'POST',
                        data: data,
                        cache: false,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (data, textStatus, jqXHR) {
                            if (data != '0') {
                                $('#favicon_preloaded').show();
                                document.getElementById('favicon_preloaded').src = '<?php echo BASE_URL; ?>/uploads/' + data;
                                document.getElementById('siteFavicon').value = data;
                                $('#loading_pic_favicon').hide();
                            } else
                                alert('<?php echo $this->lang->line('settings_image_error'); ?>');
                        }
                    });
                }

                function submitForm(event, data) {
                    $form = $(event.target);
                    var formData = $form.serialize();
                    $.each(data.files, function (key, value) {
                        formData = formData + '&filenames[]=' + value;
                    });

                    $.ajax({
                        url: '<?php echo BASE_URL; ?>/admin/settings/submit',
                        type: 'POST',
                        data: formData,
                        cache: false,
                        dataType: 'json',
                        success: function (data, textStatus, jqXHR) {
                            if (typeof data.error === 'undefined')
                                console.log('SUCCESS: ' + data.success);
                            else
                                console.log('ERRORS: ' + data.error);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log('ERRORS: ' + textStatus);
                        },
                        complete: function () {
                            $('#loading_pic_favicon').hide();
                        }
                    });
                }

                var files;
                $('input[name=favicon_upload]').on('change', prepareUploadFavi);
            }
        });
    </script>
