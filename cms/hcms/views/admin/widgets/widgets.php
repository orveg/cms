<?php echo $header; ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">
                     Widgetlar
                    </h1>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="input-group searchContainer">
                    <input type="text" class="form-control" id="searchString">
                    <span class="input-group-btn">
				<button class="btn btn-default" type="button" id="searchBtn" onClick="doPostSearch();"><span class="fa fa-search"></span></button>
			  </span>
                </div>
            </div>
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                    </li>
                    <li class="active">
                        <a href="<?php echo BASE_URL; ?>/admin/widgets">Widgetlar</a>
                    </li>
                </ol>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                    Widgetlar
                </h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th> ID</th>
                            <th> Başlık </th>
                            <th> Kod </th>
                            <th class="td-actions"></th>
                        </tr>
                        </thead>
                        <tbody id="pageContainer">
                        <?php foreach($widgets as $p) { ?>

                            <tr>
                                <td style="width: 238px"><?php echo $p["id"] ?></td>
                                <td><?php echo $p["title"] ?></td>
                                <td>{{<?php echo $p["title"] ?>}}</td>
                              <td>
                                    <a href="<?= BASE_URL . '/admin/widgets/edit/' . $p['id'] ?>" class="btn btn-small btn-success">
                                        <i class="fas fa-fw fa-pencil-alt"></i>
                                    </a>
                                    <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small delete" href="<?= BASE_URL . '/admin/widgets/delete/' . $p['id'] ?>">
                                        <i class="fas fa-fw fa-eraser"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <div class="text-center" id="loadingSpinner">
                        <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
                    </div>
                    <div id="paginationContainer">
                        <?php echo $this->pagination->create_links(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo $footer; ?>
