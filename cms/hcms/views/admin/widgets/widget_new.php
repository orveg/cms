<?php echo $header; ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">    Widget Ekle</h1>
                </div>
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                    </li>
                    <li>
                        <a href="<?php echo BASE_URL; ?>/admin/wigdets">Wigdetlar</a>
                    </li>
                    <li class="active">
                        Widget Ekle
                    </li>
                </ol>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">
                            <i class="fa fa-pencil fa-fw"></i>
                            Widget Ekle
                        </h6>
                    </div>

                    <div class="card-body">
                        <?php echo form_open(BASE_URL . '/admin/widgets/new/add'); ?>

                        <div class="form-group">
                            <?php echo form_error('title', '<div class="alert alert-danger">', '</div>'); ?>
                            <label class="control-label" for="title">Başlık</label>
                            <div class="controls">
                                <?php $data = array('name' => 'title',
                                                    'id' => 'title',
                                                    'class' => 'form-control',
                                                    'value' => set_value('title', '', FALSE));

                                echo form_input($data); ?>

                            </div> <!-- /controls -->
                        </div> <!-- /form-group -->

                        <div class="form-group">
                            <?php echo form_error('email', '<div class="alert alert-danger">', '</div>'); ?>
                            <label class="control-label" for="content">İçerik</label>
                            <div class="controls">
                                <?php $data = array('name' => 'content',
                                                    'id' => 'content',
                                                    'class' => 'form-control',
                                                    'value' => set_value('content', '', FALSE));

                                echo form_textarea($data); ?>

                            </div> <!-- /controls -->
                        </div> <!-- /form-group -->



                    </div>
                    <div class="card-footer">
                        <?php $data = array('name' => 'submit',
                                            'id' => 'submit',
                                            'class' => 'btn btn-primary',
                                            'value' => $this->lang->line('btn_save'),);
                        echo form_submit($data); ?>
                        <a class="btn" href="<?php echo BASE_URL; ?>/admin/users"><?php echo $this->lang->line('btn_cancel'); ?></a>
                    </div> <!-- /form-actions -->
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
<?php echo $footer; ?>