<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo $this->lang->line('user_delete').": ".$form[0]['title'] ?>?</h4>
</div>			<!-- /modal-header -->
<div class="modal-body">
        <div class="alert alert-danger" role="alert"><strong>WARNING: </strong>  Silme istediğinize emin misiniz?</div>
</div>			<!-- /modal-body -->
<div class="modal-footer">
	<form action="<?php echo BASE_URL.'/admin/wigdets/delete/'.$form[0]['id']; ?>" method="POST" name="pageform" id="pageform">
        <input name="deleteid" type="hidden" id="deleteid" value="<?php echo $form[0]['id'] ?>" />
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('btn_cancel'); ?></button>
        <input class="btn btn-danger" type="submit" value="<?php echo $this->lang->line('btn_delete'); ?>"/>
    </form> 
</div>			<!-- /modal-footer -->