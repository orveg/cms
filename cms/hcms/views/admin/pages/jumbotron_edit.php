<?php echo $header; ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800"><?php echo $pages[0]['pageTitle'] ?>
                    - <?php echo $this->lang->line('pages_jumbo_header'); ?></h1>
            </div>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>/admin/pages"><?php echo $this->lang->line('nav_pages_all'); ?></a>
                </li>
                <li class="active">
                    <?php echo $this->lang->line('pages_jumbo_edit'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="alert alert-info"><?php echo $this->lang->line('pages_jumbo_intro'); ?></div>
    <div class="card shadow mb-4">

        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">
                <?php echo $pages[0]['pageTitle'] ?> - <?php echo $this->lang->line('pages_slider_header'); ?>
            </h6>
        </div>
        <div class="card-body">
            <div class="form-actions">
                <a class="btn btn-primary" data-toggle="modal" href="#slider"><?php echo $this->lang->line('pages_slider_btn'); ?></a>
            </div>
        </div>
    </div>
    <div class="card shadow mb-4">

        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">
                <?php echo $pages[0]['pageTitle'] ?> - <?php echo $this->lang->line('pages_jumbo_header'); ?>
            </h6>
        </div>
        <div class="card-body">
            <?php foreach($pages

                as $p) {
                echo form_error('pageTitle', '<div class="alert">', '</div>');
                $attr = "id='contentForm'";
                echo form_open(BASE_URL . '/admin/pages/jumbotron/' . $this->uri->segment(4), $attr); ?>
                <?php $data = array('id'    => 'ckeditor',
                                    'name'  => 'jumbotron',
                                    'class' => 'js-st-instance',);

                echo form_textarea($data, set_value('jumbotron', $p['jumbotron'], FALSE)); ?>
                <div class="card-footer">
                    <a class="btn btn-primary" data-toggle="modal" href="#attributes"><?php echo $this->lang->line('btn_next'); ?></a>
                    <a class="btn" href="<?php echo BASE_URL; ?>/admin/pages"><?php echo $this->lang->line('btn_cancel'); ?></a>
                </div> <!-- /form-actions -->
                <div id="attributes" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"><?php echo $this->lang->line('pages_jumbo_header_att'); ?></h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <?php echo form_error('enableJumbotron', '<div class="alert">', '</div>'); ?>
                                    <label class="control-label" for="enableJumbotron"><?php echo $this->lang->line('pages_jumbo_enable'); ?></label>

                                    <?php
                                    $att = 'id="enableJumbotron" class="form-control"';
                                    $data = array('1' => $this->lang->line('option_yes'),
                                                  '0' => $this->lang->line('option_no'),);

                                    echo form_dropdown('enableJumbotron', $data, $p['enableJumbotron'], $att); ?>

                                </div> <!-- /form-group -->
                            </div>
                            <div class="card-footer">
                                <button class="btn ex-back" data-dismiss="modal" aria-hidden="true"><?php echo $this->lang->line('btn_back'); ?></button>
                                <a class="btn btn-primary" onClick="formSubmit()"><?php echo $this->lang->line('btn_save'); ?></a>
                            </div>
                        </div>

                    </div>
                </div>
                <div id="slider" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">

                                <h4 class="modal-title"><?php echo $this->lang->line('pages_slider_header'); ?></h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body" id="pic_container">


                                <div class="form-group">
                                    <?php echo form_error('enableSlider', '<div class="alert">', '</div>'); ?>

                                    <label class="control-label" for="enableJumbotron"><?php echo $this->lang->line('pages_slider_enable'); ?></label>
                                    <?php
                                    $att = 'id="enableSlider" class="form-control"';
                                    $data = array('1' => $this->lang->line('option_yes'),
                                                  '0' => $this->lang->line('option_no'),
                                                  '2' => "Tekli",);

                                    echo form_dropdown('enableSlider', $data, $p['enableSlider'], $att); ?>

                                </div> <!-- /form-group -->


                                <div class="slider"
                                <div id="repeater">
                                    <button class="btn btn-primary repeater-add-btn" type="button"><?php echo $this->lang->line('pages_slider_add'); ?></button>
                                    <hr/>
                                    <div class="items" data-group="test">
                                        <?php $i = 0; ?>

                                        <div class="control-group" id="pic_element<?php echo $i; ?>">
                                            <label class="control-label">
                                                <span><?php echo $this->lang->line('pages_slider_title'); ?></span>

                                                <button id="delete<?php echo $i; ?>" class="close" type="button" onClick="$(this).parents('.items').remove()">
                                                    ×
                                                </button>
                                            </label>
                                            <div class="controls">
                                                <?php echo form_error('file_upload', '<div class="alert alert-danger">',
                                                                      '</div>'); ?>
                                                <label class="control-label" for="postImage<?php echo $i; ?>"><?php echo $this->lang->line('posts_new_feature'); ?></label>
                                                <div class="input-group">
                                                    <input data-name="postImage" name="postImage[<?php echo $i; ?>]" class="form-control" id="postImage<?php echo $i; ?>" type="text" value="">
                                                    <a href="javascript:open_popup('/filemanager/dialog.php?&amp;popup=1&amp;field_id=postImage<?php echo $i; ?>&akey=<?= $this->config->config["encryption_key"] ?>')" class="btn" type="button">
                                                        Select
                                                    </a>
                                                </div>

                                                <p><?php echo $this->lang->line('pages_slider_alt'); ?></p>
                                                <input data-name="alt"  class="form-control" type="text" id="alt<?php echo $i; ?>" name="alt[<?php echo $i; ?>]"/>
                                                <p><?php echo $this->lang->line('pages_slider_link'); ?></p>
                                                <input  data-name="link" class="form-control" type="text" id="link<?php echo $i; ?>" name="link[<?php echo $i; ?>]"/>

                                                <p>HTML Widget</p>
                                                <input data-name="html"  class="form-control" type="text" id="html<?php echo $i; ?>" name="html[<?php echo $i; ?>]"/>

                                                <p>Sıralama</p>
                                                <input data-name="order"  class="form-control" type="text" id="order<?php echo $i; ?>" name="order[<?php echo $i; ?>]"/>

                                            </div> <!-- /controls -->
                                        </div> <!-- /control-group -->

                                    </div>
                                </div>
                                <div class="slider">


                                    <?php $i = 1;

                                    foreach($slides as $s) {
                                        $i++; ?>
                                        <div class="items" data-group="test">
                                            <div class="control-group" id="pic_element<?php echo $i; ?>">
                                                <label class="control-label">
                                                    <span><?php echo $this->lang->line('pages_slider_title'); ?></span>

                                                    <button id="delete<?php echo $i; ?>" class="close" type="button" onClick="$(this).parents('.items').remove()">
                                                        ×
                                                    </button>
                                                </label>
                                                <div class="controls">
                                                    <?php echo form_error('file_upload',
                                                                          '<div class="alert alert-danger">',
                                                                          '</div>'); ?>
                                                    <label class="control-label" for="postImage<?php echo $i; ?>"><?php echo $this->lang->line('posts_new_feature'); ?></label>
                                                    <div class="input-group">
                                                        <input name="postImage[<?php echo $i; ?>]" class="form-control" id="postImage<?php echo $i; ?>" type="text" value="<?php echo $s['slideImage'] ?>">
                                                        <a href="javascript:open_popup('/filemanager/dialog.php?&amp;popup=1&amp;field_id=postImage<?php echo $i; ?>&akey=<?= $this->config->config["encryption_key"] ?>')" class="btn" type="button">
                                                            Select
                                                        </a>
                                                    </div>

                                                    <p><?php echo $this->lang->line('pages_slider_alt'); ?></p>
                                                    <input class="form-control" type="text" id="alt<?php echo $i; ?>" name="alt[<?php echo $i; ?>]" <?php if($s['slideAlt'] != "") {
                                                        echo 'value="' . $s['slideAlt'] . '"';
                                                    } else {
                                                        echo '';
                                                    } ?> />
                                                    <p><?php echo $this->lang->line('pages_slider_link'); ?></p>
                                                    <input class="form-control" type="text" id="link<?php echo $i; ?>" name="link[<?php echo $i; ?>]" <?php if($s['slideLink'] != "") {
                                                        echo 'value="' . $s['slideLink'] . '"';
                                                    } else {
                                                        echo '';
                                                    } ?> />

                                                    <p>HTML Widget</p>
                                                    <input class="form-control" type="text" id="html<?php echo $i; ?>" name="html[<?php echo $i; ?>]" <?php if($s['slideHtml'] != "") {
                                                        echo 'value="' . $s['slideHtml'] . '"';
                                                    } else {
                                                        echo '';
                                                    } ?> />

                                                    <p>Sıralama</p>
                                                    <input class="form-control" type="text" id="order<?php echo $i; ?>" name="order[<?php echo $i; ?>]" <?php if($s['slideOrder'] != "") {
                                                        echo 'value="' . $s['slideOrder'] . '"';
                                                    } else {
                                                        echo '';
                                                    } ?> />

                                                </div> <!-- /controls -->
                                            </div> <!-- /control-group -->
                                        </div>
                                        <?php
                                    }

                                    ?>

                                </div>
                            </div><!-- /modal-body -->
                            <div class="card-footer">
                                <button class="btn ex-back" data-dismiss="modal" aria-hidden="true"><?php echo $this->lang->line('btn_back'); ?></button>
                                <a class="btn btn-primary" onClick="formSubmit()"><?php echo $this->lang->line('btn_save'); ?></a>
                            </div>
                        </div>

                    </div>
                </div>

                <?php echo form_close();
            } ?>
        </div>
    </div>


</div>


<?php echo $footer; ?>
<script type="text/javascript">
    CKEDITOR.replace('ckeditor', {
        filebrowserBrowseUrl: '/filemanager/dialog.php?field_id=imgField&lang=tr_TR&akey=<?=$this->config->config["encryption_key"]?>',
        filebrowserUploadUrl: '/filemanager/dialog.php?field_id=imgField&lang=tr_TR&akey=<?=$this->config->config["encryption_key"]?>',
        filebrowserImageBrowseUrl: '/filemanager/dialog.php?field_id=imgField&lang=tr_TR&akey=<?=$this->config->config["encryption_key"]?>',
        filebrowserWindowWidth: '1024',
        filebrowserWindowHeight: '800'
    });
    $("#repeater").createRepeater();

    function formSubmit() {
        $.ajax({
            url: "/admin/check/session",
        }).done(function (data) {
            sessionExist = data;
            if (sessionExist == 0) {
                $('.modal').modal('hide');
                $('#loginModal').modal({
                    backdrop: 'static',
                    keyboard: false
                }).modal('show');
            } else {

                document.getElementById("contentForm").submit();
            }
        });
    }
</script>

