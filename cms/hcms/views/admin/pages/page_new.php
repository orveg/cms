<?php echo $header; ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">    <?php echo $this->lang->line('pages_new_header'); ?></h1>
            </div>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>/admin/pages"><?php echo $this->lang->line('nav_pages_all'); ?></a>
                </li>
                <li class="active">
                    <?php echo $this->lang->line('pages_new_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">
                <?php echo $this->lang->line('pages_new_header'); ?>
            </h6>
            <div class="dropdown no-arrow">
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                </a>
            </div>
        </div>
        <div class="card-body">
            <?php echo form_error('pageTitle', '<div class="alert alert-danger">', '</div>'); ?>
            <?php echo form_error('pageURL', '<div class="alert alert-danger">', '</div>'); ?>
            <?php
            $attr = array('id' => 'contentForm');
            echo form_open(BASE_URL . '/admin/pages/new/add', $attr); ?>
            <?php $data = array('name' => 'content',
                'id' => 'ckeditor',
                'class' => 'js-st-instance');
            echo form_textarea($data, set_value('', $this->input->post('content'), FALSE)); ?>
            <div class="panel-footer mt-3">
                <a class="btn btn-primary" data-toggle="modal" href="#attributes"><?php echo $this->lang->line('btn_next'); ?></a>
                <a class="btn" href="<?php echo BASE_URL; ?>/admin/pages"><?php echo $this->lang->line('btn_cancel'); ?></a>
            </div>
            <div id="attributes" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"><?php echo $this->lang->line('pages_new_attributes'); ?></h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info"><?php echo $this->lang->line('pages_new_required'); ?></div>
                            <div class="form-group">
                                <?php echo form_error('pageTitle', '<div class="alert alert-danger">', '</div>'); ?>
                                <label class="control-label" for="pageTitle"><?php echo $this->lang->line('pages_new_title'); ?></label>
                                <div class="controls">
                                    <?php $data = array('name' => 'pageTitle',
                                        'id' => 'pageTitle',
                                        'class' => 'form-control',
                                        'value' => set_value('pageTitle', '', FALSE));

                                    echo form_input($data); ?>
                                </div> <!-- /controls -->
                            </div> <!-- /form-group -->

                            <div class="form-group">
                                <?php echo form_error('navTitle', '<div class="alert alert-danger">', '</div>'); ?>
                                <label class="control-label" for="navTitle"><?php echo $this->lang->line('pages_new_nav'); ?></label>
                                <div class="controls">
                                    <?php $data = array('name' => 'navTitle',
                                        'id' => 'navTitle',
                                        'class' => 'form-control',
                                        'value' => set_value('navTitle', '', FALSE));

                                    echo form_input($data); ?>
                                </div> <!-- /controls -->
                            </div> <!-- /form-group -->

                            <div class="form-group">
                                <label class="control-label" for="pageKeywords"><?php echo $this->lang->line('pages_new_keywords'); ?></label>
                                <div class="controls">
                                    <?php $data = array('name' => 'pageKeywords',
                                        'id' => 'pageKeywords',
                                        'class' => 'form-control',
                                        'value' => set_value('pageKeywords', '', FALSE));

                                    echo form_input($data); ?>

                                </div> <!-- /controls -->
                            </div> <!-- /form-group -->

                            <div class="form-group">
                                <label class="control-label" for="pageDescription"><?php echo $this->lang->line('pages_new_description'); ?></label>
                                <div class="controls">
                                    <?php $data = array('name' => 'pageDescription',
                                        'id' => 'pageDescription',
                                        'class' => 'form-control',
                                        'value' => set_value('pageDescription', '', FALSE));

                                    echo form_input($data); ?>

                                </div> <!-- /controls -->
                            </div> <!-- /form-group -->

                            <div class="form-group">
                                <?php echo form_error('pageURL', '<div class="alert alert-danger">', '</div>'); ?>
                                <label class="control-label" for="pageURL"><?php echo $this->lang->line('pages_new_url'); ?></label>
                                <div class="controls">
                                    <?php $data = array('name' => 'pageURL',
                                        'id' => 'pageURL',
                                        'class' => 'form-control URLField',
                                        'value' => set_value('pageURL', '', FALSE));

                                    echo form_input($data); ?>

                                </div> <!-- /controls -->
                            </div> <!-- /form-group -->

                            <div class="form-group">
                                <?php echo form_error('pagePublished', '<div class="alert">', '</div>'); ?>
                                <label class="control-label" for="pagePublished"><?php echo $this->lang->line('pages_new_publish'); ?></label>
                                <div class="controls">

                                    <?php
                                    $att = 'id="pagePublished" class="form-control"';
                                    $data = array('1' => $this->lang->line('option_yes'),
                                        '0' => $this->lang->line('option_no'),);

                                    echo form_dropdown('pagePublished', $data, '1', $att); ?>

                                </div> <!-- /controls -->
                            </div> <!-- /form-group -->
                            <div class="form-group">
                                <?php echo form_error('pageTemplate', '<div class="alert">', '</div>'); ?>
                                <label class="control-label" for="pageTemplate"><?php echo $this->lang->line('pages_new_template'); ?></label>
                                <div class="controls">

                                    <?php

                                    $att = 'id="pageTemplate" class="form-control"';
                                    $data = array();
                                    foreach($templates as $t) {
                                        $t = str_replace(".php", "", $t);
                                        if(($t != "header") && ($t != "footer") && ($t != "error") && ($t != "article") && ($t != "maintenance")&& ($t != "home") && ($t != "search") && ($t != "category") && ($t != "index.html")) {
                                            $data[$t] = convertPageName($t);
                                        }
                                    }
                                    echo form_dropdown('pageTemplate', $data, 'home', $att); ?>

                                </div> <!-- /controls -->
                            </div> <!-- /form-group -->
                        </div>
                        <div class="modal-footer">
                            <button class="btn ex-back" data-dismiss="modal" aria-hidden="true"><?php echo $this->lang->line('btn_back'); ?></button>
                            <a class="btn btn-primary" onClick="formSubmit()"><?php echo $this->lang->line('btn_save'); ?></a>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <!-- /colmd12 -->

            </div>
        </div>
    </div>
</div>

<?php echo $footer;


?>


<script type="text/javascript">
    CKEDITOR.replace('ckeditor', {
        filebrowserBrowseUrl: '/filemanager/<?=$this->config->config["encryption_key"]?>',
        filebrowserUploadUrl: '/filemanager/<?=$this->config->config["encryption_key"]?>',
        filebrowserImageBrowseUrl: '/filemanager/<?=$this->config->config["encryption_key"]?>',
        filebrowserWindowWidth: '1024',
        filebrowserWindowHeight: '800'
    });
    $(document).ready(function () {
        $("#pageTitle").keyup(function () {
            $("#pageURL").val(slugify($(this).val()));
        });
    });

    function formSubmit() {
        $.ajax({
            url: "/admin/check/session",
        }).done(function (data) {
            sessionExist = data;
            if (sessionExist == 0) {
                $('.modal').modal('hide');
                $('#loginModal').modal({
                    backdrop: 'static',
                    keyboard: false
                }).modal('show');
            } else {

                document.getElementById("contentForm").submit();
            }
        });
    }
</script>

