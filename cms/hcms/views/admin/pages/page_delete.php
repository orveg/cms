<div class="modal-header">
    <h5 class="modal-title"><?php echo $this->lang->line('pages_delete') . ": " . $form[0]['navTitle'] ?>?</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-danger" role="alert">
        <strong>WARNING: </strong><?php if($form[0]['pageID'] == "1") {
            echo '<p>' . $this->lang->line('pages_delete_home') . '</p>';
        } else {
            echo '<p>' . $this->lang->line('pages_delete_message') . '</p>';
        } ?>
    </div>
</div>
<div class="modal-footer">
    <form action="<?php echo BASE_URL . '/admin/pages/delete/' . $form[0]['pageID']; ?>" method="POST" name="pageform" id="pageform">
        <input name="deleteid" type="hidden" id="deleteid" value="<?php echo $form[0]['pageID'] ?>"/>
        <button type="button" class="btn btn-light btn-icon-split" data-dismiss="modal">
                    <span class="icon text-gray-600">
                      <i class="fas fa-arrow-right"></i>
                    </span>
            <span class="text"><?php echo $this->lang->line('btn_cancel'); ?></span>
        </button>
        <?php if($form[0]['pageID'] != "1") { ?>
            <button type="submit" class="btn btn-danger btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                <span class="text"><?php echo $this->lang->line('btn_delete'); ?></span>
            </button>
        <?php } ?>
    </form>
</div>