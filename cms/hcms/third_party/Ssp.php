<?php

class SSP
{
    /**
     * Create the data output array for the DataTables rows
     *
     * @param array $columns Column information array
     * @param array $data Data from the SQL get
     *
     * @return array          Formatted data in a row based format
     */
    static function data_output($columns, $data)
    {

        $out = array();
        if(is_array($data)) {
            for($i = 0, $ien = count($data); $i < $ien; $i++) {
                $row = array();
                for($j = 0, $jen = count($columns); $j < $jen; $j++) {
                    $column = $columns[$j];
                    if(isset($column['alias'])) {
                        if($column['alias'] != '') {
                            $c = $column['alias'];
                        } else {
                            $c = $column['db'];
                        }
                    } else {
                        $c = $column['db'];
                    }

                    // Is there a formatter?
                    if(isset($column['formatter'])) {
                        $row[$column['dt']] = $column['formatter']($data[$i][$c], $data[$i]);
                    } else {

                        $row[$column['dt']] = $data[$i][$c];
                    }
                }
                $out[] = $row;
            }
        }

        return $out;
    }

    /**
     * Paging
     *
     * Construct the LIMIT clause for server-side processing SQL query
     *
     * @param array $request Data sent to server by DataTables
     * @param array $columns Column information array
     *
     * @return string SQL limit clause
     */
    static function limit($request, $columns)
    {
        $limit = '';
        if(isset($request['start']) && $request['length'] != -1) {
            $limit = "LIMIT " . intval($request['length']) . " OFFSET " . intval($request['start']);
        }

        return $limit;
    }

    /**
     * Ordering
     *
     * Construct the ORDER BY clause for server-side processing SQL query
     *
     * @param array $request Data sent to server by DataTables
     * @param array $columns Column information array
     *
     * @return string SQL order by clause
     */
    static function order($request, $columns)
    {
        $order = '';
        if(isset($request['order']) && count($request['order'])) {
            $orderBy = array();
            $dtColumns = SSP::pluck($columns, 'dt');
            for($i = 0, $ien = count($request['order']); $i < $ien; $i++) {
                // Convert the column index into the column data property
                $columnIdx = intval($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                if($requestColumn['orderable'] == 'true') {
                    $dir = $request['order'][$i]['dir'] === 'asc' ? 'ASC' : 'DESC';
                    if(!empty($column['field'])) {
                        $orderBy[] = $column['field'] . ' ' . $dir;
                    } else {
                        $orderBy[] = $column['db'] . ' ' . $dir;
                    }

                }
            }
            $order = 'ORDER BY ' . implode(', ', $orderBy);
        }

        return $order;
    }

    /**
     * Searching / Filtering
     *
     * Construct the WHERE clause for server-side processing SQL query.
     *
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here performance on large
     * databases would be very poor
     *
     * @param array $request Data sent to server by DataTables
     * @param array $columns Column information array
     *
     * @return string SQL where clause
     */
    static function filter($request, $columns, $filtroAdd)
    {

        $globalSearch = array();
        $columnSearch = array();
        $dtColumns = SSP::pluck($columns, 'dt');
        if(isset($request['search']) && $request['search']['value'] != '') {
            $str = $request['search']['value'];
            $str = pg_escape_string($str);
            for($i = 0, $ien = count($request['columns']); $i < $ien; $i++) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                if($requestColumn['searchable'] == 'true') {
                    if(!empty($column['search'])) {
                        $globalSearch[] = " {$column['search']}::varchar ILIKE '%$str%'";
                    } else {
                        $globalSearch[] = " {$column['db']}::varchar ILIKE '%$str%'";
                    }
                }
            }
        }

        if(!empty($request)) {
            // Individual column filtering
            for($i = 0, $ien = count($request['columns']); $i < $ien; $i++) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                $str = $requestColumn['search']['value'];
                $str = pg_escape_string($str);
                if($requestColumn['searchable'] == 'true' && $str != '') {
                    if(!empty($column['search'])) {
                        $columnSearch[] = " {$column['search']}::varchar ILIKE '%$str%'";
                    } else {
                        $columnSearch[] = " {$column['db']}::varchar ILIKE '%$str%'";
                    }

                }
            }
        }

        // Combine the filters into a single string
        $where = '';
        if(count($globalSearch)) {
            $where = '(' . implode(' OR ', $globalSearch) . ')';
        }
        if(count($columnSearch)) {
            $where = $where === '' ? implode(' AND ', $columnSearch) : $where . ' AND ' . implode(' AND ', $columnSearch);
        }
        //Agrega filtro general personalizado
        if($filtroAdd !== NULL) {
            if($where !== '') {
                $where = $filtroAdd . ' AND ' . $where;
            } else {
                $where = $filtroAdd;
            }
        }

        if($where !== '') {
            $where = 'WHERE ' . $where;
        }

        return $where;
    }

    /**
     * Perform the SQL queries needed for an server-side processing requested,
     * utilising the helper functions of this class, limit(), order() and
     * filter() among others. The returned array is ready to be encoded as JSON
     * in response to an SSP request, or can be modified if needed before
     * sending back to the client.
     *
     * @param array $request Data sent to server by DataTables
     * @param array $pg_details SQL connection details - see sql_connect()
     * @param string $table SQL table to query
     * @param string $primaryKey Primary key of the table
     * @param array $columns Column information array
     *
     * @return array          Server-side processing response array
     */
    static function simple($request, $table, $primaryKey, $columns, $filtroAdd = NULL, $page = TRUE, $joinTables = array(), $groupBy = "")
    {
        $db = SSP::pg_connect();

        // Build the SQL query string from the request
        $limit = SSP::limit($request, $columns);
        $order = SSP::order($request, $columns);
        $where = SSP::filter($request, $columns, $filtroAdd);
        $limit1 = ($page) ? $limit : "";

        if($joinTables == array()) {
            $select = "SELECT " . implode(", ", SSP::pluckas($columns)) . "
			 FROM $table
			 $where
			 $groupBy
			 $order
			 $limit1";

            $count = "SELECT COUNT({$primaryKey}) cuenta FROM $table $where $groupBy";
        } else {
            $join = "";
            $joinT = "";

            foreach($joinTables as $key => $joinTable) {
                $join .= $joinTable . " AND ";
                $joinT .= $key . " ,";
            }

            $joinT = rtrim($joinT, ",");
            $join = rtrim($join, " AND");

            $select = "SELECT " . implode(", ", SSP::pluckas($columns)) . "
			 FROM $table, $joinT
			 $where
			 AND $join
			 $groupBy
			 $order
			 $limit1";
            $count = "SELECT COUNT($table.$primaryKey) cuenta FROM $table, $joinT  $where AND $join $groupBy";
        }

        $result = pg_query($db, $select) or SSP::fatal("Error al ejecutar la consulta.\n" . pg_last_error() . "\n $select");
        $data = pg_fetch_all($result);

        $resTotalLength = pg_query($db, $count);
        $recordsTotalRow = pg_fetch_all($resTotalLength);
        if(!empty($recordsTotalRow)) {
            $recordsTotalC = count($recordsTotalRow);
        } else {
            $recordsTotalC = 0;
        }
        if($recordsTotalC > 1) {
            $recordsTotal = $recordsTotalRow[0];
            $recordsFiltered = $recordsTotalC;
        } else {
            $recordsTotal = $recordsTotalRow[0];
            $recordsFiltered = $recordsTotalRow[0]["cuenta"];
        }

        pg_free_result($result);
        pg_free_result($resTotalLength);

        if(!empty($request['draw'])) {
            $draw = $request['draw'];
        } else {
            $draw = 0;
        }

        return array("draw" => intval($draw),
            "recordsTotal" => intval($recordsTotal),
            "recordsFiltered" => intval($recordsFiltered),
            "data" => SSP::data_output($columns, $data));
    }

    /**
     * Connect to the database
     *
     * @param array $pg_details SQL server connection details array, with the
     *   properties:
     *     * host - host name
     *     * db   - database name
     *     * user - user name
     *     * pass - user password
     *
     * @return resource Database connection handle
     */
    static function pg_connect()
    {
        global $config;

        $db = pg_connect("
			host={$config['database']['host']}
			dbname={$config['database']['schema']}
			user={$config['database']['username']}
			password={$config['database']['password']} options='--client_encoding=UTF8'") or SSP::fatal("Error en conexión a DB.\n" . pg_last_error());

        return $db;
    }
    /**
     * Execute an SQL query on the database
     *
     * @param resource $db Database handler
     * @param string $sql SQL query to execute.
     *
     * @return array         Result from the query (all rows)
     */
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Internal methods
     */
    /**
     * Throw a fatal error.
     *
     * This writes out an error message in a JSON string which DataTables will
     * see and show to the user in the browser.
     *
     * @param string $msg Message to send to the client
     */
    static function fatal($msg)
    {
        echo json_encode(array("error" => $msg));
        exit(0);
    }

    /**
     * Pull a particular property from each assoc. array in a numeric array,
     * returning and array of the property values from each item.
     *
     * @param array $a Array to get data from
     * @param string $prop Property to read
     *
     * @return array        Array of property values
     */
    static function pluck($a, $prop)
    {
        $out = array();
        for($i = 0, $len = count($a); $i < $len; $i++) {
            $out[] = $a[$i][$prop];
        }

        return $out;
    }

    static function pluckas($a)
    {

        $out = array();
        for($i = 0, $len = count($a); $i < $len; $i++) {

            if(isset($a[$i]['table'])) {
                if($a[$i]['table'] != '') {
                    if(isset($a[$i]['alias'])) {
                        if($a[$i]['alias'] != '') {
                            $out[] = $a[$i]['table'] . "." . $a[$i]['db'] . " AS " . $a[$i]['alias'];
                        }
                    }
                    $out[] = $a[$i]['table'] . "." . $a[$i]['db'];
                } else {
                    if($a[$i]['alias'] != '') {
                        $out[] = $a[$i]['db'] . " AS " . $a[$i]['alias'];
                    } else {
                        $out[] = $a[$i]['db'];
                    }
                    $out[] = $a[$i]['db'];
                }
            } else {
                $out[] = $a[$i]['db'];
            }


        }

        return $out;
    }
}