<?php

class Hcms_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    /*     * *************************** */
    /*     * ** Dash Querys ************ */
    /*     * *************************** */
    function getSiteName() {
        $this->db->select("*");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hcms_settings');
        if($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach($results as $u):
                return $u['siteTitle'];
            endforeach;
        }

        return array();
    }

    function checkMaintenance() {
        $this->db->select("*");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hcms_settings');
        if($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach($results as $u):
                return $u['siteMaintenance'];
            endforeach;
        }

        return array();
    }

    function getTheme() {
        // Get Theme
        $this->db->select("*");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hcms_settings');
        if($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach($results as $u):
                return $u['siteTheme'];
            endforeach;
        }

        return array();
    }

    function getLang() {
        // Get Theme
        $this->db->select("*");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hcms_settings');
        if($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach($results as $u):
                return $u['siteLang'];
            endforeach;
        }

        return array();
    }

    function getUpdatedPages() {
        // Get most recently updated pages
        $this->db->select("pageTitle, hcms_page_attributes.pageID, pageUpdated, pageContentHTML");
        $this->db->join('hcms_page_content', 'hcms_page_content.pageID = hcms_page_attributes.pageID');
        $this->db->join('hcms_page_meta', 'hcms_page_meta.pageID = hcms_page_attributes.pageID');
        $this->db->order_by("pageUpdated", "desc");
        $this->db->limit(5);
        $query = $this->db->get('hcms_page_attributes');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function getIndexContacts() {
        // Get most recently updated pages
        $this->db->select("*");
        $this->db->order_by("id", "desc");
        $this->db->limit(5);
        $query = $this->db->get('hcms_contact');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function getCountContacts($status = 0) {
        // Get most recently updated pages
        $this->db->select("*");
        $this->db->where("status", $status);
        $this->db->order_by("id", "desc");

        $query = $this->db->get('hcms_contact');

        return $query->num_rows();


        return array();
    }

    /*     * *************************** */
    /*     * ** User Querys ************ */
    /*     * *************************** */
    function countUsers() {
        return $this->db->count_all('hcms_user');
    }

    function getUsers($limit, $offset = 0) {
        // Get a list of all user accounts
        $this->db->select("userName, email, userID");
        $this->db->order_by("userName", "asc");
        $this->db->limit($limit, $offset);
        $query = $this->db->get('hcms_user');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function getUser($id) {
        // Get the user details
        $this->db->select("*");
        $this->db->where("userID", $id);
        $query = $this->db->get('hcms_user');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function getUserEmail($id) {
        // Get the user email address
        $this->db->select("email");
        $this->db->where("userID", $id);
        $query = $this->db->get('hcms_user');
        if($query->num_rows() > 0) {
            foreach($query->result() as $rows) {
                $email = $rows->email;

                return $email;
            }
        }
    }

    function createUser() {
        // Create the user account
        $data = array('userName' => $this->input->post('username'),
                      'email'    => $this->input->post('email'),
                      'password' => md5($this->input->post('password') . SALT),);
        $this->db->insert('hcms_user', $data);
    }

    function updateUser($id) {
        // update the user account
        $data = array('email'    => $this->input->post('email'),
                      'password' => md5($this->input->post('password') . SALT),);
        $this->db->where('userID', $id);
        $this->db->update('hcms_user', $data);
    }

    function removeUser($id) {
        // Delete a user account
        $this->db->delete('hcms_user', array('userID' => $id));
    }

    function login($username, $password) {
        $this->db->select("*");
        $this->db->where("userName", $username);
        $this->db->where("password", $password);
        $query = $this->db->get("hcms_user");
        if($query->num_rows() > 0) {
            foreach($query->result() as $rows) {
                $data = array('userID'    => $rows->userID,
                              'userName'  => $rows->userName,
                              'logged_in' => TRUE,);

                $this->session->set_userdata($data);

                return TRUE;
            }
        } else {
            return FALSE;
        }
    }



    /*     * *************************** */
    /*     * ** Contact Querys ************ */
    /*     * *************************** */
    function countContact($status = 0) {
        $this->db->where("status", $status);

        return $this->db->count_all('hcms_contact');
    }

    function getContacts($limit, $offset = 0) {
        // Get a list of all user accounts
        $this->db->select("*");
        $this->db->order_by("id", "desc");
        $this->db->limit($limit, $offset);
        $query = $this->db->get('hcms_contact');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function getContact($id) {
        $this->db->where("id", $id);
        $this->db->update('hcms_contact', array("status" => 1));
        // Get the user details
        $this->db->select("*");
        $this->db->where("id", $id);
        $query = $this->db->get('hcms_contact');

        if($query->num_rows() > 0) {
            return $query->row_array();
        }

        return array();
    }

    function removeContact($id) {
        // Delete a page
        $this->db->delete('hcms_contact', array('id' => $id));
    }
    /*     * *************************** */
    /*     * ** Page Querys ************ */
    /*     * *************************** */
    function pageSearch($term) {
        $this->db->select("*");
        $this->db->like("pageTitle", $term);
        $this->db->join('hcms_page_content', 'hcms_page_content.pageID = hcms_page_attributes.pageID');
        $this->db->join('hcms_page_meta', 'hcms_page_meta.pageID = hcms_page_attributes.pageID');
        $query = $this->db->get('hcms_page_attributes');
        if($term == "") {
            $this->db->limit(15);
        }
        if($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach($results as $p):
                echo '<tr>';
                echo '<td>' . $p['navTitle'] . '</td>';
                echo '<td>' . $p['pageUpdated'] . '</td>';
                echo '<td>' . $p['pageCreated'] . '</td>';
                echo '<td>' . ($p['pagePublished'] ? '<span class="fa fa-2x fa-check-circle"></span>' : '<span class="fa fa-2x fa-times-circle"></span>') . '</td>';
                echo '<td class="td-actions"><a href="' . BASE_URL . '/admin/pages/jumbo/' . $p['pageID'] . '" class="btn btn-small btn-primary">' . $this->lang->line('btn_jumbotron') . '</a> <a href="' . BASE_URL . '/admin/pages/edit/' . $p['pageID'] . '" class="btn btn-small btn-success"><i class="fas fa-fw fa-pencil-alt"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="' . BASE_URL . '/admin/pages/delete/' . $p['pageID'] . '"><i class="fas fa-fw fa-eraser"> </i></a></td>';
                echo '</tr>';
            endforeach;
        } else {
            echo "<tr><td colspan='5'><p>" . $this->lang->line('no_results') . "</p></td></tr>";
        }
    }

    function countPages() {
        return $this->db->count_all('hcms_page_attributes');
    }

    function getPages($limit, $offset = 0) {
        // Get a list of all pages
        $this->db->select("*");
        $this->db->join('hcms_page_content', 'hcms_page_content.pageID = hcms_page_attributes.pageID');
        $this->db->join('hcms_page_meta', 'hcms_page_meta.pageID = hcms_page_attributes.pageID');
        $this->db->limit($limit, $offset);
        $query = $this->db->get('hcms_page_attributes');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function getPagesAll() {
        // Get a list of all pages
        $this->db->select("*");
        $this->db->join('hcms_page_content', 'hcms_page_content.pageID = hcms_page_attributes.pageID');
        $this->db->join('hcms_page_meta', 'hcms_page_meta.pageID = hcms_page_attributes.pageID');
        $query = $this->db->get('hcms_page_attributes');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function getPostsAll() {
        // Get a list of all pages
        $this->db->select("*");
        $query = $this->db->get('hcms_post');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function getCategoryAll() {
        // Get a list of all pages
        $this->db->select("*");
        $query = $this->db->get('hcms_post_category');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }


    function createPage() {
        // Create the page
        $data = array('pagePublished' => $this->input->post('pagePublished'),
                      'pageTemplate'  => $this->input->post('pageTemplate'),
                      'pageURL'       => $this->input->post('pageURL'));
        $this->db->insert('hcms_page_attributes', $data);

        $HTMLContent = $this->input->post('content');

        $this->db->select("*");
        $this->db->where("pageURL", $this->input->post('pageURL'));
        $query = $this->db->get("hcms_page_attributes");
        if($query->num_rows() > 0) {
            foreach($query->result() as $rows) {
                $contentdata = array('pageID'          => $rows->pageID,
                                     'pageTitle'       => $this->input->post('pageTitle'),
                                     'navTitle'        => $this->input->post('navTitle'),
                                     'pageContent'     => $this->input->post('content'),
                                     'pageContentHTML' => $HTMLContent,);
                $this->db->insert('hcms_page_content', $contentdata);
                $metadata = array('pageID'          => $rows->pageID,
                                  'pageKeywords'    => $this->input->post('pageKeywords'),
                                  'pageDescription' => $this->input->post('pageDescription'),);
                $this->db->insert('hcms_page_meta', $metadata);
            }
        }
    }

    function getPage($id) {
        // Get the page details
        $this->db->select("*");
        $this->db->where("hcms_page_attributes.pageID", $id);
        $this->db->join('hcms_page_content', 'hcms_page_content.pageID = hcms_page_attributes.pageID');
        $this->db->join('hcms_page_meta', 'hcms_page_meta.pageID = hcms_page_attributes.pageID');
        $query = $this->db->get('hcms_page_attributes');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function getPageBanners($id) {
        // Get the page banners
        $this->db->select("*");
        $this->db->where("pageID", $id);
        $this->db->order_by("slideOrder ASC");
        $query = $this->db->get('hcms_banner');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function removePage($id) {
        // Delete a page
        $this->db->delete('hcms_page_content', array('pageID' => $id));
        $this->db->delete('hcms_page_meta', array('pageID' => $id));
        $this->db->delete('hcms_page_attributes', array('pageID' => $id));
    }

    function getPageURL($id) {
        // Get the page URL
        $this->db->select("pageURL");
        $this->db->where("pageID", $id);
        $query = $this->db->get('hcms_page_attributes');
        if($query->num_rows() > 0) {
            foreach($query->result() as $rows) {
                $pageURL = $rows->pageURL;

                return $pageURL;
            }
        }
    }

    function updatePage($id) {
        // Update the page


        $HTMLContent = $this->input->post('content');


        if($id != 1) {
            $data = array('pagePublished' => $this->input->post('pagePublished'),
                          'pageURL'       => $this->input->post('pageURL'),
                          'pageTemplate'  => $this->input->post('pageTemplate'),);
        } else {
            $data = array('pagePublished' => $this->input->post('pagePublished'),
                          'pageTemplate'  => $this->input->post('pageTemplate'),);
        }
        $this->db->where("pageID", $id);
        $this->db->update('hcms_page_attributes', $data);
        $contentdata = array('pageTitle'       => $this->input->post('pageTitle'),
                             'navTitle'        => $this->input->post('navTitle'),
                             'pageContent'     => $this->input->post('content'),
                             'pageContentHTML' => $HTMLContent,);
        $this->db->where("pageID", $id);
        $this->db->update('hcms_page_content', $contentdata);
        $metadata = array('pageKeywords'    => $this->input->post('pageKeywords'),
                          'pageDescription' => $this->input->post('pageDescription'),);
        $this->db->where("pageID", $id);
        $this->db->update('hcms_page_meta', $metadata);
    }

    function updateJumbotron($id) {


        $HTMLContent = $this->input->post('jumbotron');

        $data = array('enableJumbotron' => $this->input->post('enableJumbotron'),
                      'enableSlider'    => $this->input->post('enableSlider'),);

        $this->db->where("pageID", $id);
        $this->db->update('hcms_page_attributes', $data);
        $contentdata = array('jumbotron'     => $this->input->post('jumbotron'),
                             'jumbotronHTML' => $HTMLContent,);
        $this->db->where("pageID", $id);
        $this->db->update('hcms_page_content', $contentdata);

        // Clear the sliders
        $this->db->delete('hcms_banner', array('pageID' => $id));

        if(!empty($this->input->post("postImage"))) {
            foreach($this->input->post("postImage") as $key => $item) {
                $slidedata = array('pageID'     => $id,
                                   'slideImage' => $item,
                                   'slideLink'  => $this->input->post("link")[$key],
                                   'slideAlt'   => $this->input->post("alt")[$key],
                                   'slideHtml'  => $this->input->post("html")[$key],
                                   'slideOrder' => $this->input->post("order")[$key],);

                $this->db->insert('hcms_banner', $slidedata);
            }
        }


    }

    /*     * *************************** */
    /*     * ** Navigation Querys ****** */
    /*     * *************************** */
    function countNavigation() {
        return $this->db->count_all('hcms_navigation');
    }

    function getAllNav($limit, $offset = 0) {
        // Get a list of all pages
        $this->db->select("*");
        $this->db->limit($limit, $offset);
        $query = $this->db->get('hcms_navigation');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function getNav($id) {
        // Get a list of all pages
        $this->db->select("*");
        $this->db->where("navSlug", $id);
        $query = $this->db->get('hcms_navigation');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    //Get page details for building nav
    function getPageNav($url) {
        // Get the page details
        $this->db->select("*");
        $this->db->where("hcms_page_attributes.pageURL", $url);
        $this->db->join('hcms_page_content', 'hcms_page_content.pageID = hcms_page_attributes.pageID');
        $this->db->join('hcms_page_meta', 'hcms_page_meta.pageID = hcms_page_attributes.pageID');
        $query = $this->db->get('hcms_page_attributes');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }


    function getCat($url) {
        // Get the page details
        $this->db->select("*");
        $this->db->where("hcms_post_category.categorySlug", $url);
        $query = $this->db->get('hcms_post_category');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function insertNav() {
        $navigationHTML = $this->input->post('convertedNav');
        $navigationHTML = str_replace("<ul></ul>", "", $navigationHTML);
        $navigationEdit = $this->input->post('seriaNav');
        $navigationEdit = str_replace('<button data-action="collapse" type="button">Collapse</button><button style="display: none;" data-action="expand" type="button">Expand</button>',
                                      "", $navigationEdit);

        $data = array('navSlug'  => $this->input->post('navSlug'),
                      'navTitle' => $this->input->post('navTitle'),
                      'navEdit'  => $navigationEdit,
                      'navHTML'  => $navigationHTML,);
        $this->db->insert('hcms_navigation', $data);
    }

    function updateNav($id) {

        $navigationHTML = $this->input->post('convertedNav');
        $navigationHTML = str_replace("<ul></ul>", "", $navigationHTML);
        $navigationEdit = $this->input->post('seriaNav');
        $navigationEdit = str_replace('<button data-action="collapse" type="button">Collapse</button><button style="display: none;" data-action="expand" type="button">Expand</button>',
                                      "", $navigationEdit);

        $data = array('navTitle' => $this->input->post('navTitle'),
                      'navEdit'  => $navigationEdit,
                      'navHTML'  => $navigationHTML,);
        $this->db->where("navSlug", $id);
        $this->db->update('hcms_navigation', $data);
    }

    function removeNav($id) {
        // Delete a nav
        $this->db->delete('hcms_navigation', array('navSlug' => $id));
    }


    function getSettings() {
        // Get the settings
        $this->db->select("*");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hcms_settings');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }


    function updateSettings() {
        $data = array('siteTheme'              => $this->input->post('siteTheme'),
                      'siteLang'               => $this->input->post('siteLang'),
                      'siteFooter'             => $this->input->post('siteFooter'),
                      'siteMaintenance'        => $this->input->post('siteMaintenance'),
                      'siteMaintenanceHeading' => $this->input->post('siteMaintenanceHeading'),
                      'siteMaintenanceMeta'    => $this->input->post('siteMaintenanceMeta'),
                      'siteMaintenanceContent' => $this->input->post('siteMaintenanceContent'),
                      'siteAdditionalJS'       => $this->input->post('siteAdditionalJS'));

        if($this->input->post('siteTitle') != "") {
            $data['siteTitle'] = $this->input->post('siteTitle');
        }

        if($this->input->post('siteLogo') != "") {
            $data['siteLogo'] = $this->input->post('siteLogo');
        }
        if($this->input->post('siteFavicon') != "") {
            $data['siteFavicon'] = $this->input->post('siteFavicon');
        }
        $this->db->where("siteID", 0);
        $this->db->update('hcms_settings', $data);
    }


    /*     * *************************** */
    /*     * ** Post Querys ************ */
    /*     * *************************** */
    function postSearch($term) {
        $this->db->select("*");
        $this->db->like("postTitle", $term);
        $this->db->join('hcms_post_category', 'hcms_post_category.categoryID = hcms_post.categoryID');
        $this->db->order_by("unixStamp", "desc");
        if($term == "") {
            $this->db->limit(15);
        }
        $query = $this->db->get('hcms_post');
        if($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach($results as $p):
                echo '<tr>';
                echo '<td>' . $p['postTitle'] . '</td>';
                echo '<td>' . $p['categoryTitle'] . '</td>';
                echo '<td>' . $p['datePosted'] . '</td>';
                echo '<td>' . ($p['published'] ? '<span class="fa fa-2x fa-check-circle"></span>' : '<span class="fa fa-2x fa-times-circle"></span>') . '</td>';
                echo '<td class="td-actions"><a href="' . BASE_URL . '/admin/posts/edit/' . $p['postID'] . '" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="' . BASE_URL . '/admin/posts/delete/' . $p['postID'] . '"><i class="fa fa-remove"> </i></a></td>';
                echo '</tr>';
            endforeach;
        } else {
            echo "<tr><td colspan='5'><p>" . $this->lang->line('no_results') . "</p></td></tr>";
        }
    }

    function countPosts() {
        return $this->db->count_all('hcms_post');
    }

    function getPosts($limit, $offset = 0) {
        // Get a list of all posts
        $this->db->select("*");
        $this->db->join('hcms_post_category', 'hcms_post_category.categoryID = hcms_post.categoryID');
        $this->db->order_by("hcms_post.categoryID", "desc");
        $this->db->order_by("postOrder", "asc");

        $this->db->limit($limit, $offset);
        $query = $this->db->get('hcms_post');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function createPost() {
        // Create the post

        $data = array('postTitle'       => $this->input->post('postTitle'),
                      'categoryID'      => $this->input->post('categoryID'),
                      'postURL'         => $this->input->post('postURL'),
                      'postContent'     => $this->input->post('content'),
                      'postExcerptHTML' => $this->input->post('postExcerptHTML'),
                      'postExcerpt'     => $this->input->post('postExcerpt'),
                      'published'       => $this->input->post('published'),
                      'postOrder'       => $this->input->post('postOrder'),
                      'postHome'        => $this->input->post('postHomegetSocial'),
                      'datePosted'      => $this->input->post('datePosted'),
                      'unixStamp'       => $this->input->post('unixStamp'),);
        if($this->input->post('postImage') != "") {
            $data['postImage'] = $this->input->post('postImage');
        }
        if($this->input->post('postImage2') != "") {
            $data['postImage2'] = $this->input->post('postImage2');
        }
        if($this->input->post('postImage3') != "") {
            $data['postImage3'] = $this->input->post('postImage3');
        }
        if($this->input->post('postImage4') != "") {
            $data['postImage4'] = $this->input->post('postImage4');
        }
        $this->db->insert('hcms_post', $data);
    }

    function getPost($id) {
        // Get the post details
        $this->db->select("*");
        $this->db->where("postID", $id);
        $this->db->join('hcms_post_category', 'hcms_post_category.categoryID = hcms_post.categoryID');
        $query = $this->db->get('hcms_post');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function getPostUrl($url) {

        // Get the post details
        $this->db->select("*");
        $this->db->where("postURL", $url);
        $query = $this->db->get('hcms_post');
        if($query->num_rows() > 0) {
            return $query->row_array();
        }

        return array();
    }

    function removePost($id) {
        // Delete a post
        $this->db->delete('hcms_post', array('postID' => $id));
    }

    function updatePost($id) {
        // Update the post


        $data = array('postTitle'       => $this->input->post('postTitle'),
                      'categoryID'      => $this->input->post('categoryID'),
                      'postURL'         => $this->input->post('postURL'),
                      'postContent'     => $this->input->post('content'),
                      'postImage'       => $this->input->post('postImage'),
                      'postExcerptHTML' => $this->input->post('postExcerptHTML'),
                      'postExcerpt'     => $this->input->post('postExcerpt'),
                      'postOrder'       => $this->input->post('postOrder'),
                      'postHome'        => $this->input->post('postHome'),
                      'published'       => $this->input->post('published'),
                      'datePosted'      => $this->input->post('datePosted'),
                      'unixStamp'       => $this->input->post('unixStamp'),);
 
        $data['postImage'] = $this->input->post('postImage');
        $data['postImage2'] = $this->input->post('postImage2');
        $data['postImage3'] = $this->input->post('postImage3');
        $data['postImage4'] = $this->input->post('postImage4');
        $this->db->where("postID", $id);
        $this->db->update('hcms_post', $data);
    }


    /*     * *************************** */
    /*     * ** Category Querys ******** */
    /*     * *************************** */
    function countCategories() {
        return $this->db->count_all('hcms_post_category');
    }

    function getCategories() {
        // Get a list of all categories
        $this->db->select("*");
        $query = $this->db->get('hcms_post_category');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }


    function getCategoriesAll($limit, $offset = 0) {
        // Get a list of all categories
        $this->db->select("*");
        $this->db->limit($limit, $offset);
        $query = $this->db->get('hcms_post_category');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function createCategory() {
        // Create the category

        $data = array('categoryTitle'       => $this->input->post('categoryTitle'),
                      'categorySlug'        => $this->input->post('categorySlug'),
                      'categoryDescription' => $this->input->post('categoryDescription'));

        $this->db->insert('hcms_post_category', $data);
    }

    function getCategory($id) {
        // Get the category details
        $this->db->select("*");
        $this->db->where("categoryID", $id);
        $query = $this->db->get('hcms_post_category');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    function removeCategory($id) {
        // Delete a category
        $this->db->delete('hcms_post_category', array('categoryID' => $id));
    }

    function updateCategory($id) {
        // Update the category
        $data = array('categoryTitle'       => $this->input->post('categoryTitle'),
                      'categorySlug'        => $this->input->post('categorySlug'),
                      'categoryDescription' => $this->input->post('categoryDescription'));

        $this->db->where("categoryID", $id);
        $this->db->update('hcms_post_category', $data);
    }

    /*     * *************************** */
    /*     * ** Social Querys ********** */
    /*     * *************************** */

    function getSocial() {
        $this->db->select("*");
        $this->db->where("socialEnabled", 1);
        $query = $this->db->get('hcms_social');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }


    function updateSocial() {
        $this->db->select("*");
        $query = $this->db->get("hcms_social");
        if($query->num_rows() > 0) {
            foreach($query->result() as $rows) {
                $data = array();
                $data['socialLink'] = $this->input->post($rows->socialName);
                if(isset($_POST['checkbox' . $rows->socialName])) {
                    $data['socialEnabled'] = $this->input->post('checkbox' . $rows->socialName);
                } else {
                    $data['socialEnabled'] = 0;
                }
                $this->db->where("socialName", $rows->socialName);
                $this->db->update('hcms_social', $data);
            }
        }
    }

    public function get_link_tree($group, $params = array()) {
        // the plugin passes the abbreviation
        if(!is_numeric($group)) {
            $row = $this->get_group_by('href', $group);
            $group = $row ? $row->id : NULL;
        }

        if(!empty($params['order'])) {
            $this->db->order_by($params['order']);
        } else {
            $this->db->order_by('position');
        }

        if(isset($params['front_end']) and $params['front_end']) {
            $front_end = TRUE;
        } else {
            $front_end = FALSE;
        }

        if(isset($params['user_group'])) {
            $user_group = $params['user_group'];
        } else {
            $user_group = FALSE;
        }

        $all_links = $this->db->where('navigation_group_id', $group)->get($this->_table)->result_array();

        $this->load->helper('url');

        $links = array();

        // we must reindex the array first and build urls
        $all_links = $this->make_url_array($all_links, $user_group, $front_end);
        foreach($all_links AS $row) {
            $links[$row['id']] = $row;
        }

        unset($all_links);

        $link_array = array();

        // build a multidimensional array of parent > children
        foreach($links AS $row) {
            if(array_key_exists($row['parent'], $links)) {
                // add this link to the children array of the parent link
                $links[$row['parent']]['children'][] =& $links[$row['id']];
            }

            if(!isset($links[$row['id']]['children'])) {
                $links[$row['id']]['children'] = array();
            }

            // this is a root link
            if($row['parent'] == 0) {
                $link_array[] =& $links[$row['id']];
            }
        }

        return $link_array;
    }

    public function make_url_array($links, $user_group = FALSE, $front_end = FALSE) {
        // We have to fetch it ourselves instead of just using $current_user because this
        // will all be cached per user group
        $group = $this->db->select('id')->where('name', $user_group)->get('groups')->row();

        foreach($links as $key => &$row) {
            // Looks like it's restricted. Let's find out who
            if($row['restricted_to'] and $front_end) {
                $row['restricted_to'] = (array)explode(',', $row['restricted_to']);

                if(!$user_group or ($user_group != 'admin' AND !in_array($group->id, $row['restricted_to']))) {
                    unset($links[$key]);
                }
            }

            // If its any other type than a URL, it needs some help becoming one
            switch($row['link_type']) {
                case 'uri':
                    $row['url'] = site_url($row['uri']);
                    break;

                case 'module':
                    $row['url'] = site_url($row['module_name']);
                    break;

                case 'page':
                    if($page = $this->page_m->get_by(array_filter(array('id'     => $row['page_id'],
                                                                        'status' => ($front_end ? 'live' : NULL))))) {
                        $row['url'] = site_url($page->uri);
                        $row['is_home'] = $page->is_home;

                        // But wait. If we're on the front-end and they don't have access to the page then we'll remove it anyway.
                        if($front_end and $page->restricted_to) {
                            $page->restricted_to = (array)explode(',', $page->restricted_to);

                            if(!$user_group or ($user_group != 'admin' and !in_array($group->id,
                                                                                     $page->restricted_to))) {
                                unset($links[$key]);
                            }
                        }
                    } else {
                        unset($links[$key]);
                    }
                    break;
            }
        }

        return $links;
    }


    public function getWidgets() {
        // Get a list of all pages
        $this->db->select("*");
        $query = $this->db->get('hcms_widgets');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    public function createWidget() {
        // Create the user account
        $data = array('title'   => $this->input->post('title'),
                      'content' => $this->input->post('content'));
        $this->db->insert('hcms_widgets', $data);
    }

    public function getWidget($id) {
        // Get the user details
        $this->db->select("*");
        $this->db->where("id", $id);
        $query = $this->db->get('hcms_widgets');
        if($query->num_rows() > 0) {
            return $query->result_array();
        }

        return array();
    }

    public function updateWidget($id) {
        // update the user account
        $data = array('title'   => $this->input->post('title'),
                      'content' => $this->input->post('content'));
        $this->db->where('id', $id);
        $this->db->update('hcms_widgets', $data);
    }

    public function removeWidget($id) {
        // Delete a user account
        $this->db->delete('hcms_widgets', array('id' => $id));
    }
}

?>