<?php

class Hcms_page_model extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    public function getPage($pageURL) {
        // Get page
        $this->db->select("*");
        $this->db->join('hcms_page_content', 'hcms_page_content.pageID = hcms_page_attributes.pageID');
        $this->db->join('hcms_page_meta', 'hcms_page_meta.pageID = hcms_page_attributes.pageID');
        $this->db->where("pagePublished", 1);
        $this->db->where("pageURL", $pageURL);
        $query = $this->db->get('hcms_page_attributes');
        if($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach($results as $u):
                $page = array('pageID'          => $u['pageID'],
                              'pageTitle'       => $u['pageTitle'],
                              'pageKeywords'    => $u['pageKeywords'],
                              'pageDescription' => $u['pageDescription'],
                              'pageContentHTML' => $u['pageContentHTML'],
                              'pageContent'     => $u['pageContent'],
                              'pageTemplate'    => $u['pageTemplate'],
                              'enableJumbotron' => $u['enableJumbotron'],
                              'enableSlider'    => $u['enableSlider'],
                              'jumbotronHTML'   => $u['jumbotronHTML'],);
            endforeach;

            return $page;

        }

        return array('pageID'       => "",
                     'pageTemplate' => "");
    }

    public function getCategory($catSlug) {
        // Get category

        $this->db->select("*");
        $this->db->where("categorySlug", $catSlug);
        $query = $this->db->get('hcms_post_category');
        if($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach($results as $u):
                $category = array('pageID'          => $u['categoryID'],
                                  'categoryID'      => $u['categoryID'],
                                  'pageTitle'       => $u['categoryTitle'],
                                  'pageKeywords'    => '',
                                  'pageDescription' => $u['categoryDescription'],);
            endforeach;

            return $category;
        }

        return array('categoryID' => "");
    }

    public function countProduct($catSlug = NULL) {
        // Get categor

        if($catSlug == NULL) {
            return $this->db->count_all('hcms_post');
        }

        $this->db->where("categorySlug", $catSlug);
        return $this->db->count_all('hcms_post');
    }

    public function getArticle($postURL) {
        // Get article
        $this->db->select("*");
        $this->db->where("postURL", $postURL);
        $this->db->where("published", 1);
        $this->db->join('hcms_post_category', 'hcms_post_category.categoryID = hcms_post.categoryID');
        $query = $this->db->get('hcms_post');
        if($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach($results as $u):
                $category = array('pageID'          => $u['postID'],
                                  'postID'          => $u['postID'],
                                  'pageTitle'       => $u['postTitle'],
                                  'pageKeywords'    => "",
                                  'pageDescription' => $u['postExcerpt'],
                                  'postContent'     => $u['postContent'],
                                  'postExcerpt'     => $u['postExcerpt'],
                                  'postExcerptHTML' => $u['postExcerptHTML'],
                                  'postImage'       => $u['postImage'],
                                  'postImage2'       => $u['postImage2'],
                                  'postImage3'       => $u['postImage3'],
                                  'postImage4'       => $u['postImage4'],
                                  'datePosted'      => $u['datePosted'],
                                  'categoryTitle'   => $u['categoryTitle'],
                                  'categorySlug'    => $u['categorySlug'],);
            endforeach;

            return $category;

        }

        return array('postID' => "");
    }

    public function getSettings() {
        // Get settings
        $this->db->select("*");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hcms_settings');
        if($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach($results as $u):
                $page = array('siteLogo'               => $u['siteLogo'],
                              'siteFavicon'            => $u['siteFavicon'],
                              'siteTitle'              => $u['siteTitle'],
                              'siteTheme'              => $u['siteTheme'],
                              'siteFooter'             => $u['siteFooter'],
                              'siteMaintenanceHeading' => $u['siteMaintenanceHeading'],
                              'siteMaintenanceMeta'    => $u['siteMaintenanceMeta'],
                              'siteMaintenanceContent' => $u['siteMaintenanceContent'],
                              'siteMaintenance'        => $u['siteMaintenance'],
                              'siteAdditionalJS'       => $u['siteAdditionalJS'],
                              'businessAddress'        => $u['businessAddress'],
                              'businessPhone'          => $u['businessPhone'],);
            endforeach;

            return $page;

        }

        return array();
    }

    public function getSocials() {
        // Get settings
        $this->db->select("*");
        $this->db->where("socialEnabled", 1);
        $query = $this->db->get('hcms_social');
        if($query->num_rows() > 0) {
            $results = $query->result_array();

            return $results;

        }

        return array();
    }

    public function homeProducts() {
        // Get settings
        $this->db->select("postURL,postTitle,postExcerpt,postImage,categorySlug");
        $this->db->where("postHome", 1);
        $this->db->join('hcms_post_category', 'hcms_post_category.categoryID = hcms_post.categoryID');
        $query = $this->db->get('hcms_post');
        if($query->num_rows() > 0) {
            $results = $query->result_array();

            return $results;

        }

        return array();
    }

    public function concact_send($type) {

        $contentdata = array('type'    => $type,
                             'name'    => $this->input->post('name'),
                             'phone'   => $this->input->post('phone'),
                             'message' => $this->input->post('message'));
        if($type == 3) {
            $a = array("file" => $this->upload->data("orig_name"));
            $contentdata = array_merge($a,$contentdata);
        }

        $this->db->insert('hcms_contact', $contentdata);
    }

    public function getSearch($q) {
        // Get article
        $this->db->select("*");
        $this->db->like('postContent', $q, 'both');
        $this->db->where("published", 1);
        $this->db->join('hcms_post_category', 'hcms_post_category.categoryID = hcms_post.categoryID');
        $query = $this->db->get('hcms_post');
        if($query->num_rows() > 0) {
            $results = $query->result_array();

            foreach($results as $u):
                $category[] = array('pageID'          => $u['postID'],
                                    'postID'          => $u['postID'],
                                    'pageTitle'       => $u['postTitle'],
                                    'pageKeywords'    => "",
                                    'pageDescription' => $u['postExcerpt'],
                                    'postContent'     => $u['postContent'],
                                    'postExcerpt'     => $u['postExcerpt'],
                                    'postImage'       => $u['postImage'],
                                    'datePosted'      => $u['datePosted'],
                                    'postURL'         => $u['postURL'],
                                    'categoryTitle'   => $u['categoryTitle'],
                                    'categorySlug'    => $u['categorySlug'],);
            endforeach;


            return $category;
        }

        return array();
    }

    public function getSearchCount($q) {
        // Get article
        $this->db->select("*");
        $this->db->like('postContent', $q, 'both');
        $this->db->where("published", 1);
        $this->db->join('hcms_post_category', 'hcms_post_category.categoryID = hcms_post.categoryID');
        $query = $this->db->get('hcms_post');

        return $query->num_rows();
    }

}

?>