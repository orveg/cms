<footer class="bg-three">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 mmx-fo-box">
                <img src="<?php echo THEME_FOLDER; ?>/images/ersistem-white-logo.svg" class="fo-logo">
                <p>
                    <?php echo $settings['siteFooter']; ?>
                </p>
            </div>
            <div class="col-lg-4 col-md-6 mmx-fo-box">
                <h4 class="mmx-fo-title">Bize Ulaşın</h4>
                <div class="mmx-fo-txt">
                    <p>
                        T. +90 216 610 03 06<br>
                        M. info@ersistem.com.tr
                        <?php echo $settings['businessPhone']; ?>
                    </p>
                    <p>
                        CEMİL MERİÇ MAHALLESİ, ÇAYIRÖNÜ CADDESİ NO:13/C
                        <br>
                        ÜMRANİYE / İSTANBUL
                        <?php echo $settings['businessAddress'];
                        ?>
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 mmx-fo-box">
                <h4 class="mmx-fo-title">Sosyal Medya</h4>
                <ul class="mmx-social">
                    <?php foreach ($socials as $social) { ?>
                        <li>
                            <a target="_blank" href="<?= $social["socialLink"] ?>"><i class="fab <?= $social["class"] ?>"></i></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</footer>
<section class="bg-four p-1">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-md-12">
                <span class="ersistem">© <?= date("Y") ?> <span>  <?php echo $settings['siteTitle']; ?> </span> Her Hakkı Saklıdır.</span>
                <a style="display: none" href="" class="mmx-private">Site Haritası</a>
                <a href="#" data-toggle="modal"
                   data-target="#privateModal" class="mmx-private">Kişisel Verilerin Korunması Kanunu</a>
            </div>
            <div class="col-lg-2 col-md-2  ">
                <a href="https://mediaminerva.com/" target="_blank">
                    <img src="<?php echo THEME_FOLDER; ?>/images/mminerva.png" class="mmx-img-fluid2"></a>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="formModal" tabindex="-1" role="dialog"
     aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> Ücretsiz Keşif Talep Formu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="mmx-form modal-form">

                    <p class="font12">Ücretsiz keşif için sizi ziyaret edelim, size en uygun sistemi
                        sunalım.</p>
                    <form action="/iletisim-gonder/2" method="post" id="free-1">
                        <div class="form-group">
                            <input type="text" placeholder="Adınız & Soyadınız" name="name" class="form-control"
                                   required>
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="İrtibat Numaranız" name="phone" class="form-control"
                                   required data-mask="000-000-00-00" data-mask-selectonfocus="true">
                        </div>
                        <div class="form-group">
                            <textarea name="message"  placeholder="Mesajınız"  class="form-control"></textarea>
                            </textarea>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" name="defaultCheck1"
                                   id="defaultCheck1" required>
                            <label class="form-check-label" for="defaultCheck1" data-toggle="modal"
                                   data-target="#privateModal">
                                Kişisel verilerin
                                korunması kanununu okudum, kabul ediyorum.
                            </label>
                        </div>

                        <div class="form-group text-center">
                            <input type="submit" value="Ücretsiz Keşif" class="btn-submit btn-danger">
                        </div>
                        <p class="safe"><i class="fas fa-user-shield"></i>Bilgileriniz bizimle güvende</p>

                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade mmx-private" id="privateModal" tabindex="-1" role="dialog"
     aria-labelledby="privateModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Kişisel Verilerin Korunması Kanunu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-body">
                    <p>Bu Taahhütname, 6698 sayılı Kişisel Verilerin Korunması Kanunu (“6698 Sayılı Kanun”) ile
                        düzenlenen gerçek kişilerin “Kişisel Verilerinin” işlenmesi,
                        depolanması ve paylaşımı kapsamında tarafınız ile mevcut hizmet sözleşmenizin/abonelik
                        sözleşmenizin/satım sözleşmenizin/vb. eki ve ayrılmaz
                        bir parçası olarak hazırlanmıştır.</p>
                    <p>Bu Taahhütname ve 6698 Sayılı Kanun kapsamında, “Kişisel Veri”; Şirketimiz ile tarafınız arasında
                        imzalanmış olan sözleşme dâhilinde hizmet sözleşmesinin/abonelik sözleşmesinin/satım
                        sözleşmesinin/vb. imzalandığı itibaren sözleşme içeriğinde, sözleşme formunda ve sözleşme eki ve
                        ayrılmaz parçalarında yer alan;</p>
                    <p>• Adınız, soyadınız, telefon numaranız, TC Kimlik numaranız, elektronik posta adresiniz, müşteri
                        numaranız, sözleşme numaranız, aboneliğinize dair tarafınıza atanan kullanıcı kimliğiniz gibi
                        sizi tanımlayabileceğimiz kişisel verileriniz,</p>
                    <p>• Doğum yeriniz ve tarihiniz, medeni durumunuz, cinsiyetiniz ve sair kimlik bilgileriniz,</p>
                    <p>• Aboneliğiniz kapsamındaki tarife ve paket bilgileriniz, fatura, finansal ve borç bilgileriniz,
                        abonelik türünüz ve kanalınız, abonelik statünüz (iptal, aktif vb.) gibi temel abonelik
                        bilgileriniz,</p>
                    <p>• Cihaz kampanyalarımıza ve servislerimize aboneliklerinizde finansal yeterlilik ön
                        değerlendirmesi yapabilmek için kredi notunuz vb. bilgileriniz,</p>
                    <p>• Satış ve müşteri temsilcileri tarafından çağrı merkezi standartları gereği tutulan sesli
                        görüşme kayıtlarınız ile elektronik posta, mektup veya sair vasıtalar aracılığı ile tarafımızla
                        iletişime geçtiğinizde elde edilen kişisel verileriniz,</p>
                    <p>• Kredi kartı ve banka kartı bilgileriniz, şube kodu, hesap numara ve sair banka
                        bilgileriniz,</p>
                    <p>• Satış kanallarındaki talep ve işlem bilgileriniz gibi satış aracılığı ile elde edilen
                        verileriniz,</p>
                    <p>• Katma değerli servisleri kullanım miktarınız, sağladığınız içerik ve kişisel bilgiler ve sair
                        kayıtlarınız gibi kullanım detaylarınızı içeren kişisel verileriniz,</p>
                    <p>• Şirketimiz internet sitelerindeki ve mobil uygulamalarındaki davranış bilgileriniz ile kullanım
                        verileriniz, sağladığınız içerik ve kişisel bilgiler ile belirli ürün ve hizmetlerimizi
                        kullanımınıza ilişkin tercihleriniz,</p>
                    <p>• Ödeme verileriniz ve sair kayıtlarınız,</p>
                    <p>• Şirketimize iş başvurusunda bulunmanız halinde bu hususta temin edilen özgeçmiş dâhil sair
                        kişisel verileriniz ile Şirketimiz çalışanı ya da ilişkili çalışan olmanız halinde hizmet
                        akdiniz ve işe yatkınlığınız ile ilgili her türlü kişisel verileriniz, dâhil olmak üzere her
                        türlü güncel bilgi “Kişisel Veri” olarak kabul edilecektir.</p>
                    <p>Şirketimiz, 6698 Sayılı Kanun’da tanımlandığı şekli ile “Veri Sorumlusu” sıfatını haiz olarak,
                        Kişisel Veri’nin işleneceğini, kaydedileceğini, depolanacağını, muhafaza edileceğini,
                        gerektiğinde güncelleneceğini, aktarılabileceğini veya şirketimizin bağlı şirketleri,
                        danışmanları, hissedarları ya da çözüm ortakları ile sair ilgili kişileri tarafından Kişisel
                        Veri üzerinde yapılacak her türlü işlem kişisel verinin işlenmesi olarak addedilecektir.</p>
                    <p>Kişisel verileriniz;</p>
                    <p>• Ürün ve hizmetlerimizden yararlanmanız halinde söz konusu işlemi gerçekleştirmek ve siparişin
                        ilerlemesinden sizleri haberdar edebilme,</p>
                    <p>• Sizi yeni ürün ve hizmetlerimizden haberdar edebilme ve size en uygun ürün ve hizmeti
                        sağlayabilme, </p>
                    <p>• Ürün ve hizmetlerimizi kullanış şeklinizden yola çıkarak size teklif sunma ve promosyonlar
                        hakkında bilgi verme, </p>
                    <p>• Ürün ve hizmetlerimizi geliştirme amacıyla analiz yapma, </p>
                    <p>• Çalışanlarımızı eğitme ve geliştirme, </p>
                    <p>• Ürün ve hizmetlerimizi kullanmanız karşılığında faturalandırma yapma, </p>
                    <p>• Kimliğinizi teyit etme, </p>
                    <p>• Acil yardım çağrısı halinde ilgili mevzuata uygun olarak yerinizi tespit etme ve yetkili
                        mercilerle paylaşma, </p>
                    <p>• İlgili ürün ve hizmetlerimize başvurularınızda gerekmesi halinde kredi notu kontrolü ve kredi
                        raporlaması işlemlerini gerçekleştirme, </p>
                    <p>• Ürün ya da hizmetlerimize ilişkin her türlü soru ve şikâyetinize cevap verebilme, </p>
                    <p>• Ürün ve hizmet güvenliği kapsamında tüm gerekli teknik ve idari tedbirleri alma, </p>
                    <p>• Size sunduğumuz ürün ve hizmetlerin geliştirilmesi ve iyileştirilmesi amacıyla ürün ve
                        hizmetlerimizi kullanımınızı analiz etme, </p>
                    <p>• Ürün ve hizmetlerimizin kullanımında suiistimalleri önlemek ve tespit etme, </p>
                    <p>• Düzenleyici ve denetleyici kurumlarla, resmi mercilerin talep ve denetimleri doğrultusunda
                        gerekli bilgilerin temini, </p>
                    <p>• İlgili mevzuat gereği saklanması gereken aboneliğinize ilişkin bilgileri muhafaza etme, </p>
                    <p>• İlgili iş ortaklarımız ve sair üçüncü kişilerle sizlere sunduğumuz ürün ve hizmetlerle ilgili
                        finansal mutabakat sağlanması, </p>
                    <p>• Bilgilerinizin tutarlılığının kontrolünün sağlanması, </p>
                    <p>• Müşteri memnuniyetinin ölçülmesi, </p>
                    <p>• Ödül ve çekiliş yapılması, </p>
                    <p>• Müşteri hizmetleri operasyonlarının yürütülmesi dahil ve bunlarla sınırlı olmaksızın, ürün ve
                        servislerin analizi, kampanya, tarife, ürün, strateji belirleme ve ölçme gibi pazarlama
                        aktiviteleri ve iletişimi, müşteri hizmetleri ve memnuniyeti, finansal raporlama ve analiz,
                        kanuni takip, servis optimizasyonu ve benzeri amaçlarla işlenebilecektir. </p>
                    <p>• Ayrıca kişisel verileriniz, mevzuat hükümleri çerçevesinde işlenebilecek olup, Şirketimiz’e ait
                        fiziki arşivler ve bilişim sistemlerine nakledilerek, hem dijital ortamda hem de fiziki ortamda
                        muhafaza altında tutulabilecektir.
                        Kişisel verileriniz, ilgili mevzuat kapsamında ve yukarıda yer verilen amaçlarla,
                        doğrudan/dolaylı olarak</p>
                    <p>• “Ersistem Yangın ve Güvenlik Sistemleri Ltd” şirketinden, </p>
                    <p>• Sipariş ettiğiniz veya kullandığınız ürün ve hizmetlerin ulaştırılması amacıyla ilişkili olarak
                        iş ortaklıkları, </p>
                    <p>• Şirket nam ve hesabına sizlere verilebilecek hizmetleri yerine getirmek için faaliyette bulunan
                        şirketler, </p>
                    <p>• Kredi referans şirketi, icra ve sair yollarla alacak takibini teminen ilgili kurum ve
                        kuruluşlar, </p>
                    <p>• Yetki vermiş olduğunuz temsilcileriniz, </p>
                    <p>• Avukatlar, vergi danışmanları ve denetimciler de dâhil olmak üzere danışmanlık aldığımız üçüncü
                        kişiler, </p>
                    <p>• Düzenleyici ve denetleyici kurumlarla, resmi merciler, dâhil yurtiçi/yurtdışı iştiraklerimiz ya
                        da bağlı ortaklıklarımız, şirketimizce hizmet/destek/danışmanlık alınan ya da işbirliği yapılan
                        ya da proje/program/finansman ortağı olunan yurtiçi/yurtdışı/uluslararası, kamu/özel kurum ve
                        kuruluşlar, şirketler ve sair 3. kişi ya da kuruluşlara aktarılabilecektir. </p>
                    <p>Kişisel Verilerinizin, sözleşmenizin devamı süresince ve sözleşmenizin sona ermesi halinde hukuki
                        yükümlülüklerin gerektirdiği süreyle ya da ilgili mevzuatta izin verilen süreyle süre mevzuata
                        uygun koşullarda saklanmak durumunda olduğunu ve süre sonunda ise otomatik olarak yok
                        edileceğini veya anonim hale getirileceğini bildiririz. </p>
                    <p>Şirketimize müracaat ederek 6698 Sayılı Kanun’un 11. Maddesi uyarınca; kişisel verilerinizin
                        işlenip işlenmediğini, şayet işlenmişse, buna ilişkin bilgileri, işlenme amacını ve bu amaca
                        uygun kullanılıp kullanılmadığını ve söz konusu verilerin aktarıldığı yurt içinde veya yurt
                        dışındaki 3. Kişileri öğrenme, kişisel verileriniz eksik ya da yanlış işlenmişse bunların
                        düzeltilmesini, kişisel verilerinizin Kanunun 7. maddesinde öngörülen şartlar çerçevesinde
                        silinmesini ya da yok edilmesini ve bu kapsamda şirketimizce yapılan işlemlerin bilgilerin
                        aktarıldığı üçüncü kişilere bildirilmesini talep etme, kişisel verilerinizin münhasıran otomatik
                        sistemler ile analiz edilmesi nedeniyle aleyhinize bir sonucun ortaya çıkması halinde buna
                        itiraz etme ve kanuna aykırı olarak işlenmesi sebebiyle zarara uğramanız hâlinde zararın
                        giderilmesini talep etme haklarına sahip bulunmaktasınız.
                        6698 Sayılı Kanun’da yer aldığı şekli ile burada belirtilen haklarınızı 07.10.2016 tarihinden
                        itibaren kullanmanız mümkün olacaktır. </p>
                    <p>Ancak taleplerinizin yerine getirilmesi için şirketimizce yapılacak masrafları 6698 Sayılı
                        Kanun’un “Veri Sorumlusuna Başvuru” başlıklı 13. Maddesinde belirtilen esaslar uyarınca
                        tarafınızdan talep etme hakkımız saklıdır. </p>
                    <p>Yukarıda yapılan açıklamalar çerçevesinde; </p>
                    <p>İşbu taahhütnameyi, okuyup, anladığımı ve bu şekilde alınan aşağıdaki beyanımın geçerli olduğunu
                        kabul ediyorum. </p>
                    <p>6698 sayılı Kişisel Verilerin Korunması Kanunu'na uygun olarak kişisel ve/veya özel nitelikli
                        kişisel verilerimin; tamamen veya kısmen elde edilmesi, kaydedilmesi, depolanması,
                        değiştirilmesi, güncellenmesi, periyodik olarak kontrol edilmesi, yeniden düzenlenmesi,
                        sınıflandırılması, işlendikleri amaç için gerekli olan ya da ilgili kanunda öngörülen süre kadar
                        muhafaza edilmesi, kanuni ya da hizmete bağlı fiili gereklilikler halinde şirketinizin
                        birlikte çalıştığı ya da kanunen yükümlü olduğu kamu kurum ve kuruluşlarıyla ve/veya Türkiye'de
                        veya yurt dışında mukim olan 3. kişi hizmet sağlayıcı, tedarikçi firmalar, Şirketiniz ve/veya
                        Şirketinizin ortakları ile paylaşılması, kanuni ya da hizmete bağlı fiili gereklilikler halinde
                        yurtdışına aktarılması da dâhil olmak üzere yukarıda belirtilen açıklamalar kapsamında
                        işlenmesine, konu hakkında tereddüde yer vermeyecek şekilde bilgi
                        sahibi olarak, aydınlatılmış açık rızam ile onay veriyorum. </p>
                </div>
            </div>

        </div>
    </div>
</div>

<script  src="<?php echo THEME_FOLDER; ?>/js/jquery.slimmenu.min.js"></script>
<script  src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

<script  src="<?php echo THEME_FOLDER; ?>/js/main.js"></script>
<script  src="<?php echo THEME_FOLDER; ?>/js/mask.js"></script>
<script  src="//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script   defer src="//use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
<script defer src="<?php echo THEME_FOLDER; ?>/js/jquery.flexslider.js"></script>
<script   src="<?php echo THEME_FOLDER; ?>/js/wow.min.js"></script>

<script type="text/javascript">
    var title = document.title;
    var alttitle = "En Son Sekme | Ersistem Yangın ve Güvenlik Sistemleri";
    window.onblur = function () { document.title = alttitle; };
    window.onfocus = function () { document.title = title; };
</script>

<script type="text/javascript">

    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails",
            start: function (slider) {
                $('body').removeClass('loading');
            }
        });
    });
</script>

<script>
    wow = new WOW(
        {
            animateClass: 'animated',
            offset: 100,
            callback: function (box) {

            }
        }
    );
    wow.init();
    <?php echo $settings['siteAdditionalJS']; ?>
</script>
</body>
</html>
