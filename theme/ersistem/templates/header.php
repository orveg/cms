
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="language" content="tr-TR">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="Content-language" content="tr"/>
    <meta name="author" content="Ersistem"/>
    <meta name="copyright" content="all content copyright 2019 pt corp."/>
    <meta name="description" content="Yangın ve Güvenlik Sistemleri konularında 16 Yıllık sektör tecrübemiz  ile hizmetinizdeyiz">
    <meta name="keywords" content="Yangın İhbar Sistemleri, Yangın Algılama Sistemleri, Ex-Proof Duman ve Gaz Dedektörü,  Yangın Alarm Paneli, Konvansiyonel Dedektör, Yangın Söndürücü, Yangın Projesi, Yangın Malzemesi, Yangın Söndürme, Yangın Algılama Paneli, Yangın Kontol Paneli, Yangın Nedir, Yangın ve Güvenlik, Binaların Yangından Korunması Hakkında Yönetmelik, Duman dedektörü, yangın alarm sistemleri nedir"/>
    <?php if ($settings['siteFavicon']!=""){ ?>
        <link type="image/x-icon" rel="icon" href="<?php echo BASE_URL; ?><?php echo $settings['siteFavicon']; ?>" />
    <?php } ?>
    <title><?php echo $page['pageTitle']; ?> | <?php echo $settings['siteTitle']; ?> </title>
    <link type="text/css" rel="stylesheet" href="<?php echo THEME_FOLDER; ?>/css/main.css">
    <link type="text/css" rel="stylesheet" href="<?php echo THEME_FOLDER; ?>/css/mobile.css">
    <link type="text/css" rel="stylesheet" href="<?php echo THEME_FOLDER; ?>/css/animate/animate.css">
    <link type="text/css" rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <script  src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140581114-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-140581114-1');
    </script>

</head>
<body class="fade-in">
<header>
    <div class="container-fluid bg-two">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 d-none d-md-none d-lg-block">
                        <a href="<?php echo BASE_URL; ?>">
                            <img src="<?php echo THEME_FOLDER; ?>/images/ersistem-logo.svg" class="mmx-img-fluid">
                        </a>
                    </div>
                    <div class="col-md-12 col-lg-9 m-t4">
                        <div class="row justify-content-center">
                            <a href="<?php echo BASE_URL; ?>" class="col-4 col-md-3 mob-logo mmx-xs-iblock">
                                <img class="img-fluid" src="<?php echo THEME_FOLDER; ?>/images/ersistem-logo.svg">
                            </a>
                            <?php hooskNav('header') ?>

                            <span class="mmx-top d-none d-md-none d-lg-block">
                                <a href="#" class="mmx-search"></a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form method="post" action="/arama" >
        <div class="container-fluid mmx-searh-content">
            <div class="row justify-content-center">
                <div class="col-md-10 p-2">
                    <input type="text" name="q" placeholder="Ne aramak istemiştiniz?">
                </div>
            </div>
        </div>
    </form>
</header>
