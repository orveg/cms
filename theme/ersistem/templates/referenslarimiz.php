<?php echo $header; ?>
<?php if($page['enableSlider'] == 1) { ?>
    <div id="carousel" class="carousel slide " data-ride="carousel">
        <?php getCarousel($page['pageID']); ?>
        <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
<?php } ?>
<?php if($page['enableSlider'] == 2) {  $banner = getBanner($page['pageID']); ?>
    <section class="mmx-content-title">
        <div class="container-fluid">
            <div class="row">
                <div class="mmx-banner">
                    <img class="img-fluid w-100" alt="<?php echo $banner->slideAlt ?>" src="<?php echo $banner->slideImage ?>">
                </div>
                <div class="mmx-title">
                    <h3 class="mmx-b-title"><?php echo $page['pageTitle'] ?></h3>
                    <?php echo breadcrumb($this->uri) ?>
                </div>
                <a href="#" class="free-cap" data-toggle="modal" data-target="#formModal">Ücretsiz Keşif</a>
            </div>
        </div>
    </section>
<?php } ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 general-content">
                    <?php if($page['enableJumbotron'] == 1) { ?>
                        <?php echo $page['jumbotronHTML']; ?>
                    <?php } ?>
                </div>
                <div class="col-12">
                    <div class="row">
                     <?php referenslarimiz_sayfa() ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php echo $footer; ?>
