<?php echo $header; ?>
<?php if ($page['enableSlider'] == 1) { ?>
    <section>
        <div class="container-fluid">
            <div class="row">
                <div id="main-carousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">

                    <?php getCarousel($page['pageID']); ?>
                    <a class="carousel-control-prev  mx-carousel-control-prev" href="#main-carousel" role="button"
                       data-slide="prev">
                    </a>
                    <a class="carousel-control-next mx-carousel-control-next" href="#main-carousel" role="button"
                       data-slide="next">
                    </a>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<?php if ($page['enableJumbotron'] == 1) { ?>
    <div class="container content-padding">
        <div class="row">
            <div class="col-md-12">
                <?php echo $page['jumbotronHTML']; ?>
            </div>
        </div>
    </div>

<?php } ?>

<section class="bg-one pb-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow fadeInUp">
                <h2 class="sec-title">Faaliyet Alanlarımız</h2>
            </div>

            <div class="col-md-12 ">
                <div class="row">
                    <?php
                    foreach($home_products as $homeProduct) {
                        ?>
                        <div class="col-12 col-lg-4 col-md-4 wow fadeInUp mb-3" data-wow-delay="1s">
                            <a href="/kategori/<?=$homeProduct["categorySlug"]?>/urun/<?=$homeProduct["postURL"]?>"
                               class="mx-mini-box mx-main">
                                <figure>
                                    <img src="<?=$homeProduct["postImage"]?>"
                                         class="img-fluid">
                                </figure>

                                <div class="mini-box-txt">
                                    <h5>
                                        <?=$homeProduct["postTitle"]?>
                                    </h5>
                                    <p>
                                        <?=$homeProduct["postExcerpt"]?>
                                    </p>
                                    <p class="more">
                                        Detaylı Bilgi
                                    </p>
                                </div>
                            </a>
                        </div>
                   <?php }

                    ?>

                </div>
            </div>
            <div class="col-md-12">
                <a href="http://ersistem.com.tr/kategori/tum-urunler" class="mmx-all-product">TÜM FAALİYET ALANLARIMIZ</a>
            </div>
        </div>
    </div>

</section>

<section class="pb-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow fadeInUp">
                <h2 class="sec-title">Referanslarımız</h2>
                <a class="carousel-control-prev mx-carousel-control-prev left-prev" href="#referance-carousel"
                   role="button" data-slide="prev">
                </a>
                <a class="carousel-control-next mx-carousel-control-next right-next" href="#referance-carousel"
                   role="button" data-slide="next">
                </a>
            </div>

            <div class="col-md-12 wow fadeInUp">
                <div id="referance-carousel" class="carousel slide" data-ride="carousel">
                    <? referenslarimiz() ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-one pb-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow fadeInUp">
                <h2 class="sec-title">Hizmetlerimiz</h2>
            </div>
            <div class="col-md-6 col-lg-6 m-b3 wow fadeInLeft">
                <div class="mmx-caption-block mmx-ex">
                    <a href="http://ersistem.com.tr/kategori/yangin-ve-gaz-algilama-sistemleri"
                       class="mmx-pre-caption-hover">
                        <img src="/uploads/anasayfa/home-img1.jpg" class="img-fluid">
                        <div class="on-pre p1">
                            <p class="">Yangın ve Alarm Sistemleri</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="row">
                    <div class="col-md-12 m-b3 wow fadeInRight">
                        <div class="mmx-caption-block mmx-ex">
                            <a href="http://ersistem.com.tr/kategori/kamera-sistemleri" class="mmx-pre-caption-hover">
                                <img src="/uploads/anasayfa/home-img3.jpg" class="img-fluid">
                                <div class="on-pre p4">
                                    <p class=""> Kamera Sistemleri</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-12 wow fadeInRight">
                        <div class="mmx-caption-block mmx-ex">
                            <a href="http://ersistem.com.tr/kategori/yangin-ve-gaz-algilama-sistemleri/urun/exproof-endustriyel-tip-gaz-ve-yangin-algilama-sistemleri" class="mmx-pre-caption-hover">
                                <img src="/uploads/anasayfa/gaz_alarm_sistemleri.jpg" class="img-fluid">
                                <div class="on-pre p2">
                                    <p class="">Exproof Endüstriyel Tip Gaz ve Yangın Algılama</p>
                                </div>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pb-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow fadeInUp">
                <h2 class="sec-title">Neden Biz?</h2>
            </div>
            <div class="col-md-12 wow fadeInUp">
                <div class="row">
                    <div class="col-lg-2 col-md-3 col-4 m-b3">
                        <img src="/uploads/anasayfa/belgeli-hizmet.jpg" class="img-fluid"/>
                    </div>
                    <div class="col-lg-2 col-md-3 col-4 m-b3">
                        <img src="/uploads/anasayfa/tecrube.jpg" class="img-fluid"/>
                    </div>
                    <div class="col-lg-2 col-md-3 col-4 m-b3">
                        <img src="/uploads/anasayfa/sorumluluk-tasiyoruz.jpg" class="img-fluid"/>
                    </div>
                    <div class="col-lg-2 col-md-3 col-4 m-b3">
                        <img src="/uploads/anasayfa/sozunun-eri.jpg" class="img-fluid"/>
                    </div>
                    <div class="col-lg-2 col-md-3 col-4 m-b3">
                        <img src="/uploads/anasayfa/guvenilir-yapi.jpg" class="img-fluid"/>
                    </div>
                    <div class="col-lg-2 col-md-3 col-4 m-b3">
                        <img src="/uploads/anasayfa/sureklilik.jpg" class="img-fluid"/>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


<?php echo $footer; ?>
