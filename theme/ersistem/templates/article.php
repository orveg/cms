<?php echo $header ?>
<section class="mmx-content-title">
    <div class="container-fluid">
        <div class="row">
            <div class="mmx-banner">
                <img class="w-100 img-fluid" alt="<?php echo $page['pageTitle']; ?>" src="/uploads/urunlerbanner.jpg">
            </div>

            <div class="mmx-title">
                <h3 class="mmx-b-title"><?php echo $page['pageTitle']; ?></h3>
                <ul class="mmx-bread-crumb">
                    <li>
                        <a href="#">Anasayfa</a>
                    </li>
                    <li>
                        <a href="/kategori/<?php echo $page['categorySlug']; ?>"><?php echo $page['categoryTitle']; ?></a>
                    </li>
                    <li>
                        <a href="#"><?php echo $page['pageTitle']; ?></a>
                    </li>
                </ul>

            </div>
            <a href="#" class="free-cap" data-toggle="modal" data-target="#formModal">Ücretsiz Keşif</a>
        </div>
    </div>
</section>
<section class="general-content">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12 mmx-main-product" style="overflow: hidden">
                <figure style="display: none">
                    <img src="<?php echo $page['postImage']; ?>" class="img-fluid" alt="<?php echo $page['pageTitle']; ?>">
                </figure>

                <div class="slider">
                    <div class="flexslider">
                        <ul class="slides">
                            <li data-thumb="<?php echo $page['postImage']; ?>">
                                <img src="<?php echo $page['postImage']; ?>" class="img-fluid" alt="<?php echo $page['pageTitle']; ?>">
                            </li>
                            <? if(!empty($page['postImage2'])) { ?>
                                <li data-thumb="<?php echo $page['postImage2']; ?>">
                                    <img src="<?php echo $page['postImage2']; ?>" class="img-fluid" alt="<?php echo $page['pageTitle']; ?>">
                                </li>
                            <? } ?>
                            <? if(!empty($page['postImage3'])) { ?>
                                <li data-thumb="<?php echo $page['postImage3']; ?>">
                                    <img src="<?php echo $page['postImage3']; ?>" class="img-fluid" alt="<?php echo $page['pageTitle']; ?>">
                                </li>
                            <? } ?>
                            <? if(!empty($page['postImage4'])) { ?>
                                <li data-thumb="<?php echo $page['postImage4']; ?>">
                                    <img src="<?php echo $page['postImage4']; ?>" class="img-fluid" alt="<?php echo $page['pageTitle']; ?>">
                                </li>
                            <? } ?>
                        </ul>
                    </div>
                </div>


            </div>
            <div class="col-md-6 col-12">

                <?php echo $page['postExcerptHTML']; ?>

            </div>
            <div class="col-12">
                <?php echo $page['postContent']; ?>
            </div>
        </div>
    </div>
</section>
<?php echo $footer; ?>
