<?php echo $header; ?>
<section class="mmx-content-title">
    <div class="container-fluid">
        <div class="row">
            <div class="mmx-banner">
                <img class="img-fluid w-100" src="/uploads/content-title12.jpg">
            </div>
            <div class="mmx-title">
                <h3 class="mmx-b-title">Arama Sonuçları</h3>
                <ul class="mmx-bread-crumb">
                    <li>
                        <a href="#">Anasayfa</a>
                    </li>
                    <li>
                        <a href="#">Arama Sonuçları</a>
                    </li>
                </ul>
            </div>
            <a href="#" class="free-cap" data-toggle="modal" data-target="#formModal">Ücretsiz Keşif</a>
        </div>
    </div>
</section>
<section class="">
    <div class="container">
        <div class="row">
            <div class="col-12 general-content">
                <?php echo $q ?> kelimesi için <?php echo $count ?> sonuç bulundu.
                <ul class="mmx-search-page">
                    <?php if(!empty($search)) { ?>
                        <?php foreach($search as $s) { ?>
                            <li>
                                <a href="/kategori/<?php echo $s["categorySlug"] ?>/urun/<?php echo $s["postURL"] ?>">
                                    <p><?php echo $s["pageTitle"] ?></p>
                                    <span>Detaylar</span>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php echo $footer; ?>
