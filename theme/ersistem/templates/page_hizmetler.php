<?php echo $header; ?>
<?php if($page['enableSlider'] == 1) { ?>
    <div id="carousel" class="carousel slide " data-ride="carousel">
        <?php getCarousel($page['pageID']); ?>
        <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
<?php } ?>
<?php if($page['enableSlider'] == 2) {
    $banner = getBanner($page['pageID']); ?>
    <section class="mmx-content-title">
        <div class="container-fluid">
            <div class="row">
                <div class="mmx-banner">
                    <img class="img-fluid w-100" alt="<?php echo $banner->slideAlt ?>" src="<?php echo $banner->slideImage ?>">
                </div>
                <div class="mmx-title">
                    <h3 class="mmx-b-title"><?php echo $page['pageTitle'] ?></h3>
                    <?php echo breadcrumb($this->uri) ?>
                </div>

                <a href="#" class="free-cap" data-toggle="modal" data-target="#formModal">Ücretsiz Keşif</a>
            </div>
        </div>
    </section>
<?php } ?>
<?php if($page['enableJumbotron'] == 1) { ?>
    <div class="container content-padding">
        <div class="row">
            <div class="col-md-12">
                <?php echo $page['jumbotronHTML']; ?>
            </div>
        </div>
    </div>
<?php } ?>
<section class="">
    <div class="container">
        <div class="row">
            <div class="col-12 general-content">
                <h1>
                    "Belki her zaman ihtiyacınız olmaz ama ihtiyacınız olduğunda değerini para ile ölçemezsiniz!"
                </h1>
                <p>Yangın zaman ve mekan açısından kontrolsüz bir şekilde oluşan yanma olayıdır.
                    Yangın algılama sistemleri ile ne kadar erken müdahale edilirse (ilk 3-5 dakika) kontrol altına
                    almak daha kolay olacaktır.</p>

            </div>
            <div class="col-12 general-content">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="mmx-caption-block">
                            <a href="#" class="mmx-pre-caption-hover" data-toggle="modal" data-target="#formModal">
                                <img src="/uploads/hizmetler/hizmetler-img1.png" class="img-fluid w-100" alt="Ücretsiz Keşif">
                                <div class="on-pre p3">
                                    <p class="">Ücretsiz Keşif</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="mmx-caption-block">
                            <a href="http://cms.orveg.com/hizmetlerimiz/kesif-danismanlik-ve-projelendirme" class="mmx-pre-caption-hover">
                                <img src="/uploads/hizmetler/hizmetler-img2.png" class="img-fluid w-100" alt="">
                                <div class="on-pre p3">
                                    <p class="">Keşif Danışmanlık ve Projelendirme</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="mmx-caption-block">
                            <a href="http://cms.orveg.com/hizmetlerimiz/anahtar-teslim-uygulama" class="mmx-pre-caption-hover">
                                <img src="/uploads/hizmetler/hizmetler-img3.png" class="img-fluid w-100" alt="">
                                <div class="on-pre p3">
                                    <p class="">Anahtar Teslim Uygulama </p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="mmx-caption-block">
                            <a href="http://cms.orveg.com/hizmetlerimiz/servis-bakim-ve-periyodik-bakim" class="mmx-pre-caption-hover">
                                <img src="/uploads/hizmetler//hizmetler-img4.png" class="img-fluid w-100" alt="">
                                <div class="on-pre p3">
                                    <p class=""> Servis & Bakım ve Periyodik Bakım </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>
<?php echo $footer; ?>
