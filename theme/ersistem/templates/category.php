<?php echo $header; ?>
<?php $totSegments = $this->uri->total_segments();
if(!is_numeric($this->uri->segment($totSegments))) {
    $offset = 0;
} else {
    if(is_numeric($this->uri->segment($totSegments))) {
        $offset = $this->uri->segment($totSegments);
    }
}

$limit = 6;
?>
<section class="mmx-content-title">
    <div class="container-fluid">
        <div class="row">
            <div class="mmx-banner">
                <img class="img-fluid w-100" alt="Ürünler" src="/uploads/urunlerbanner.jpg">
            </div>
            <div class="mmx-title">
                <h3 class="mmx-b-title">Ürünler</h3>
                <ul class="mmx-bread-crumb">
                    <li>
                        <a href="#">Anasayfa</a>
                    </li>
                    <li>
                        <a href="#">Ürünler</a>
                    </li>
                </ul>
            </div>
            <a href="#" class="free-cap" data-toggle="modal" data-target="#formModal">Ücretsiz Keşif</a>
        </div>
    </div>
</section>
<section class="general-content">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-12">
                <div class="sidebar-navigation">
                    <a href="/kategori/tum-urunler" class="title"> Tüm Ürünler </a>
                    <?php getCategories(); ?>
                </div>
            </div>
            <div class="col-md-8 col-12">
                <div class="row mmx-product">
                    <div class="col-12">
                        <div class="mmx-sort">
                            <div class="row">
                                <div class="col-9">
                                    <p><?php countPosts($categoryID, $limit, $offset); ?></p>

                                </div>
                                <div class="col-3 float-right" style="display: none">
                                    <div class="btn-group ">
                                        <button class="btn btn-secondary dropdown-toggle btn-sm" type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                            Varsılan Sıralama
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right"
                                             aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#">Sıralama1</a>
                                            <a class="dropdown-item" href="#">Sıralama2 </a>
                                            <a class="dropdown-item" href="#">Sıralama3</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php getCategoryProducts($categoryID, $limit, $offset); ?>
                    <? if(countPosts($categoryID, $limit, $offset, FALSE) > $limit) { ?>
                        <div class="col-12">
                            <nav aria-label="Page navigation" class="float-right">
                                <?php echo $this->pagination->create_links(); ?>
                            </nav>
                        </div>
                    <? } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $footer; ?>
