<?php
echo $header;
$banner = getBanner($page['pageID']); ?>
<section class="mmx-content-title">
    <div class="container-fluid">
        <div class="row">
            <div class="mmx-banner">
                <img class="img-fluid w-100" alt="<?php echo $banner->slideAlt ?>" src="<?php echo $banner->slideImage ?>">
            </div>
            <div class="mmx-title">
                <h3 class="mmx-b-title"><?php echo $page['pageTitle'] ?></h3>
                <?php echo breadcrumb($this->uri) ?>
            </div>
            <a href="#" class="free-cap" data-toggle="modal" data-target="#formModal">Ücretsiz Keşif</a>
        </div>
    </div>
</section>
<section class="">
    <div class="container">
        <div class="row">
            <div class="col-12 general-content">
                <h1 class="text-center">Ailemize Katılın</h1>
                <p class="text-center">B&uuml;t&uuml;n personellerimizin maddi ve manevi &ccedil;ıkarlarını g&ouml;zeten
                    anlayışımız, m&uuml;şteri memnuniyetini &ouml;n planda tutan kadromuza sizde dahil olun.</p>
            </div>
            <div class="col-12 general-content">
                <div class="row wow fadeInUp">
                    <div class="col-lg-4 col-md-12 ">
                        <div class="mmx-caption-block">
                            <a class="mmx-pre-caption-hover"
                               href="http://cms.orveg.com/insan-kaynaklari/kariyer-olanaklari">
                                <img alt="Ersistem Yangın ve Güvenlik Kariyer Olanakları" class="img-fluid w-100"
                                     src="/uploads/ik/ik-kutu-1.png"/>
                                <div class="on-pre p3">
                                    <p>
                                        Kariyer Olanakları

                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12">
                        <div class="mmx-caption-block">
                            <a class="mmx-pre-caption-hover" href="http://cms.orveg.com/insan-kaynaklari/aranilan-nitelikler">
                                <img alt="" class="img-fluid w-100" src="/uploads/ik/ik-kutu-2.png"/>
                                <div class="on-pre p3">
                                    <p>Aradığımız Nitelikler</p>
                                </div>

                            </a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12">
                        <div class="mmx-caption-block">
                            <a class="mmx-pre-caption-hover" href="http://cms.orveg.com/insan-kaynaklari/is-basvuru-formu">
                                <img alt="" class="img-fluid w-100" src="/uploads/ik/ik-kutu-3.png"/>
                                <div class="on-pre p3">
                                    <p>İş Başvuru Formu</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $footer; ?>
