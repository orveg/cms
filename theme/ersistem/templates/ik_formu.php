<?php echo $header; ?>
<?php if ($page['enableSlider'] == 1) { ?>
    <div id="carousel" class="carousel slide " data-ride="carousel">
        <?php getCarousel($page['pageID']); ?>
        <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
<?php } ?>
<?php if ($page['enableSlider'] == 2) {
    $banner = getBanner($page['pageID']); ?>
    <section class="mmx-content-title">
        <div class="container-fluid">
            <div class="row">
                <div class="mmx-banner">
                    <img class="img-fluid w-100" alt="<?php echo $banner->slideAlt ?>"
                         src="<?php echo $banner->slideImage ?>">
                </div>

                <div class="mmx-title">
                    <h3 class="mmx-b-title"><?php echo $page['pageTitle'] ?></h3>
                    <?php echo breadcrumb($this->uri) ?>
                </div>
                <a href="#" class="free-cap" data-toggle="modal" data-target="#formModal">Ücretsiz Keşif</a>
            </div>
        </div>
    </section>
<?php } ?>
<?php if ($page['enableJumbotron'] == 1) { ?>
    <div class="container content-padding">
        <div class="row">
            <div class="col-md-12">
                <?php echo $page['jumbotronHTML']; ?>
            </div>
        </div>
    </div>
<?php } ?>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-12 mb-4 wow fadeInUp">
                <div class="mmx-address">
                    <h3>Ersistem Yangın ve Güvenlik Sistemleri Ltd. Şti.</h3>
                    <ul>
                        <li>
                            <svg class="svg-inline--fa fa-home fa-w-18" aria-hidden="true" focusable="false"
                                 data-prefix="fas" data-icon="home" role="img" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 576 512" data-fa-i2svg="">
                                <path fill="currentColor"
                                      d="M280.37 148.26L96 300.11V464a16 16 0 0 0 16 16l112.06-.29a16 16 0 0 0 15.92-16V368a16 16 0 0 1 16-16h64a16 16 0 0 1 16 16v95.64a16 16 0 0 0 16 16.05L464 480a16 16 0 0 0 16-16V300L295.67 148.26a12.19 12.19 0 0 0-15.3 0zM571.6 251.47L488 182.56V44.05a12 12 0 0 0-12-12h-56a12 12 0 0 0-12 12v72.61L318.47 43a48 48 0 0 0-61 0L4.34 251.47a12 12 0 0 0-1.6 16.9l25.5 31A12 12 0 0 0 45.15 301l235.22-193.74a12.19 12.19 0 0 1 15.3 0L530.9 301a12 12 0 0 0 16.9-1.6l25.5-31a12 12 0 0 0-1.7-16.93z"></path>
                            </svg><!-- <i class="fas fa-home"></i> -->
                            Merkez Ofis Adresi:
                            <p>
                                CEMİL MERİÇ MAHALLESİ, ÇAYIRÖNÜ CADDESİ NO:13/C
                                <br>
                                ÜMRANİYE / İSTANBUL
                            </p>
                        </li>
                        <li>
                            <svg class="svg-inline--fa fa-phone fa-w-16" aria-hidden="true" focusable="false"
                                 data-prefix="fas" data-icon="phone" role="img" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 512 512" data-fa-i2svg="">
                                <path fill="currentColor"
                                      d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z"></path>
                            </svg><!-- <i class="fas fa-phone"></i> -->
                            Ofis Numarası:
                            <p>
                                +90 216 610 03 06
                            </p>
                        </li>
                        <li>
                            <svg class="svg-inline--fa fa-phone fa-w-16" aria-hidden="true" focusable="false"
                                 data-prefix="fas" data-icon="phone" role="img" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 512 512" data-fa-i2svg="">
                                <path fill="currentColor"
                                      d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z"></path>
                            </svg><!-- <i class="fas fa-phone"></i> -->
                            GSM Numarası
                            <p>
                                +90 530 962 74 62
                            </p>
                        </li>
                        <li>
                            <svg class="svg-inline--fa fa-phone fa-w-16" aria-hidden="true" focusable="false"
                                 data-prefix="fas" data-icon="phone" role="img" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 512 512" data-fa-i2svg="">
                                <path fill="currentColor"
                                      d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z"></path>
                            </svg><!-- <i class="fas fa-phone"></i> -->
                            Fax Numarası
                            <p>
                                +90 216 612 04 34
                            </p>
                        </li>
                        <li>
                            <svg class="svg-inline--fa fa-envelope fa-w-16" aria-hidden="true" focusable="false"
                                 data-prefix="fas" data-icon="envelope" role="img" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 512 512" data-fa-i2svg="">
                                <path fill="currentColor"
                                      d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z"></path>
                            </svg><!-- <i class="fas fa-envelope"></i> -->
                            E-posta
                            <p>
                                info@ersistem.com.tr
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-12">

                <div class="form-wrapper">
                    <h5>
                        Başvuru Formu
                    </h5>
                    <p>Lütfen bütün alanları eksiksiz doldurunuz. En kısa sürede size ulaşacağız. Teşekkürler</p>

                    <?php echo form_open_multipart('/iletisim-gonder/3'); ?>
                    <?php if ($this->session->flashdata('msg')) { ?>
                        <p class="alert-danger font-weight-bolder text-center p-3">       <?php echo $this->session->flashdata('msg'); ?>      </p>
                    <?php } ?>


                    <div class="form-group">
                        <?php echo form_error('name', '<div class="alert">', '</div>'); ?>
                        <label class="form-label" for="name">Ad & Soyad</label>
                        <input id="name" name="name" class="form-input" type="text" required/>
                    </div>

                    <div class="form-group">
                        <?php echo form_error('mail', '<div class="alert">', '</div>'); ?>
                        <label class="form-label" for="mail">E-Posta</label>
                        <input id="mail" name="mail" class="form-input" type="text" required/>
                    </div>
                    <div class="form-group">
                        <?php echo form_error('phone', '<div class="alert">', '</div>'); ?>
                        <label class="form-label" for="phone">Telefon</label>
                        <input id="phone" name="phone" class="form-input" type="text" required data-mask="0000-000-00-00" data-mask-selectonfocus="true"/>
                    </div>
                    <div class="form-group">
                        <?php echo form_error('pozisyon', '<div class="alert">', '</div>'); ?>
                        <label class="form-label" for="pozisyon">Başvurulan Pozisyon</label>
                        <input  name="pozisyon" class="form-input" type="text" required/>
                    </div>
                    <div class="form-group">
                        <input type="file" name="fileupload" value="fileupload" id="fileupload" class="img-file"
                               required="">

                        <label for="fileupload" class="file-holder"> Lütfen Öz Geçmişinizi Yükleyin</label>
                    </div>
                    <div class="form-group">
                        <input value="Gönder" type="submit" class="btn-danger btn-submit">
                    </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="container-fluid mt-5 wow fadeInUp">
        <div class="row">

            <iframe src="https://www.google.com/maps/embed?pb=!1m21!1m12!1m3!1d1842.4480918941529!2d29.14685795990945!3d41.0164070653759!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m6!3e6!4m3!3m2!1d41.0168901!2d29.147708299999998!4m0!5e0!3m2!1str!2str!4v1475481708760"
                    width="100%" height="435" frameborder="0" style="border:0" allowfullscreen=""></iframe>

        </div>
    </div>

</section>
<?php echo $footer; ?>
