<?php echo $header; ?>
<?php if($page['enableSlider'] == 1) { ?>
    <div id="carousel" class="carousel slide " data-ride="carousel">
        <?php getCarousel($page['pageID']); ?>
        <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
<?php } ?>
<?php if($page['enableSlider'] == 2) {      $banner = getBanner($page['pageID']);?>
    <section class="mmx-content-title">
        <div class="container-fluid">
            <div class="row">
                <div class="mmx-banner">
                    <img class="img-fluid w-100" alt="<?php echo $banner->slideAlt ?>" src="<?php echo $banner->slideImage ?>">
                </div>

                <div class="mmx-title">
                    <h3 class="mmx-b-title"><?php echo $page['pageTitle'] ?></h3>
                    <?php echo breadcrumb($this->uri) ?>
                </div>

                <a href="#" class="free-cap" data-toggle="modal" data-target="#formModal">Ücretsiz Keşif</a>
            </div>
        </div>
    </section>
<?php } ?>
<?php if($page['enableJumbotron'] == 1) { ?>
    <div class="container content-padding">
        <div class="row">
            <div class="col-md-12">
                <?php echo $page['jumbotronHTML']; ?>
            </div>
        </div>
    </div>
<?php } ?>


<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-12 mb-4 wow fadeInUp">
                <div class="mmx-address">
                    <h3>Ersistem Yangın ve Güvenlik Sistemleri Ltd Şti</h3>
                    <ul>
                        <li>
                            <i class="fas fa-home"></i>
                            Merkez Ofis Adresi:
                            <p>
                                CEMİL MERİÇ MAHALLESİ, ÇAYIRÖNÜ CADDESİ NO:13/C
                                <br>
                                ÜMRANİYE / İSTANBUL
                            </p>
                        </li>
                        <li>
                            <i class="fas fa-phone"></i>
                            Ofis Numarası:
                            <p>
                                 +90 216 610 03 06
                            </p>
                        </li>
                        <li>
                            <i class="fas fa-phone"></i>
                            GSM Numarası
                            <p>
                                +90 530 962 74 62
                            </p>
                        </li>
                        <li>
                            <i class="fas fa-phone"></i>
                            Fax Numarası
                            <p>
                                +90 216 612 04 34
                            </p>
                        </li>
                        <li>
                            <i class="fas fa-envelope"></i>
                            E-posta
                            <p>
                                info@ersistem.com.tr
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-12">

                <div class="form-wrapper">
                    <h5>
                        İletişim Formu
                    </h5>
                    <p>Bize iletmek istediğiniz görüş ve  öneriler için formu doldurabilirsiniz.</p>
                    <form action="/iletisim-gonder/1" method="post" class="form mt-4">
                        <?php if($this->session->flashdata('msg')) { ?>
                            <p class="alert-danger font-weight-bolder text-center p-3">       <?php echo $this->session->flashdata('msg'); ?>      </p>
                        <?php } ?>
                        <div class="form-group">
                            <?php echo form_error('name', '<div class="alert">', '</div>'); ?>
                            <label class="form-label" for="name">Ad & Soyad</label>
                            <input id="name" name="name" class="form-input" type="text" required/>
                        </div>

                        <div class="form-group">
                            <?php echo form_error('phone', '<div class="alert">', '</div>'); ?>
                            <label class="form-label" for="posta">E-Posta</label>
                            <input id="posta" name="posta" class="form-input" type="text" required/>
                        </div>
                        <div class="form-group">
                            <?php echo form_error('message', '<div class="alert">', '</div>'); ?>
                            <label class="form-label" for="message">Mesajınız</label>
                            <textarea id="message" name="message" class="form-input"></textarea>
                        </div>
                        <div class="form-group">
                            <input value="Gönder" type="submit" class="btn-danger btn-submit">
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="container-fluid mt-5 wow fadeInUp">
        <div class="row">

            <iframe src="https://www.google.com/maps/embed?pb=!1m21!1m12!1m3!1d1842.4480918941529!2d29.14685795990945!3d41.0164070653759!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m6!3e6!4m3!3m2!1d41.0168901!2d29.147708299999998!4m0!5e0!3m2!1str!2str!4v1475481708760"
                    width="100%" height="435" frameborder="0" style="border:0" allowfullscreen=""></iframe>

        </div>
    </div>

</section>

<?php echo $footer; ?>
