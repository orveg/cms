<?php echo $header; ?>
<div class="jumbotron text-center errorpadding">
    <div class="container">
        <div class="row">
            <div class="col col-lg-12 col-sm-12 mt-5">
                <img style="width: 300px" src="<?php echo THEME_FOLDER; ?>/images/ersistem-logo.svg"/>
                <h1>Oops, Aradığınız sayfayı bulamadık</h1>
                <p></p>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <hr>
</div>
<?php echo $footer; ?>
