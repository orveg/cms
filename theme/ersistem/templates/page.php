<?php echo $header; ?>


<?php if($page['enableSlider'] == 1) { ?>
    <div id="carousel" class="carousel slide " data-ride="carousel">

        <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
<?php } ?>
<?php if($page['enableSlider'] == 2) {
    $banner = getBanner($page['pageID']);
    ?>
    <section class="mmx-content-title">
        <div class="container-fluid">
            <div class="row">
                <div class="mmx-banner">
                    <img class="img-fluid w-100" alt="<?php echo $banner->slideAlt ?>" src="<?php echo $banner->slideImage ?>">
                </div>

                <div class="mmx-title">
                    <h3 class="mmx-b-title"><?php echo $page['pageTitle'] ?></h3>
                    <?php echo breadcrumb($this->uri) ?>
                </div>

                <a href="#" class="free-cap" data-toggle="modal" data-target="#formModal">Ücretsiz Keşif</a>
            </div>
        </div>
    </section>
<?php } ?>
<?php if($page['enableJumbotron'] == 1) { ?>
    <div class="container content-padding">
        <div class="row">
            <div class="col-md-12">
                <?php echo $page['jumbotronHTML']; ?>
            </div>
        </div>
    </div>
<?php } ?>


<section class="">
    <div class="container">
        <div class="row">
            <div class="col-12 general-content">
                <?php echo $page['pageContent']; ?>

            </div>
        </div>
    </div>
    <?php
    if($this->uri->segment(2) == "hakkimizda") {
        ?>
        <div class="container-fluid bg-one wow fadeInUp ">
            <div class="row">
                <div class="container mt-5 mb-5">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-6 mmx-counter ico-counter1">
                            <span class="mmx-span1"></span>
                            <span class="mmx-span2"></span>
                            <div class="counter" data-count="82">0</div>
                            <p>Yerleşim Alanı</p>

                        </div>
                        <div class="col-lg-3 col-md-6 col-6 mmx-counter ico-counter2">
                            <span class="mmx-span1"></span>
                            <span class="mmx-span2"></span>
                            <div class="counter" data-count="128">0</div>
                            <p>Turizm Tesisi</p>

                        </div>

                        <div class="col-lg-3 col-md-6 col-6 mmx-counter ico-counter1">
                            <span class="mmx-span1"></span>
                            <span class="mmx-span2"></span>
                            <div class="counter" data-count="43">0</div>
                            <p> Kamu Kuruluşu </p>

                        </div>
                        <div class="col-lg-3 col-md-6 col-6 mmx-counter ico-counter1">
                            <span class="mmx-span1"></span>
                            <span class="mmx-span2"></span>
                            <div class="counter" data-count="59">0</div>
                            <p>Sanayii Tesisi</p>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    <? } ?>
</section>

<?php echo $footer; ?>
