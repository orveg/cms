$(function () {
    var e = $(".sidebar-navigation > ul");
    e.find("li a").click(function (l) {
        var i = $(this).parent();
        i.find("ul").length > 0 && (l.preventDefault(), i.hasClass("selected") ? (i.removeClass("selected").find("li").removeClass("selected"), i.find("ul").slideUp(400), i.find("a em").removeClass("mdi-flip-v")) : (0 == i.parents("li.selected").length ? (e.find("li").removeClass("selected"), e.find("ul").slideUp(400), e.find("li a em").removeClass("mdi-flip-v")) : (i.parent().find("li").removeClass("selected"), i.parent().find("> li ul").slideUp(400), i.parent().find("> li a em").removeClass("mdi-flip-v")), i.addClass("selected"), i.find(">ul").slideDown(400), i.find(">a>em").addClass("mdi-flip-v")))
    }), $(".sidebar-navigation > ul ul").each(function (e) {
        if ($(this).find(">li>ul").length > 0) {
            var l = $(this).parent().parent().find(">li>a").css("padding-left"), i = parseInt(l) + 20;
            $(this).find(">li>a").css("padding-left", i)
        } else {
            l = $(this).parent().parent().find(">li>a").css("padding-left"), i = parseInt(l) + 20;
            $(this).find(">li>a").css("padding-left", i).parent().addClass("selected--last")
        }
    });
    for (var l = 1; l <= 10; l++) $(".sidebar-navigation > ul > " + " li > ul ".repeat(l)).addClass("subMenuColor" + l);
    var i = $("li.selected");
    i.length && function e(l) {
        var i = l.closest("ul");
        if (i.length) {
            if (l.addClass("selected"), i.addClass("open"), l.find(">a>em").addClass("mdi-flip-v"), !i.closest("li").length) return !1;
            e(i.closest("li"))
        }
    }(i)
});
