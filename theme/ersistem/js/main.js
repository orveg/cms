$(".slimmenu").slimmenu({
    resizeWidth: "800",
    collapserTitle: "Main Menu",
    animSpeed: "medium",
    indentChildren: !0,
    childrenIndenter: "&raquo;"
}), $(".mmx-search").click(function () {
    $(".mmx-searh-content").slideToggle(300)
}), $("#main-carousel").hover(function () {
    $(".mx-carousel-control-prev").show(300)
}), $(".counter").each(function () {
    var e = $(this), t = e.attr("data-count");
    $({countNum: e.text()}).animate({countNum: t}, {
        duration: 3000, easing: "linear", step: function () {
            e.text(Math.floor(this.countNum))
        }, complete: function () {
            e.text(this.countNum)
        }
    })
}), $("input").focus(function () {
    $(this).parents(".form-group").addClass("focused")
}), $("textarea").focus(function () {
    $(this).parents(".form-group").addClass("focused")
}), $("input").blur(function () {
    "" == $(this).val() ? ($(this).removeClass("filled"), $(this).parents(".form-group").removeClass("focused")) : $(this).addClass("filled")
}), $("textarea").blur(function () {
    "" == $(this).val() ? ($(this).removeClass("filled"), $(this).parents(".form-group").removeClass("focused")) : $(this).addClass("filled")
});

